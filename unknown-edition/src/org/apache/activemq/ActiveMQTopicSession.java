/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.activemq;

import java.io.Serializable;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.IllegalStateException;
import javax.jms.InvalidDestinationException;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;


/**
 * A TopicSession implementation that throws IllegalStateExceptions when Queue operations are attempted but which
 * delegates to another TopicSession for all other operations. The ActiveMQSessions implement both Topic and Queue
 * Sessions methods but the spec states that TopicSession should throw Exceptions if queue operations are attempted on
 * it.
 *
 *
 */
public class ActiveMQTopicSession implements TopicSession
{

	private final TopicSession next;

	public ActiveMQTopicSession(final TopicSession next)
	{
		this.next = next;
	}

	/**
	 * @throws JMSException
	 */
	public void close() throws JMSException
	{
		next.close();
	}

	/**
	 * @throws JMSException
	 */
	public void commit() throws JMSException
	{
		next.commit();
	}

	/**
	 * @param queue
	 * @return
	 * @throws JMSException
	 */
	public QueueBrowser createBrowser(final Queue queue) throws JMSException
	{
		throw new IllegalStateException("Operation not supported by a TopicSession");
	}

	/**
	 * @param queue
	 * @param messageSelector
	 * @return
	 * @throws JMSException
	 */
	public QueueBrowser createBrowser(final Queue queue, final String messageSelector) throws JMSException
	{
		throw new IllegalStateException("Operation not supported by a TopicSession");
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public BytesMessage createBytesMessage() throws JMSException
	{
		return next.createBytesMessage();
	}

	/**
	 * @param destination
	 * @return
	 * @throws JMSException
	 */
	public MessageConsumer createConsumer(final Destination destination) throws JMSException
	{
		if (destination instanceof Queue)
		{
			throw new InvalidDestinationException("Queues are not supported by a TopicSession");
		}
		return next.createConsumer(destination);
	}

	/**
	 * @param destination
	 * @param messageSelector
	 * @return
	 * @throws JMSException
	 */
	public MessageConsumer createConsumer(final Destination destination, final String messageSelector) throws JMSException
	{
		if (destination instanceof Queue)
		{
			throw new InvalidDestinationException("Queues are not supported by a TopicSession");
		}
		return next.createConsumer(destination, messageSelector);
	}

	/**
	 * @param destination
	 * @param messageSelector
	 * @param noLocal
	 * @return
	 * @throws JMSException
	 */
	public MessageConsumer createConsumer(final Destination destination, final String messageSelector, final boolean noLocal)
			throws JMSException
	{
		if (destination instanceof Queue)
		{
			throw new InvalidDestinationException("Queues are not supported by a TopicSession");
		}
		return next.createConsumer(destination, messageSelector, noLocal);
	}

	/**
	 * @param topic
	 * @param name
	 * @return
	 * @throws JMSException
	 */
	public TopicSubscriber createDurableSubscriber(final Topic topic, final String name) throws JMSException
	{
		return next.createDurableSubscriber(topic, name);
	}

	/**
	 * @param topic
	 * @param name
	 * @param messageSelector
	 * @param noLocal
	 * @return
	 * @throws JMSException
	 */
	public TopicSubscriber createDurableSubscriber(final Topic topic, final String name, final String messageSelector,
			final boolean noLocal) throws JMSException
	{
		return next.createDurableSubscriber(topic, name, messageSelector, noLocal);
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public MapMessage createMapMessage() throws JMSException
	{
		return next.createMapMessage();
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public Message createMessage() throws JMSException
	{
		return next.createMessage();
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public ObjectMessage createObjectMessage() throws JMSException
	{
		return next.createObjectMessage();
	}

	/**
	 * @param object
	 * @return
	 * @throws JMSException
	 */
	public ObjectMessage createObjectMessage(final Serializable object) throws JMSException
	{
		return next.createObjectMessage(object);
	}

	/**
	 * @param destination
	 * @return
	 * @throws JMSException
	 */
	public MessageProducer createProducer(final Destination destination) throws JMSException
	{
		if (destination instanceof Queue)
		{
			throw new InvalidDestinationException("Queues are not supported by a TopicSession");
		}
		return next.createProducer(destination);
	}

	/**
	 * @param topic
	 * @return
	 * @throws JMSException
	 */
	public TopicPublisher createPublisher(final Topic topic) throws JMSException
	{
		return next.createPublisher(topic);
	}

	/**
	 * @param queueName
	 * @return
	 * @throws JMSException
	 */
	public Queue createQueue(final String queueName) throws JMSException
	{
		throw new IllegalStateException("Operation not supported by a TopicSession");
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public StreamMessage createStreamMessage() throws JMSException
	{
		return next.createStreamMessage();
	}

	/**
	 * @param topic
	 * @return
	 * @throws JMSException
	 */
	public TopicSubscriber createSubscriber(final Topic topic) throws JMSException
	{
		return next.createSubscriber(topic);
	}

	/**
	 * @param topic
	 * @param messageSelector
	 * @param noLocal
	 * @return
	 * @throws JMSException
	 */
	public TopicSubscriber createSubscriber(final Topic topic, final String messageSelector, final boolean noLocal)
			throws JMSException
	{
		return next.createSubscriber(topic, messageSelector, noLocal);
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public TemporaryQueue createTemporaryQueue() throws JMSException
	{
		throw new IllegalStateException("Operation not supported by a TopicSession");
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public TemporaryTopic createTemporaryTopic() throws JMSException
	{
		return next.createTemporaryTopic();
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public TextMessage createTextMessage() throws JMSException
	{
		return next.createTextMessage();
	}

	/**
	 * @param text
	 * @return
	 * @throws JMSException
	 */
	public TextMessage createTextMessage(final String text) throws JMSException
	{
		return next.createTextMessage(text);
	}

	/**
	 * @param topicName
	 * @return
	 * @throws JMSException
	 */
	public Topic createTopic(final String topicName) throws JMSException
	{
		return next.createTopic(topicName);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object arg0)
	{
		if (this != arg0)
		{
			return next.equals(arg0);
		}

		return true;
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public int getAcknowledgeMode() throws JMSException
	{
		return next.getAcknowledgeMode();
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public MessageListener getMessageListener() throws JMSException
	{
		return next.getMessageListener();
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	public boolean getTransacted() throws JMSException
	{
		return next.getTransacted();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return next.hashCode();
	}

	/**
	 * @throws JMSException
	 */
	public void recover() throws JMSException
	{
		next.recover();
	}

	/**
	 * @throws JMSException
	 */
	public void rollback() throws JMSException
	{
		next.rollback();
	}

	/**
	 *
	 */
	public void run()
	{
		next.run();
	}

	/**
	 * @param listener
	 * @throws JMSException
	 */
	public void setMessageListener(final MessageListener listener) throws JMSException
	{
		next.setMessageListener(listener);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return next.toString();
	}

	/**
	 * @param name
	 * @throws JMSException
	 */
	public void unsubscribe(final String name) throws JMSException
	{
		next.unsubscribe(name);
	}

	public TopicSession getNext()
	{
		return next;
	}

	@Override
	public MessageConsumer createDurableConsumer(final Topic arg0, final String arg1) throws JMSException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageConsumer createDurableConsumer(final Topic arg0, final String arg1, final String arg2, final boolean arg3)
			throws JMSException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageConsumer createSharedConsumer(final Topic arg0, final String arg1) throws JMSException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageConsumer createSharedConsumer(final Topic arg0, final String arg1, final String arg2) throws JMSException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageConsumer createSharedDurableConsumer(final Topic arg0, final String arg1) throws JMSException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageConsumer createSharedDurableConsumer(final Topic arg0, final String arg1, final String arg2) throws JMSException
	{
		// TODO Auto-generated method stub
		return null;
	}
}
