/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.activemq;

import java.net.URI;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.XAConnection;
import javax.jms.XAConnectionFactory;
import javax.jms.XAJMSContext;
import javax.jms.XAQueueConnection;
import javax.jms.XAQueueConnectionFactory;
import javax.jms.XATopicConnection;
import javax.jms.XATopicConnectionFactory;

import org.apache.activemq.management.JMSStatsImpl;
import org.apache.activemq.transport.Transport;


/**
 * A factory of {@link XAConnection} instances
 *
 *
 */
public class ActiveMQXAConnectionFactory extends ActiveMQConnectionFactory
		implements XAConnectionFactory, XAQueueConnectionFactory, XATopicConnectionFactory
{

	public ActiveMQXAConnectionFactory()
	{
	}

	public ActiveMQXAConnectionFactory(final String userName, final String password, final String brokerURL)
	{
		super(userName, password, brokerURL);
	}

	public ActiveMQXAConnectionFactory(final String userName, final String password, final URI brokerURL)
	{
		super(userName, password, brokerURL);
	}

	public ActiveMQXAConnectionFactory(final String brokerURL)
	{
		super(brokerURL);
	}

	public ActiveMQXAConnectionFactory(final URI brokerURL)
	{
		super(brokerURL);
	}

	public XAConnection createXAConnection() throws JMSException
	{
		return (XAConnection) createActiveMQConnection();
	}

	public XAConnection createXAConnection(final String userName, final String password) throws JMSException
	{
		return (XAConnection) createActiveMQConnection(userName, password);
	}

	public XAQueueConnection createXAQueueConnection() throws JMSException
	{
		return (XAQueueConnection) createActiveMQConnection();
	}

	public XAQueueConnection createXAQueueConnection(final String userName, final String password) throws JMSException
	{
		return (XAQueueConnection) createActiveMQConnection(userName, password);
	}

	public XATopicConnection createXATopicConnection() throws JMSException
	{
		return (XATopicConnection) createActiveMQConnection();
	}

	public XATopicConnection createXATopicConnection(final String userName, final String password) throws JMSException
	{
		return (XATopicConnection) createActiveMQConnection(userName, password);
	}

	@Override
	protected ActiveMQConnection createActiveMQConnection(final Transport transport, final JMSStatsImpl stats) throws Exception
	{
		final ActiveMQXAConnection connection = new ActiveMQXAConnection(transport, getClientIdGenerator(),
				getConnectionIdGenerator(), stats);
		configureXAConnection(connection);
		return connection;
	}

	private void configureXAConnection(final ActiveMQXAConnection connection)
	{
		connection.setXaAckMode(xaAckMode);
	}

	public int getXaAckMode()
	{
		return xaAckMode;
	}

	public void setXaAckMode(final int xaAckMode)
	{
		this.xaAckMode = xaAckMode;
	}

	@Override
	public void populateProperties(final Properties props)
	{
		super.populateProperties(props);
		props.put("xaAckMode", Integer.toString(xaAckMode));
	}

	@Override
	public XAJMSContext createXAContext()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XAJMSContext createXAContext(final String arg0, final String arg1)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
