package org.codehaus.activemq.transport.jgroups;

import java.net.URI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.transport.TransportServerChannelSupport;

public class JGroupsTransportServerChannel extends TransportServerChannelSupport
{
  private static final Log log = LogFactory.getLog(JGroupsTransportServerChannel.class);
  protected URI bindAddress;

  public JGroupsTransportServerChannel(URI bindAddr)
  {
    this.bindAddress = bindAddr;
  }

  public String toString() {
    return "JGroupsTransportServerChannel@" + this.bindAddress;
  }
}