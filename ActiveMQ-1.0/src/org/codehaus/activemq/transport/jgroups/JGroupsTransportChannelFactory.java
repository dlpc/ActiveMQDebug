//package org.codehaus.activemq.transport.jgroups;
//
//import java.io.PrintStream;
//import java.net.URI;
//import javax.jms.JMSException;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportChannel;
//import org.codehaus.activemq.transport.TransportChannelFactory;
//import org.codehaus.activemq.util.JMSExceptionHelper;
//import org.jgroups.Channel;
//import org.jgroups.ChannelException;
//import org.jgroups.ChannelFactory;
//import org.jgroups.JChannelFactory;
//
//public class JGroupsTransportChannelFactory
//  implements TransportChannelFactory
//{
//  private ChannelFactory channelFactory = new JChannelFactory();
//  private Object channelConfiguration;
//  private String channelName = "ActiveMQ";
//
//  public JGroupsTransportChannelFactory() {
//  }
//
//  public JGroupsTransportChannelFactory(ChannelFactory channelFactory, Object channelConfiguration, String channelName) {
//    this.channelFactory = channelFactory;
//    this.channelConfiguration = channelConfiguration;
//    this.channelName = channelName;
//  }
//
//  public TransportChannel create(WireFormat wireFormat, URI remoteLocation) throws JMSException {
//    try {
//      Channel channel = createChannel(remoteLocation);
//      channel.setOpt(5, Boolean.TRUE);
//      channel.connect(this.channelName);
//      return new JGroupsTransportChannel(wireFormat, channel, null);
//    } catch (ChannelException e) {
//    }
//    throw JMSExceptionHelper.newJMSException("Failed to construct JGroups Channel: " + e, e);
//  }
//
//  public TransportChannel create(WireFormat wireFormat, URI remoteLocation, URI localLocation) throws JMSException
//  {
//    return create(wireFormat, remoteLocation);
//  }
//
//  public boolean requiresEmbeddedBroker() {
//    return true;
//  }
//
//  public ChannelFactory getChannelFactory()
//  {
//    return this.channelFactory;
//  }
//
//  public void setChannelFactory(ChannelFactory channelFactory) {
//    this.channelFactory = channelFactory;
//  }
//
//  public Object getChannelConfiguration() {
//    return this.channelConfiguration;
//  }
//
//  public void setChannelConfiguration(Object channelConfiguration) {
//    this.channelConfiguration = channelConfiguration;
//  }
//
//  public String getChannelName() {
//    return this.channelName;
//  }
//
//  public void setChannelName(String channelName) {
//    this.channelName = channelName;
//  }
//
//  protected Channel createChannel(URI remoteLocation) throws ChannelException {
//    Object config = this.channelConfiguration;
//    if (config == null)
//    {
//      String text = remoteLocation.getSchemeSpecificPart();
//      if (!text.equalsIgnoreCase("default")) {
//        config = text;
//      }
//    }
//    System.out.println("Configuring JGroups with: " + config);
//    return this.channelFactory.createChannel(this.channelConfiguration);
//  }
//}