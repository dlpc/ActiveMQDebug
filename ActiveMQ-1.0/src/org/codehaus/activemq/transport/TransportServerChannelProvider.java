package org.codehaus.activemq.transport;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.util.FactoryFinder;

public class TransportServerChannelProvider
{
  private static FactoryFinder finder = new FactoryFinder("META-INF/services/org/codehaus/activemq/transport/server/");

  public static TransportServerChannel create(WireFormat wireFormat, URI bindAddress)
    throws JMSException
  {
    return getFactory(bindAddress.getScheme()).create(wireFormat, bindAddress);
  }

  public static TransportServerChannel newInstance(WireFormat wireFormat, String bindAddress)
    throws JMSException, URISyntaxException
  {
    return create(wireFormat, new URI(bindAddress));
  }

  protected static TransportServerChannelFactory getFactory(String protocol) throws JMSException {
    try {
      Object value = finder.newInstance(protocol);
      if ((value instanceof TransportServerChannelFactory)) {
        return (TransportServerChannelFactory)value;
      }

      throw new JMSException("Factory does not implement TransportServerChannelFactory: " + value);
    }
    catch (IllegalAccessException e)
    {
      throw createJMSexception(protocol, e);
    }
    catch (InstantiationException e) {
      throw createJMSexception(protocol, e);
    }
    catch (IOException e) {
      throw createJMSexception(protocol, e);
    } catch (ClassNotFoundException e) {
    	throw createJMSexception(protocol, e);
    }
    
  }

  protected static JMSException createJMSexception(String protocol, Exception e)
  {
    JMSException answer = new JMSException("Could not load protocol: " + protocol + ". Reason: " + e);
    answer.setLinkedException(e);
    return answer;
  }
}