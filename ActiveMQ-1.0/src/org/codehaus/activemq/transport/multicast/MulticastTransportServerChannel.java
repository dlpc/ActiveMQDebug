package org.codehaus.activemq.transport.multicast;

import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
import java.net.URI;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportServerChannelSupport;

public class MulticastTransportServerChannel extends TransportServerChannelSupport
{
  private static final Log log = LogFactory.getLog(MulticastTransportServerChannel.class);
  protected URI bindAddress;
  private SynchronizedBoolean started;

  public MulticastTransportServerChannel(WireFormat wireFormat, URI bindAddr)
  {
    this.bindAddress = bindAddr;
    this.started = new SynchronizedBoolean(false);
    log.info("ServerChannel at: " + this.bindAddress);
  }

  public void start()
    throws JMSException
  {
    if (this.started.commit(false, true));
  }

  public String toString()
  {
    return "MulticastTransportServerChannel@" + this.bindAddress;
  }
}