package org.codehaus.activemq.transport.multicast;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportServerChannel;
import org.codehaus.activemq.transport.TransportServerChannelFactory;

public class MulticastTransportServerChannelFactory
  implements TransportServerChannelFactory
{
  public TransportServerChannel create(WireFormat wireFormat, URI bindAddress)
    throws JMSException
  {
    return new MulticastTransportServerChannel(wireFormat, bindAddress);
  }
}