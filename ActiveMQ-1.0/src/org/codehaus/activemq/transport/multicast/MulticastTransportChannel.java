package org.codehaus.activemq.transport.multicast;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.MulticastSocket;
import java.net.URI;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.udp.UdpTransportChannel;

public class MulticastTransportChannel extends UdpTransportChannel
{
  private static final Log log = LogFactory.getLog(MulticastTransportChannel.class);

  private boolean loopbackMode = false;

  public MulticastTransportChannel(WireFormat wireFormat, URI remoteLocation)
    throws JMSException
  {
    super(wireFormat, remoteLocation);
  }

  public MulticastTransportChannel(WireFormat wireFormat, MulticastSocket socket)
    throws JMSException
  {
    super(wireFormat, socket);
  }

  public boolean isMulticast() {
    return true;
  }

  public String toString()
  {
    return "MulticastTransportChannel: " + this.socket;
  }

  protected void connect() throws IOException {
    MulticastSocket msocket = (MulticastSocket)this.socket;

    msocket.setLoopbackMode(this.loopbackMode);

    msocket.joinGroup(this.inetAddress);
  }

  protected DatagramSocket createSocket(int port) throws IOException {
    return new MulticastSocket(port);
  }
}