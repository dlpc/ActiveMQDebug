package org.codehaus.activemq.transport.multicast;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportChannel;
import org.codehaus.activemq.transport.TransportChannelFactory;

public class MulticastTransportChannelFactory
  implements TransportChannelFactory
{
  public TransportChannel create(WireFormat wireFormat, URI remoteLocation)
    throws JMSException
  {
    return new MulticastTransportChannel(wireFormat, remoteLocation);
  }

  public TransportChannel create(WireFormat wireFormat, URI remoteLocation, URI localLocation)
    throws JMSException
  {
    return new MulticastTransportChannel(wireFormat, remoteLocation);
  }

  public boolean requiresEmbeddedBroker() {
    return true;
  }
}