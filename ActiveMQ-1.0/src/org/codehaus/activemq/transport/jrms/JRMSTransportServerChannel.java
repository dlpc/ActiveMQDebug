package org.codehaus.activemq.transport.jrms;

import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
import java.net.URI;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportServerChannelSupport;

public class JRMSTransportServerChannel extends TransportServerChannelSupport
{
  private static final Log log = LogFactory.getLog(JRMSTransportServerChannel.class);
  private WireFormat wireFormat;
  private URI bindAddress;
  private SynchronizedBoolean started;

  public JRMSTransportServerChannel(WireFormat wireFormat, URI bindAddr)
  {
    this.wireFormat = wireFormat;
    this.bindAddress = bindAddr;
    this.started = new SynchronizedBoolean(false);
    log.info("JRMS ServerChannel at: " + this.bindAddress);
  }

  public void start()
    throws JMSException
  {
    if (this.started.commit(false, true));
  }

  public String toString()
  {
    return "JRMSTransportServerChannel@" + this.bindAddress;
  }
}