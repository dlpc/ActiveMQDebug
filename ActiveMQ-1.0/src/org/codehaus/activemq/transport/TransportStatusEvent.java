package org.codehaus.activemq.transport;

import java.net.URI;

public class TransportStatusEvent
{
  public static final int CONNECTED = 1;
  public static final int DISCONNECTED = 2;
  public static final int RECONNECTED = 3;
  public static final int FAILED = 4;
  private URI remoteURI;
  private int channelStatus;

  public String toString()
  {
    return "channel: " + this.remoteURI + " has " + getStatusAsString(this.channelStatus);
  }

  private String getStatusAsString(int status) {
    String result = null;
    switch (status) {
    case 1:
      result = "connected";
      break;
    case 2:
      result = "disconnected";
      break;
    case 3:
      result = "reconnected";
      break;
    case 4:
      result = "failed";
      break;
    default:
      result = "unknown";
    }
    return result;
  }

  public int getChannelStatus()
  {
    return this.channelStatus;
  }

  public void setChannelStatus(int channelStatus)
  {
    this.channelStatus = channelStatus;
  }

  public URI getRemoteURI()
  {
    return this.remoteURI;
  }

  public void setRemoteURI(URI remoteURI)
  {
    this.remoteURI = remoteURI;
  }
}