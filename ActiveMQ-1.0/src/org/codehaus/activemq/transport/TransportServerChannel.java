package org.codehaus.activemq.transport;

import javax.jms.JMSException;
import org.codehaus.activemq.service.Service;

public abstract interface TransportServerChannel extends Service
{
  public abstract void stop();

  public abstract void start()
    throws JMSException;

  public abstract void setTransportChannelListener(TransportChannelListener paramTransportChannelListener);
}