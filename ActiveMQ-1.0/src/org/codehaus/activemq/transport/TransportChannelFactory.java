package org.codehaus.activemq.transport;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;

public abstract interface TransportChannelFactory
{
  public abstract TransportChannel create(WireFormat paramWireFormat, URI paramURI)
    throws JMSException;

  public abstract TransportChannel create(WireFormat paramWireFormat, URI paramURI1, URI paramURI2)
    throws JMSException;

  public abstract boolean requiresEmbeddedBroker();
}