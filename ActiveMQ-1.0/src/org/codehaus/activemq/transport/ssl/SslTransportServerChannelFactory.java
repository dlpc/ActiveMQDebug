package org.codehaus.activemq.transport.ssl;

import javax.net.ssl.SSLServerSocketFactory;
import org.codehaus.activemq.transport.tcp.SfTransportServerChannelFactory;

public class SslTransportServerChannelFactory extends SfTransportServerChannelFactory
{
  public SslTransportServerChannelFactory()
  {
    super(SSLServerSocketFactory.getDefault());
  }

  public SslTransportServerChannelFactory(SSLServerSocketFactory socketFactory) {
    super(socketFactory);
  }
}