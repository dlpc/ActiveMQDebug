package org.codehaus.activemq.transport.ssl;

import javax.net.ssl.SSLSocketFactory;
import org.codehaus.activemq.transport.tcp.SfTransportChannelFactory;

public class SslTransportChannelFactory extends SfTransportChannelFactory
{
  public SslTransportChannelFactory()
  {
    super(SSLSocketFactory.getDefault());
  }

  public SslTransportChannelFactory(SSLSocketFactory socketFactory) {
    super(socketFactory);
  }
}