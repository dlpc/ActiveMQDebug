package org.codehaus.activemq.transport.udp;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportChannel;
import org.codehaus.activemq.transport.TransportChannelFactory;

public class UdpTransportChannelFactory
  implements TransportChannelFactory
{
  public TransportChannel create(WireFormat wireFormat, URI remoteLocation)
    throws JMSException
  {
    return new UdpTransportChannel(wireFormat, remoteLocation);
  }

  public TransportChannel create(WireFormat wireFormat, URI remoteLocation, URI localLocation)
    throws JMSException
  {
    return new UdpTransportChannel(wireFormat, remoteLocation);
  }

  public boolean requiresEmbeddedBroker() {
    return false;
  }
}