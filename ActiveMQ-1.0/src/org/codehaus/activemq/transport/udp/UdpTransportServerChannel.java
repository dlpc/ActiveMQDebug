package org.codehaus.activemq.transport.udp;

import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
import java.net.URI;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.transport.TransportServerChannelSupport;

public class UdpTransportServerChannel extends TransportServerChannelSupport
{
  private static final Log log = LogFactory.getLog(UdpTransportServerChannel.class);
  protected URI bindAddress;
  private SynchronizedBoolean started;

  public UdpTransportServerChannel(URI bindAddr)
  {
    this.started = new SynchronizedBoolean(false);
    this.bindAddress = bindAddr;
    log.info("ServerChannel at: " + this.bindAddress);
  }

  public void start()
    throws JMSException
  {
    if (this.started.commit(false, true));
  }

  public String toString()
  {
    return "UdpTransportServerChannel@" + this.bindAddress;
  }
}