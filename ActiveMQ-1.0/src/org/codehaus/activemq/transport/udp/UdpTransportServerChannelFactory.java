package org.codehaus.activemq.transport.udp;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportServerChannel;
import org.codehaus.activemq.transport.TransportServerChannelFactory;

public class UdpTransportServerChannelFactory
  implements TransportServerChannelFactory
{
  public TransportServerChannel create(WireFormat wireFormat, URI bindAddress)
    throws JMSException
  {
    return new UdpTransportServerChannel(bindAddress);
  }
}