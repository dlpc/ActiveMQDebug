package org.codehaus.activemq.transport;

public abstract interface TransportStatusEventListener
{
  public abstract void statusChanged(TransportStatusEvent paramTransportStatusEvent);
}