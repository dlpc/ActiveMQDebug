//package org.codehaus.activemq.transport.gnet;
//
//import java.net.URI;
//import javax.jms.JMSException;
//import org.apache.geronimo.network.SelectorManager;
//import org.apache.geronimo.pool.ClockPool;
//import org.apache.geronimo.pool.ThreadPool;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportServerChannel;
//import org.codehaus.activemq.transport.TransportServerChannelFactory;
//
//public class GTransportServerChannelFactory
//  implements TransportServerChannelFactory
//{
//  private static ThreadPool threadPool;
//  private static ClockPool clockPool;
//  private static SelectorManager selectorManager;
//
//  public static void init(SelectorManager sm, ThreadPool tp, ClockPool cp)
//    throws IllegalArgumentException
//  {
//    if ((sm == null) || (tp == null) || (cp == null)) {
//      throw new IllegalArgumentException();
//    }
//
//    selectorManager = sm;
//    threadPool = tp;
//    clockPool = cp;
//  }
//
//  private static void init()
//    throws Exception
//  {
//    if (threadPool != null) {
//      return;
//    }
//
//    threadPool = new ThreadPool();
//    threadPool.setKeepAliveTime(1000L);
//    threadPool.setPoolSize(5);
//    threadPool.setPoolName("S Pool");
//
//    clockPool = new ClockPool();
//    clockPool.setPoolName("S Clock");
//
//    selectorManager = new SelectorManager();
//    selectorManager.setThreadPool(threadPool);
//    selectorManager.setThreadName("S Manager");
//    selectorManager.setTimeout(1000L);
//
//    threadPool.doStart();
//    clockPool.doStart();
//    selectorManager.doStart();
//  }
//
//  public TransportServerChannel create(WireFormat wireFormat, URI bindAddress)
//    throws JMSException
//  {
//    JMSException ex;
//    try
//    {
//      init();
//      return new GTransportServerChannel(wireFormat, bindAddress, selectorManager, threadPool, clockPool);
//    }
//    catch (Exception e) {
//      e.printStackTrace();
//      ex = new JMSException(e.getMessage());
//      ex.setLinkedException(e);
//    }throw ex;
//  }
//}