//package org.codehaus.activemq.transport.gnet;
//
//import EDU.oswego.cs.dl.util.concurrent.Latch;
//import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
//import java.net.URI;
//import javax.jms.JMSException;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.apache.geronimo.network.SelectorManager;
//import org.apache.geronimo.network.protocol.AcceptableProtocol;
//import org.apache.geronimo.network.protocol.ProtocolFactory;
//import org.apache.geronimo.network.protocol.ProtocolFactory.AcceptedCallBack;
//import org.apache.geronimo.network.protocol.ServerSocketAcceptor;
//import org.apache.geronimo.network.protocol.SocketProtocol;
//import org.apache.geronimo.pool.ClockPool;
//import org.apache.geronimo.pool.ThreadPool;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportServerChannel;
//import org.codehaus.activemq.transport.TransportServerChannelSupport;
//
//public class GTransportServerChannel extends TransportServerChannelSupport
//  implements TransportServerChannel
//{
//  protected static final int BACKLOG = 500;
//  private static final Log log = LogFactory.getLog(GTransportServerChannel.class);
//  private WireFormat wireFormat;
//  protected String bindAddressURI;
//  private SynchronizedBoolean closed;
//  private ThreadPool tp;
//  private ClockPool cp;
//  private SelectorManager sm;
//  private ServerSocketAcceptor ssa;
//  private ProtocolFactory pf;
//  private Latch startLatch;
//
//  public GTransportServerChannel(WireFormat wireFormat, URI bindAddr, SelectorManager selectorManager, ThreadPool threadPool, ClockPool clockPool)
//    throws Exception
//  {
//    this.wireFormat = wireFormat;
//    this.sm = selectorManager;
//    this.tp = threadPool;
//    this.cp = clockPool;
//
//    this.closed = new SynchronizedBoolean(false);
//    this.startLatch = new Latch();
//
//    SocketProtocol spt = new SocketProtocol();
//    spt.setTimeout(30000L);
//    spt.setSelectorManager(this.sm);
//
//    this.pf = new ProtocolFactory();
//    this.pf.setClockPool(this.cp);
//    this.pf.setMaxAge(9223372036854775807L);
//    this.pf.setMaxInactivity(9223372036854775807L);
//
//    this.pf.setReclaimPeriod(10000L);
//    this.pf.setTemplate(spt);
//
//    this.pf.setAcceptedCallBack(createAcceptedCallBack());
//
//    this.ssa = new ServerSocketAcceptor();
//    this.ssa.setSelectorManager(this.sm);
//    this.ssa.setTimeOut(5000);
//    this.ssa.setUri(bindAddr);
//    this.ssa.setAcceptorListener(this.pf);
//  }
//
//  private ProtocolFactory.AcceptedCallBack createAcceptedCallBack()
//  {
//    return new ProtocolFactory.AcceptedCallBack()
//    {
//      public void accepted(AcceptableProtocol p) {
//        try {
//          GTransportServerChannel.this.startLatch.acquire();
//
//          if (p != null) {
//            GTransportChannel channel = new GTransportChannel(GTransportServerChannel.this.wireFormat, p, GTransportServerChannel.this.tp);
//            GTransportServerChannel.this.addClient(channel);
//          }
//        }
//        catch (Exception e)
//        {
//          GTransportServerChannel.log.error("Caught while attempting to add new protocol: " + e, e);
//        }
//      }
//    };
//  }
//
//  public void stop()
//  {
//    if (this.closed.commit(false, true)) {
//      super.stop();
//      try {
//        this.ssa.drain();
//        this.pf.drain();
//      }
//      catch (Throwable e) {
//        log.trace("error closing GTransportServerChannel", e);
//      }
//    }
//  }
//
//  public void start()
//    throws JMSException
//  {
//    super.start();
//    try {
//      this.ssa.startup();
//    }
//    catch (Exception e) {
//      JMSException jmsEx = new JMSException("Could not start ServerSocketAcceptor: " + e);
//      jmsEx.setLinkedException(e);
//      throw jmsEx;
//    }
//    this.startLatch.release();
//  }
//
//  public String toString()
//  {
//    return "GTransportServerChannel@" + this.bindAddressURI;
//  }
//}