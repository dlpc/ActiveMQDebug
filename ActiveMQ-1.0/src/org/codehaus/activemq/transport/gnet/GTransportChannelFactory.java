//package org.codehaus.activemq.transport.gnet;
//
//import java.net.URI;
//import javax.jms.JMSException;
//import org.apache.geronimo.network.SelectorManager;
//import org.apache.geronimo.pool.ClockPool;
//import org.apache.geronimo.pool.ThreadPool;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportChannel;
//import org.codehaus.activemq.transport.TransportChannelFactory;
//
//public class GTransportChannelFactory
//  implements TransportChannelFactory
//{
//  private static ThreadPool threadPool;
//  private static ClockPool clockPool;
//  private static SelectorManager selectorManager;
//
//  public static void init(SelectorManager sm, ThreadPool tp, ClockPool cp)
//    throws IllegalArgumentException
//  {
//    if ((sm == null) || (tp == null) || (cp == null)) {
//      throw new IllegalArgumentException();
//    }
//
//    selectorManager = sm;
//    threadPool = tp;
//    clockPool = cp;
//  }
//
//  private static void init()
//    throws Exception
//  {
//    if (threadPool != null) {
//      return;
//    }
//
//    threadPool = new ThreadPool();
//    threadPool.setKeepAliveTime(1000L);
//    threadPool.setPoolSize(5);
//    threadPool.setPoolName("C Pool");
//
//    clockPool = new ClockPool();
//    clockPool.setPoolName("C Clock");
//
//    selectorManager = new SelectorManager();
//    selectorManager.setThreadPool(threadPool);
//    selectorManager.setThreadName("C Manager");
//    selectorManager.setTimeout(1000L);
//
//    threadPool.doStart();
//    clockPool.doStart();
//    selectorManager.doStart();
//  }
//
//  public TransportChannel create(WireFormat wireFormat, URI remoteLocation)
//    throws JMSException
//  {
//    return create(null, remoteLocation);
//  }
//
//  public TransportChannel create(WireFormat wireFormat, URI remoteLocation, URI localLocation)
//    throws JMSException
//  {
//    JMSException ex;
//    try
//    {
//      init();
//      return new GTransportChannel(wireFormat, remoteLocation, localLocation, selectorManager, threadPool, clockPool);
//    }
//    catch (Exception e) {
//      ex = new JMSException(e.getMessage());
//      ex.setLinkedException(e);
//    }throw ex;
//  }
//
//  public boolean requiresEmbeddedBroker()
//  {
//    return false;
//  }
//}