//package org.codehaus.activemq.transport.ember;
//
//import java.io.IOException;
//import java.net.URI;
//import javax.jms.JMSException;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportChannel;
//import org.codehaus.activemq.transport.TransportChannelFactory;
//import org.codehaus.activemq.util.IdGenerator;
//import pyrasun.eio.EIOGlobalContext;
//import pyrasun.eio.EIOPoolingStrategy;
//import pyrasun.eio.services.EmberServiceController;
//import pyrasun.eio.services.EmberServiceException;
//import pyrasun.eio.services.bytearray.ByteArrayClientService;
//import pyrasun.eio.services.bytearray.ByteArrayServerClient;
//
//public class EmberTransportChannelFactory extends EmberSupport
//  implements TransportChannelFactory
//{
//  protected static final Log log = LogFactory.getLog(EmberTransportChannelFactory.class);
//
//  private IdGenerator idGenerator = new IdGenerator();
//
//  public EmberTransportChannelFactory() {
//  }
//
//  public EmberTransportChannelFactory(EIOGlobalContext context, EIOPoolingStrategy ioPoolingStrategy) {
//    super(context, ioPoolingStrategy);
//  }
//
//  public TransportChannel create(WireFormat wireFormat, URI remoteLocation)
//    throws JMSException
//  {
//    JMSException jmsEx;
//    try
//    {
//      String id = this.idGenerator.generateId();
//      EmberServiceController controller = getController();
//
//      ByteArrayServerClient client = createClient(controller, remoteLocation, id);
//      return new EmberTransportChannel(wireFormat, getContext(), controller, client);
//    }
//    catch (IOException ioe) {
//      JMSException jmsEx = new JMSException("Initialization of TransportChannel failed: " + ioe);
//      jmsEx.setLinkedException(ioe);
//      throw jmsEx;
//    }
//    catch (EmberServiceException e) {
//      jmsEx = new JMSException("Initialization of TransportChannel failed: " + e);
//      jmsEx.setLinkedException(e);
//    }throw jmsEx;
//  }
//
//  public TransportChannel create(WireFormat wireFormat, URI remoteLocation, URI localLocation)
//    throws JMSException
//  {
//    return create(wireFormat, remoteLocation);
//  }
//
//  public boolean requiresEmbeddedBroker() {
//    return false;
//  }
//
//  protected ByteArrayServerClient createClient(EmberServiceController controller, URI remoteLocation, String id)
//    throws JMSException, EmberServiceException, IOException
//  {
//    ByteArrayClientService service = createNioService(controller);
//    ByteArrayServerClient client = service.createClient(remoteLocation.getHost(), remoteLocation.getPort(), id, null);
//    return client;
//  }
//
//  protected ByteArrayClientService createNioService(EmberServiceController controller)
//    throws JMSException
//  {
//    try
//    {
//      ByteArrayClientService service = new ByteArrayClientService(getContext(), getIoPoolingStrategy());
//      controller.addService(service);
//    }
//    catch (IOException e) {
//      throw createJMSException("Creation of NIO service failed: ", e);
//    }
//    ByteArrayClientService service;
//    return service;
//  }
//}