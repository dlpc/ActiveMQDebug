//package org.codehaus.activemq.transport.ember;
//
//import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
//import java.net.URI;
//import javax.jms.JMSException;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportServerChannelSupport;
//import pyrasun.eio.EIOGlobalContext;
//import pyrasun.eio.services.EmberServiceController;
//import pyrasun.eio.services.EmberServiceException;
//import pyrasun.eio.services.bytearray.ByteArrayServerClient;
//import pyrasun.eio.services.bytearray.ByteArrayServerClientListener;
//import pyrasun.eio.services.bytearray.ByteArrayServerListener;
//
//public class EmberTransportServerChannel extends TransportServerChannelSupport
//  implements ByteArrayServerListener, ByteArrayServerClientListener
//{
//  private static final Log log = LogFactory.getLog(EmberTransportServerChannel.class);
//  private WireFormat wireFormat;
//  private EIOGlobalContext context;
//  private EmberServiceController controller;
//  protected URI bindAddress;
//  private SynchronizedBoolean closed;
//  private SynchronizedBoolean started;
//
//  public EmberTransportServerChannel(WireFormat wireFormat, URI bindAddr, EIOGlobalContext context, EmberServiceController controller)
//  {
//    this.wireFormat = wireFormat;
//    this.bindAddress = bindAddr;
//    this.context = context;
//    this.controller = controller;
//    this.closed = new SynchronizedBoolean(false);
//    this.started = new SynchronizedBoolean(false);
//    log.info("ServerChannel at: " + this.bindAddress);
//  }
//
//  public void stop()
//  {
//    if (this.closed.commit(false, true))
//      try {
//        this.controller.stopAll();
//        this.context.stop();
//      }
//      catch (EmberServiceException e) {
//        log.error("Caught while closing: " + e, e);
//      }
//  }
//
//  public void start()
//    throws JMSException
//  {
//    super.start();
//    if (this.started.commit(false, true)) {
//      super.stop();
//      try {
//        this.context.start();
//        this.controller.startAll();
//      }
//      catch (EmberServiceException e) {
//        JMSException jmsEx = new JMSException("Could not start EmberIOController: " + e);
//        jmsEx.setLinkedException(e);
//        throw jmsEx;
//      }
//    }
//  }
//
//  public String toString()
//  {
//    return "EmberTransportServerChannel@" + this.bindAddress;
//  }
//
//  protected void handleException(ByteArrayServerClient client, JMSException e) {
//    log.error("Could not create new TransportChannel for client: " + client, e);
//  }
//
//  public void newClient(ByteArrayServerClient client) {
//    log.trace("New client received!");
//
//    addClient(new EmberTransportChannel(this.wireFormat, null, null, client));
//  }
//
//  public void clientClosed(ByteArrayServerClient client) {
//    log.info("Client has disconnected: " + client);
//  }
//
//  public void newMessage(ByteArrayServerClient byteArrayServerClient, Object msg)
//  {
//    log.warn("New message received!: " + msg);
//  }
//}