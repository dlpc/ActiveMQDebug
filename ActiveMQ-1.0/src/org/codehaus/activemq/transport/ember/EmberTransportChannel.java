//package org.codehaus.activemq.transport.ember;
//
//import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
//import java.io.IOException;
//import javax.jms.JMSException;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.codehaus.activemq.message.Packet;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.AbstractTransportChannel;
//import pyrasun.eio.EIOGlobalContext;
//import pyrasun.eio.services.EmberServiceController;
//import pyrasun.eio.services.EmberServiceException;
//import pyrasun.eio.services.bytearray.ByteArrayServerClient;
//import pyrasun.eio.services.bytearray.ByteArrayServerClientListener;
//
//public class EmberTransportChannel extends AbstractTransportChannel
//  implements ByteArrayServerClientListener
//{
//  private static final Log log = LogFactory.getLog(EmberTransportChannel.class);
//  private WireFormat wireFormat;
//  private EIOGlobalContext context;
//  private EmberServiceController controller;
//  private ByteArrayServerClient client;
//  private SynchronizedBoolean closed;
//  private SynchronizedBoolean started;
//
//  protected EmberTransportChannel(WireFormat wireFormat)
//  {
//    this.wireFormat = wireFormat;
//
//    this.closed = new SynchronizedBoolean(false);
//    this.started = new SynchronizedBoolean(false);
//  }
//
//  public EmberTransportChannel(WireFormat wireFormat, EIOGlobalContext context, EmberServiceController controller, ByteArrayServerClient client)
//  {
//    this(wireFormat);
//    this.context = context;
//    this.client = client;
//    this.controller = controller;
//    client.setListener(this);
//  }
//
//  public void stop()
//  {
//    super.stop();
//    if (this.closed.commit(false, true))
//      try
//      {
//        if (this.controller != null) {
//          this.controller.stopAll();
//        }
//        if (this.context != null)
//          this.context.stop();
//      }
//      catch (EmberServiceException e)
//      {
//        log.error("Caught while closing: " + e, e);
//      }
//  }
//
//  public void start()
//    throws JMSException
//  {
//    if (this.started.commit(false, true))
//    {
//      try
//      {
//        if (this.context != null) {
//          this.context.start();
//        }
//        if (this.controller != null)
//          this.controller.startAll();
//      }
//      catch (EmberServiceException e)
//      {
//        JMSException jmsEx = new JMSException("Error starting NIO client: " + e.getMessage());
//        jmsEx.setLinkedException(e);
//        throw jmsEx;
//      }
//    }
//  }
//
//  public void asyncSend(Packet packet)
//    throws JMSException
//  {
//    try
//    {
//      byte[] bytes = this.wireFormat.toBytes(packet);
//
//      synchronized (this.client) {
//        this.client.write(bytes);
//      }
//    }
//    catch (IOException e) {
//      throw createJMSException("Failed to write packet: " + packet + ". ", e);
//    }
//  }
//
//  public boolean isMulticast()
//  {
//    return false;
//  }
//
//  protected JMSException createJMSException(String message, Exception ex)
//  {
//    JMSException jmsEx = new JMSException(message + ex.getMessage());
//    jmsEx.setLinkedException(ex);
//    return jmsEx;
//  }
//
//  public String toString()
//  {
//    return "EmberTransportChannel: " + this.client;
//  }
//
//  public void newMessage(ByteArrayServerClient client, Object msg) {
//    byte[] bytes = (byte[])msg;
//    Packet packet = null;
//    try {
//      packet = this.wireFormat.fromBytes(bytes);
//      doConsumePacket(packet);
//    }
//    catch (IOException e) {
//      log.error("Could not parse byte[] of size: " + bytes.length + ". Reason: " + e, e);
//    }
//  }
//}