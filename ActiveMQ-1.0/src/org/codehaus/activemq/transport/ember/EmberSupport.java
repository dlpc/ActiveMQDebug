//package org.codehaus.activemq.transport.ember;
//
//import java.io.IOException;
//import javax.jms.JMSException;
//import pyrasun.eio.EIOEvent;
//import pyrasun.eio.EIOEventDescriptor;
//import pyrasun.eio.EIOGlobalContext;
//import pyrasun.eio.EIOPoolingStrategy;
//import pyrasun.eio.services.EmberServiceController;
//import pyrasun.eio.services.EmberServiceControllerImpl;
//
//public class EmberSupport
//{
//  private EIOGlobalContext context;
//  private String key = "emberIo";
//  private int maxEm = 1;
//  private EmberServiceControllerImpl controller;
//  private EIOPoolingStrategy ioPoolingStrategy;
//  private String poolingStrategyName = "SELECTOR_READER";
//
//  public EmberSupport() {
//  }
//
//  public EmberSupport(EIOGlobalContext context, EIOPoolingStrategy ioPoolingStrategy) {
//    this.context = context;
//    this.ioPoolingStrategy = ioPoolingStrategy;
//  }
//
//  public EIOGlobalContext getContext() throws IOException {
//    if (this.context == null) {
//      this.context = new EIOGlobalContext(this.maxEm, this.key);
//    }
//    return this.context;
//  }
//
//  public String getKey() {
//    return this.key;
//  }
//
//  protected EmberServiceController getController() throws IOException {
//    if (this.controller == null) {
//      this.controller = new EmberServiceControllerImpl(getContext());
//    }
//    return this.controller;
//  }
//
//  protected EIOPoolingStrategy getIoPoolingStrategy() {
//    if (this.ioPoolingStrategy == null) {
//      this.ioPoolingStrategy = getPoolingStrategyByName(this.poolingStrategyName);
//    }
//    return this.ioPoolingStrategy;
//  }
//
//  protected EIOPoolingStrategy getPoolingStrategyByName(String name) {
//    EIOPoolingStrategy strategy = EIOPoolingStrategy.getStrategyByName(name);
//
//    EIOEventDescriptor evRead = strategy.getEventDescriptor(EIOEvent.READ);
//    evRead.setPoolSize(1);
//
//    EIOEventDescriptor evWrite = strategy.getEventDescriptor(EIOEvent.WRITE);
//    evWrite.setPoolSize(1);
//
//    EIOEventDescriptor evProcess = strategy.getEventDescriptor(EIOEvent.PROCESS);
//    evProcess.setPoolSize(1);
//    return strategy;
//  }
//
//  protected JMSException createJMSException(String message, Exception ex)
//  {
//    JMSException jmsEx = new JMSException(message + ex.getMessage());
//    jmsEx.setLinkedException(ex);
//    return jmsEx;
//  }
//}