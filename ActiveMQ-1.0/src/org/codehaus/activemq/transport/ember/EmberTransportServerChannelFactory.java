//package org.codehaus.activemq.transport.ember;
//
//import java.io.IOException;
//import java.net.URI;
//import javax.jms.JMSException;
//import org.codehaus.activemq.message.WireFormat;
//import org.codehaus.activemq.transport.TransportServerChannel;
//import org.codehaus.activemq.transport.TransportServerChannelFactory;
//import pyrasun.eio.EIOGlobalContext;
//import pyrasun.eio.EIOPoolingStrategy;
//import pyrasun.eio.services.EmberServiceController;
//import pyrasun.eio.services.bytearray.ByteArrayServerService;
//
//public class EmberTransportServerChannelFactory extends EmberSupport
//  implements TransportServerChannelFactory
//{
//  private EIOPoolingStrategy acceptPoolingStrategy;
//  private String acceptPoolingStrategyName = "BLOCKING_ACCEPTOR";
//
//  public EmberTransportServerChannelFactory() {
//  }
//
//  public EmberTransportServerChannelFactory(EIOGlobalContext context, EIOPoolingStrategy ioPoolingStrategy, EIOPoolingStrategy acceptPoolingStrategy) {
//    super(context, ioPoolingStrategy);
//    this.acceptPoolingStrategy = acceptPoolingStrategy;
//  }
//
//  public TransportServerChannel create(WireFormat wireFormat, URI bindAddress)
//    throws JMSException
//  {
//    try
//    {
//      EmberServiceController controller = getController();
//      ByteArrayServerService service = new ByteArrayServerService(getContext(), getAcceptPoolingStrategy(), getIoPoolingStrategy(), bindAddress.getHost(), bindAddress.getPort());
//      controller.addService(service);
//
//      EmberTransportServerChannel answer = new EmberTransportServerChannel(wireFormat, bindAddress, getContext(), controller);
//      service.setListener(answer);
//      return answer;
//    } catch (IOException e) {
//    }
//    throw createJMSException("Initialization of TransportServerChannel failed: ", e);
//  }
//
//  protected EIOPoolingStrategy getAcceptPoolingStrategy()
//  {
//    if (this.acceptPoolingStrategy == null) {
//      this.acceptPoolingStrategy = getPoolingStrategyByName(this.acceptPoolingStrategyName);
//    }
//    return this.acceptPoolingStrategy;
//  }
//}