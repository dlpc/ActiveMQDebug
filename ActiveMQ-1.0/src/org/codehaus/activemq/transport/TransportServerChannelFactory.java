package org.codehaus.activemq.transport;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;

public abstract interface TransportServerChannelFactory
{
  public abstract TransportServerChannel create(WireFormat paramWireFormat, URI paramURI)
    throws JMSException;
}