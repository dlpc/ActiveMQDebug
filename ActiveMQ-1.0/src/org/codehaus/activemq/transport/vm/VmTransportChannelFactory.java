package org.codehaus.activemq.transport.vm;

import java.net.URI;
import javax.jms.JMSException;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.transport.TransportChannel;
import org.codehaus.activemq.transport.TransportChannelFactory;

public class VmTransportChannelFactory
  implements TransportChannelFactory
{
  public TransportChannel create(WireFormat wireFormat, URI remoteLocation)
    throws JMSException
  {
    return new VmTransportChannel();
  }

  public TransportChannel create(WireFormat wireFormat, URI remoteLocation, URI localLocation)
    throws JMSException
  {
    return create(wireFormat, remoteLocation);
  }

  public boolean requiresEmbeddedBroker() {
    return true;
  }
}