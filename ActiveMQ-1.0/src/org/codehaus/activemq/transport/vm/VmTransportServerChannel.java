package org.codehaus.activemq.transport.vm;

import EDU.oswego.cs.dl.util.concurrent.SynchronizedBoolean;
import java.net.URI;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.transport.TransportServerChannelSupport;

public class VmTransportServerChannel extends TransportServerChannelSupport
{
  private static final Log log = LogFactory.getLog(VmTransportServerChannel.class);
  private SynchronizedBoolean started;
  private URI bindAddress;

  public VmTransportServerChannel(URI bindAddr)
  {
    this.started = new SynchronizedBoolean(false);
    this.bindAddress = bindAddr;
    log.info("Listening for connections at: " + this.bindAddress);
  }

  public void start()
    throws JMSException
  {
    if (this.started.commit(false, true));
  }

  public String toString()
  {
    return "VmTransportServerChannel@" + this.bindAddress;
  }
}