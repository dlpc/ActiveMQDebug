package org.codehaus.activemq.transport;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import org.codehaus.activemq.message.Packet;
import org.codehaus.activemq.message.PacketListener;
import org.codehaus.activemq.message.Receipt;
import org.codehaus.activemq.service.Service;

public abstract interface TransportChannel extends Service
{
  public abstract void stop();

  public abstract void start()
    throws JMSException;

  public abstract Receipt send(Packet paramPacket)
    throws JMSException;

  public abstract Receipt send(Packet paramPacket, int paramInt)
    throws JMSException;

  public abstract void asyncSend(Packet paramPacket)
    throws JMSException;

  public abstract void setPacketListener(PacketListener paramPacketListener);

  public abstract void setExceptionListener(ExceptionListener paramExceptionListener);

  public abstract boolean isMulticast();

  public abstract void addTransportStatusEventListener(TransportStatusEventListener paramTransportStatusEventListener);

  public abstract void removeTransportStatusEventListener(TransportStatusEventListener paramTransportStatusEventListener);

  public abstract void setClientID(String paramString);

  public abstract String getClientID();

  public abstract void setTransportChannelListener(TransportChannelListener paramTransportChannelListener);
}