package org.codehaus.activemq.transport;

public abstract interface TransportChannelListener
{
  public abstract void addClient(TransportChannel paramTransportChannel);

  public abstract void removeClient(TransportChannel paramTransportChannel);
}