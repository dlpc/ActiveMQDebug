package org.codehaus.activemq;

import org.codehaus.activemq.message.ActiveMQMessage;

public abstract interface ActiveMQMessageDispatcher
{
  public abstract boolean isTarget(ActiveMQMessage paramActiveMQMessage);

  public abstract void dispatch(ActiveMQMessage paramActiveMQMessage);
}