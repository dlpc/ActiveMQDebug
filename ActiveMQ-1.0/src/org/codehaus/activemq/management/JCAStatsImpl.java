package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.JCAConnectionPoolStats;
import javax.management.j2ee.statistics.JCAConnectionStats;
import javax.management.j2ee.statistics.JCAStats;

public class JCAStatsImpl extends StatsImpl
  implements JCAStats
{
  private JCAConnectionStats[] connectionStats;
  private JCAConnectionPoolStats[] connectionPoolStats;

  public JCAStatsImpl(JCAConnectionStats[] connectionStats, JCAConnectionPoolStats[] connectionPoolStats)
  {
    this.connectionStats = connectionStats;
    this.connectionPoolStats = connectionPoolStats;
  }

  public JCAConnectionStats[] getConnections() {
    return this.connectionStats;
  }

  public JCAConnectionPoolStats[] getConnectionPools() {
    return this.connectionPoolStats;
  }
}