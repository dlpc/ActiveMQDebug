package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.CountStatistic;

public class CountStatisticImpl extends StatisticImpl
  implements CountStatistic
{
  private long count;
  private CountStatisticImpl parent;

  public CountStatisticImpl(CountStatisticImpl parent, String name, String description)
  {
    this(name, description);
    this.parent = parent;
  }

  public CountStatisticImpl(String name, String description) {
    this(name, "count", description);
  }

  public CountStatisticImpl(String name, String unit, String description) {
    super(name, unit, description);
  }

  public void reset() {
    super.reset();
    this.count = 0L;
  }

  public long getCount() {
    return this.count;
  }

  public void increment() {
    this.count += 1L;
    updateSampleTime();
    if (this.parent != null)
      this.parent.increment();
  }

  public void decrement()
  {
    this.count -= 1L;
    updateSampleTime();
    if (this.parent != null)
      this.parent.decrement();
  }

  public CountStatisticImpl getParent()
  {
    return this.parent;
  }

  public void setParent(CountStatisticImpl parent) {
    this.parent = parent;
  }

  protected void appendFieldDescription(StringBuffer buffer) {
    buffer.append(" count: ");
    buffer.append(Long.toString(this.count));
    super.appendFieldDescription(buffer);
  }
}