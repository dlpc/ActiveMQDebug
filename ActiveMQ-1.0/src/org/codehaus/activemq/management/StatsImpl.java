package org.codehaus.activemq.management;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.management.j2ee.statistics.Statistic;
import javax.management.j2ee.statistics.Stats;

public class StatsImpl
  implements Stats, Resettable
{
  private Map map = new HashMap();

  public StatsImpl() {
  }

  public StatsImpl(Map map) {
    this.map = map;
  }

  public void reset() {
    Statistic[] stats = getStatistics();
    int i = 0; for (int size = stats.length; i < size; i++) {
      Statistic stat = stats[i];
      if ((stat instanceof Resettable)) {
        Resettable r = (Resettable)stat;
        r.reset();
      }
    }
  }

  public Statistic getStatistic(String name) {
    return (Statistic)this.map.get(name);
  }

  public String[] getStatisticNames() {
    Set keys = this.map.keySet();
    String[] answer = new String[keys.size()];
    keys.toArray(answer);
    return answer;
  }

  public Statistic[] getStatistics() {
    Collection values = this.map.values();
    Statistic[] answer = new Statistic[values.size()];
    values.toArray(answer);
    return answer;
  }

  protected void addStatistic(String name, Statistic statistic) {
    this.map.put(name, statistic);
  }
}