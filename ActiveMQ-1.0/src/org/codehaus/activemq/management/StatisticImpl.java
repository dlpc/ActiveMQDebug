package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.Statistic;

public class StatisticImpl
  implements Statistic, Resettable
{
  private String name;
  private String unit;
  private String description;
  private long startTime;
  private long lastSampleTime;

  public StatisticImpl(String name, String unit, String description)
  {
    this.name = name;
    this.unit = unit;
    this.description = description;
    this.startTime = System.currentTimeMillis();
    this.lastSampleTime = this.startTime;
  }

  public void reset() {
    this.startTime = System.currentTimeMillis();
    this.lastSampleTime = this.startTime;
  }

  protected void updateSampleTime() {
    this.lastSampleTime = System.currentTimeMillis();
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append(this.name);
    buffer.append("{");
    appendFieldDescription(buffer);
    buffer.append(" }");
    return buffer.toString();
  }

  public String getName() {
    return this.name;
  }

  public String getUnit() {
    return this.unit;
  }

  public String getDescription() {
    return this.description;
  }

  public long getStartTime() {
    return this.startTime;
  }

  public long getLastSampleTime() {
    return this.lastSampleTime;
  }

  protected void appendFieldDescription(StringBuffer buffer) {
    buffer.append(" unit: ");
    buffer.append(this.unit);
    buffer.append(" startTime: ");

    buffer.append(this.startTime);
    buffer.append(" lastSampleTime: ");

    buffer.append(this.lastSampleTime);
    buffer.append(" description: ");
    buffer.append(this.description);
  }
}