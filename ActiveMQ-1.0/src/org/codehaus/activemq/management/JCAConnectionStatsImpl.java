package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.JCAConnectionStats;
import javax.management.j2ee.statistics.TimeStatistic;

public class JCAConnectionStatsImpl extends StatsImpl
  implements JCAConnectionStats
{
  private String connectionFactory;
  private String managedConnectionFactory;
  private TimeStatistic waitTime;
  private TimeStatistic useTime;

  public JCAConnectionStatsImpl(String connectionFactory, String managedConnectionFactory, TimeStatistic waitTime, TimeStatistic useTime)
  {
    this.connectionFactory = connectionFactory;
    this.managedConnectionFactory = managedConnectionFactory;
    this.waitTime = waitTime;
    this.useTime = useTime;

    addStatistic("waitTime", waitTime);
    addStatistic("useTime", useTime);
  }

  public String getConnectionFactory() {
    return this.connectionFactory;
  }

  public String getManagedConnectionFactory() {
    return this.managedConnectionFactory;
  }

  public TimeStatistic getWaitTime() {
    return this.waitTime;
  }

  public TimeStatistic getUseTime() {
    return this.useTime;
  }
}