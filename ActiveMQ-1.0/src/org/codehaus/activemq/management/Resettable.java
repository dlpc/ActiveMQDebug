package org.codehaus.activemq.management;

public abstract interface Resettable
{
  public abstract void reset();
}