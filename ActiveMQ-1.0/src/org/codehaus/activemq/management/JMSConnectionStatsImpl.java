package org.codehaus.activemq.management;

import java.util.List;
import javax.management.j2ee.statistics.JMSConnectionStats;
import javax.management.j2ee.statistics.JMSSessionStats;
import org.codehaus.activemq.ActiveMQSession;
import org.codehaus.activemq.util.IndentPrinter;

public class JMSConnectionStatsImpl extends StatsImpl
  implements JMSConnectionStats
{
  private List sessions;
  private boolean transactional;

  public JMSConnectionStatsImpl(List sessions, boolean transactional)
  {
    this.sessions = sessions;
    this.transactional = transactional;
  }

  public JMSSessionStats[] getSessions()
  {
    Object[] sessionArray = this.sessions.toArray();
    int size = sessionArray.length;
    JMSSessionStats[] answer = new JMSSessionStats[size];
    for (int i = 0; i < size; i++) {
      ActiveMQSession session = (ActiveMQSession)sessionArray[i];
      answer[i] = session.getSessionStats();
    }
    return answer;
  }

  public boolean isTransactional() {
    return this.transactional;
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer("connection{ ");
    JMSSessionStats[] array = getSessions();
    for (int i = 0; i < array.length; i++) {
      if (i > 0) {
        buffer.append(", ");
      }
      buffer.append(Integer.toString(i));
      buffer.append(" = ");
      buffer.append(array[i]);
    }
    buffer.append(" }");
    return buffer.toString();
  }

  public void dump(IndentPrinter out) {
    out.printIndent();
    out.println("connection {");
    out.incrementIndent();
    JMSSessionStats[] array = getSessions();
    for (int i = 0; i < array.length; i++) {
      JMSSessionStatsImpl sessionStat = (JMSSessionStatsImpl)array[i];
      out.printIndent();
      out.println("session {");
      out.incrementIndent();
      sessionStat.dump(out);
      out.decrementIndent();
      out.printIndent();
      out.println("}");
    }
    out.decrementIndent();
    out.printIndent();
    out.println("}");
    out.flush();
  }
}