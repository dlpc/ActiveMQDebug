package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.BoundaryStatistic;

public class BoundaryStatisticImpl extends StatisticImpl
  implements BoundaryStatistic
{
  private long lowerBound;
  private long upperBound;

  public BoundaryStatisticImpl(String name, String unit, String description, long lowerBound, long upperBound)
  {
    super(name, unit, description);
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
  }

  public long getLowerBound() {
    return this.lowerBound;
  }

  public long getUpperBound() {
    return this.upperBound;
  }

  protected void appendFieldDescription(StringBuffer buffer) {
    buffer.append(" lowerBound: ");
    buffer.append(Long.toString(this.lowerBound));
    buffer.append(" upperBound: ");
    buffer.append(Long.toString(this.upperBound));
    super.appendFieldDescription(buffer);
  }
}