package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.TimeStatistic;

public class TimeStatisticImpl extends StatisticImpl
  implements TimeStatistic
{
  private long count;
  private long maxTime;
  private long minTime;
  private long totalTime;
  private TimeStatisticImpl parent;

  public TimeStatisticImpl(String name, String description)
  {
    this(name, "millis", description);
  }

  public TimeStatisticImpl(TimeStatisticImpl parent, String name, String description) {
    this(name, description);
    this.parent = parent;
  }

  public TimeStatisticImpl(String name, String unit, String description) {
    super(name, unit, description);
  }

  public void reset() {
    super.reset();
    this.count = 0L;
    this.maxTime = 0L;
    this.minTime = 0L;
    this.totalTime = 0L;
  }

  public long getCount() {
    return this.count;
  }

  public void addTime(long time) {
    this.count += 1L;
    this.totalTime += time;
    if (time > this.maxTime) {
      this.maxTime = time;
    }
    if ((time < this.minTime) || (this.minTime == 0L)) {
      this.minTime = time;
    }
    updateSampleTime();
    if (this.parent != null)
      this.parent.addTime(time);
  }

  public long getMaxTime()
  {
    return this.maxTime;
  }

  public long getMinTime()
  {
    return this.minTime;
  }

  public long getTotalTime()
  {
    return this.totalTime;
  }

  public double getAverageTime()
  {
    if (this.count == 0L) {
      return 0.0D;
    }
    double d = this.totalTime;
    return d / this.count;
  }

  public double getAverageTimeExcludingMinMax()
  {
    if (this.count <= 2L) {
      return 0.0D;
    }
    double d = this.totalTime - this.minTime - this.maxTime;
    return d / (this.count - 2L);
  }

  public double getAveragePerSecond()
  {
    double d = 1000.0D;
    return d / getAverageTime();
  }

  public double getAveragePerSecondExcludingMinMax()
  {
    double d = 1000.0D;
    return d / getAverageTimeExcludingMinMax();
  }

  public TimeStatisticImpl getParent() {
    return this.parent;
  }

  public void setParent(TimeStatisticImpl parent) {
    this.parent = parent;
  }

  protected void appendFieldDescription(StringBuffer buffer) {
    buffer.append(" count: ");
    buffer.append(Long.toString(this.count));
    buffer.append(" maxTime: ");
    buffer.append(Long.toString(this.maxTime));
    buffer.append(" minTime: ");
    buffer.append(Long.toString(this.minTime));
    buffer.append(" totalTime: ");
    buffer.append(Long.toString(this.totalTime));
    buffer.append(" averageTime: ");
    buffer.append(Double.toString(getAverageTime()));
    buffer.append(" averageTimeExMinMax: ");
    buffer.append(Double.toString(getAverageTimeExcludingMinMax()));
    buffer.append(" averagePerSecond: ");
    buffer.append(Double.toString(getAveragePerSecond()));
    buffer.append(" averagePerSecondExMinMax: ");
    buffer.append(Double.toString(getAveragePerSecondExcludingMinMax()));
    super.appendFieldDescription(buffer);
  }
}