package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.Stats;

public abstract interface StatsCapable
{
  public abstract Stats getStats();
}