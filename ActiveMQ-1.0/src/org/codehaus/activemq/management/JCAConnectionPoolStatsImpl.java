package org.codehaus.activemq.management;

import javax.management.j2ee.statistics.BoundedRangeStatistic;
import javax.management.j2ee.statistics.CountStatistic;
import javax.management.j2ee.statistics.JCAConnectionPoolStats;
import javax.management.j2ee.statistics.RangeStatistic;
import javax.management.j2ee.statistics.TimeStatistic;

public class JCAConnectionPoolStatsImpl extends JCAConnectionStatsImpl
  implements JCAConnectionPoolStats
{
  private CountStatistic closeCount;
  private CountStatistic createCount;
  private BoundedRangeStatistic freePoolSize;
  private BoundedRangeStatistic poolSize;
  private RangeStatistic waitingThreadCount;

  public JCAConnectionPoolStatsImpl(String connectionFactory, String managedConnectionFactory, TimeStatistic waitTime, TimeStatistic useTime, CountStatistic closeCount, CountStatistic createCount, BoundedRangeStatistic freePoolSize, BoundedRangeStatistic poolSize, RangeStatistic waitingThreadCount)
  {
    super(connectionFactory, managedConnectionFactory, waitTime, useTime);
    this.closeCount = closeCount;
    this.createCount = createCount;
    this.freePoolSize = freePoolSize;
    this.poolSize = poolSize;
    this.waitingThreadCount = waitingThreadCount;

    addStatistic("freePoolSize", freePoolSize);
    addStatistic("poolSize", poolSize);
    addStatistic("waitingThreadCount", waitingThreadCount);
  }

  public CountStatistic getCloseCount() {
    return this.closeCount;
  }

  public CountStatistic getCreateCount() {
    return this.createCount;
  }

  public BoundedRangeStatistic getFreePoolSize() {
    return this.freePoolSize;
  }

  public BoundedRangeStatistic getPoolSize() {
    return this.poolSize;
  }

  public RangeStatistic getWaitingThreadCount() {
    return this.waitingThreadCount;
  }
}