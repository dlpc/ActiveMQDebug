package org.codehaus.activemq.management;

import EDU.oswego.cs.dl.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import javax.management.j2ee.statistics.JMSConnectionStats;
import javax.management.j2ee.statistics.JMSStats;
import org.codehaus.activemq.ActiveMQConnection;
import org.codehaus.activemq.util.IndentPrinter;

public class JMSStatsImpl extends StatsImpl
  implements JMSStats
{
  private List connections = new CopyOnWriteArrayList();

  public JMSConnectionStats[] getConnections()
  {
    Object[] connectionArray = this.connections.toArray();
    int size = connectionArray.length;
    JMSConnectionStats[] answer = new JMSConnectionStats[size];
    for (int i = 0; i < size; i++) {
      ActiveMQConnection connection = (ActiveMQConnection)connectionArray[i];
      answer[i] = connection.getConnectionStats();
    }
    return answer;
  }

  public void addConnection(ActiveMQConnection connection) {
    this.connections.add(connection);
  }

  public void removeConnection(ActiveMQConnection connection) {
    this.connections.remove(connection);
  }

  public void dump(IndentPrinter out) {
    out.printIndent();
    out.println("factory {");
    out.incrementIndent();
    JMSConnectionStats[] array = getConnections();
    for (int i = 0; i < array.length; i++) {
      JMSConnectionStatsImpl connectionStat = (JMSConnectionStatsImpl)array[i];
      connectionStat.dump(out);
    }
    out.decrementIndent();
    out.printIndent();
    out.println("}");
    out.flush();
  }
}