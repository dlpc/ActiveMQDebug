package org.codehaus.activemq.spring;

import org.codehaus.activemq.broker.BrokerContainer;
import org.codehaus.activemq.broker.BrokerContainerFactory;
import org.springframework.core.io.Resource;

public class SpringBrokerContainerFactory
  implements BrokerContainerFactory
{
  private Resource resource;

  public static BrokerContainer newInstance(Resource resource, String brokerName)
  {
    SpringBrokerContainerFactory factory = new SpringBrokerContainerFactory(resource);
    return factory.createBrokerContainer(brokerName);
  }

  public SpringBrokerContainerFactory() {
  }

  public SpringBrokerContainerFactory(Resource resource) {
    this.resource = resource;
  }

  public BrokerContainer createBrokerContainer(String brokerName) {
    ActiveMQBeanFactory beanFactory = new ActiveMQBeanFactory(brokerName, this.resource);
    return (BrokerContainer)beanFactory.getBean("broker");
  }

  public Resource getResource()
  {
    return this.resource;
  }

  public void setResource(Resource resource) {
    this.resource = resource;
  }
}