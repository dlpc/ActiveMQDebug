package org.codehaus.activemq.spring;

import org.springframework.beans.BeansException;
import org.springframework.core.io.Resource;

public class ConfigurationParseException extends BeansException
{
  private Resource resource;

  public ConfigurationParseException(Resource resource, Throwable e)
  {
    super("Could not parse resource: " + resource + ". Reason: " + e, e);
    this.resource = resource;
  }

  public Resource getResource()
  {
    return this.resource;
  }
}