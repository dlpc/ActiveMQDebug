package org.codehaus.activemq.spring;

import java.io.PrintStream;
import javax.jms.JMSException;
import org.codehaus.activemq.broker.BrokerContainer;
import org.springframework.core.io.FileSystemResource;

public class Main
{
  public static void main(String[] args)
  {
    try
    {
      SpringBrokerContainerFactory factory = new SpringBrokerContainerFactory();
      if (args.length <= 0) {
        System.out.println("Usage: Main <xmlConfigFile>");
        System.out.println("You must specify a deployment descriptor to run the ActiveMQ Message Broker");
        return;
      }
      String file = args[0];

      System.out.println("Loading Message Broker from file: " + file);
      factory.setResource(new FileSystemResource(file));

      BrokerContainer container = factory.createBrokerContainer("DefaultBroker");
      container.start();

      Object lock = new Object();
      synchronized (lock) {
        lock.wait();
      }
    }
    catch (JMSException e) {
      System.out.println("Caught: " + e);
      e.printStackTrace();
      Exception le = e.getLinkedException();
      System.out.println("Reason: " + le);
      if (le != null)
        le.printStackTrace();
    }
    catch (Exception e)
    {
      System.out.println("Caught: " + e);
      e.printStackTrace();
    }
  }
}