package org.codehaus.activemq.spring;

import java.io.InputStream;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;

public class ActiveMQBeanFactory extends DefaultListableBeanFactory
{
  private XmlBeanDefinitionReader reader;

  public static ActiveMQBeanFactory newInstance(String brokerName, Resource resource)
  {
    return new ActiveMQBeanFactory(brokerName, resource);
  }

  public ActiveMQBeanFactory(String brokerName, Resource resource)
    throws BeansException
  {
    this(brokerName, resource, null);
  }

  public ActiveMQBeanFactory(String brokerName, InputStream is)
    throws BeansException
  {
    this(brokerName, new InputStreamResource(is, "(no description)"), null);
  }

  public ActiveMQBeanFactory(String brokerName, Resource resource, BeanFactory parentBeanFactory)
    throws BeansException
  {
    super(parentBeanFactory);
    this.reader = createReader(brokerName);
    this.reader.loadBeanDefinitions(resource);
  }

  protected XmlBeanDefinitionReader getReader() {
    return this.reader;
  }

  protected XmlBeanDefinitionReader createReader(String brokerName)
  {
    return new ActiveMQBeanDefinitionReader(this, brokerName);
  }
}