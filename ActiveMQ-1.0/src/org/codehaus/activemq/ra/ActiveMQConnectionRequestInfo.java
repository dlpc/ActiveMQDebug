package org.codehaus.activemq.ra;

import javax.resource.spi.ConnectionRequestInfo;

public class ActiveMQConnectionRequestInfo
  implements ConnectionRequestInfo, Cloneable
{
  String userName;
  String password;
  String serverUrl;
  String clientid;
  boolean xa;

  public ActiveMQConnectionRequestInfo copy()
  {
    try
    {
      return (ActiveMQConnectionRequestInfo)clone();
    } catch (CloneNotSupportedException e) {
    	throw new RuntimeException("Could not clone: ", e);
    }
    
  }

  public int hashCode()
  {
    int rc = 0;
    if (this.userName != null) {
      rc ^= this.userName.hashCode();
    }
    if (this.password != null) {
      rc ^= this.password.hashCode();
    }
    if (this.serverUrl != null) {
      rc ^= this.serverUrl.hashCode();
    }
    if (this.clientid != null) {
      rc ^= this.clientid.hashCode();
    }
    if (this.xa) {
      rc ^= 11259375;
    }
    return rc;
  }

  public boolean equals(Object o)
  {
    if (o == null) {
      return false;
    }
    if (!getClass().equals(o.getClass())) {
      return false;
    }
    ActiveMQConnectionRequestInfo i = (ActiveMQConnectionRequestInfo)o;
    if (((this.userName == null ? 1 : 0) ^ (i.userName == null ? 1 : 0)) != 0) {
      return false;
    }
    if ((this.userName != null) && (!this.userName.equals(i.userName))) {
      return false;
    }
    if (((this.password == null ? 1 : 0) ^ (i.password == null ? 1 : 0)) != 0) {
      return false;
    }
    if ((this.password != null) && (!this.password.equals(i.password))) {
      return false;
    }
    if (((this.serverUrl == null ? 1 : 0) ^ (i.serverUrl == null ? 1 : 0)) != 0) {
      return false;
    }
    if ((this.serverUrl != null) && (!this.serverUrl.equals(i.serverUrl))) {
      return false;
    }
    if (((this.clientid == null ? 1 : 0) ^ (i.clientid == null ? 1 : 0)) != 0) {
      return false;
    }
    if ((this.clientid != null) && (!this.clientid.equals(i.clientid))) {
      return false;
    }

    return this.xa == i.xa;
  }

  public String getServerUrl()
  {
    return this.serverUrl;
  }

  public void setServerUrl(String url)
  {
    this.serverUrl = url;
  }

  public String getPassword()
  {
    return this.password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getUserName()
  {
    return this.userName;
  }

  public void setUserName(String userid)
  {
    this.userName = userid;
  }

  public String getClientid()
  {
    return this.clientid;
  }

  public void setClientid(String clientid)
  {
    this.clientid = clientid;
  }

  public boolean isXa()
  {
    return this.xa;
  }

  public void setXa(boolean xa)
  {
    this.xa = xa;
  }
}