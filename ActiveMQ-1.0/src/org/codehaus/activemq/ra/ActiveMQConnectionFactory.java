package org.codehaus.activemq.ra;

import java.io.Serializable;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.naming.Reference;
import javax.resource.Referenceable;
import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ActiveMQConnectionFactory
  implements ConnectionFactory, Referenceable, Serializable
{
  private static final Log log = LogFactory.getLog(ActiveMQConnectionFactory.class);
  private transient ConnectionManager manager;
  private transient ActiveMQManagedConnectionFactory factory;
  private Reference reference;
  private final ActiveMQConnectionRequestInfo info;

  public ActiveMQConnectionFactory(ActiveMQManagedConnectionFactory factory, ConnectionManager manager, ActiveMQConnectionRequestInfo info)
  {
    this.factory = factory;
    this.manager = manager;
    this.info = info;
  }

  public Connection createConnection()
    throws JMSException
  {
    return createConnection(this.info.copy());
  }

  public Connection createConnection(String userName, String password)
    throws JMSException
  {
    ActiveMQConnectionRequestInfo i = this.info.copy();
    i.setUserName(userName);
    i.setPassword(password);
    return createConnection(i);
  }

  private Connection createConnection(ActiveMQConnectionRequestInfo info)
    throws JMSException
  {
    try
    {
      return (Connection)this.manager.allocateConnection(this.factory, info);
    }
    catch (ResourceException e)
    {
      if ((e.getCause() instanceof JMSException)) {
        throw ((JMSException)e.getCause());
      }
      log.debug("Connection could not be created:", e);
      throw new JMSException(e.getMessage());
    }
  }

  public Reference getReference()
  {
    return this.reference;
  }

  public void setReference(Reference reference)
  {
    this.reference = reference;
  }

@Override
public JMSContext createContext() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public JMSContext createContext(int arg0) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public JMSContext createContext(String arg0, String arg1) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public JMSContext createContext(String arg0, String arg1, int arg2) {
	// TODO Auto-generated method stub
	return null;
}
}