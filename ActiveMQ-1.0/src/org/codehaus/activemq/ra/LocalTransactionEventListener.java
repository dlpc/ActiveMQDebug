package org.codehaus.activemq.ra;

public abstract interface LocalTransactionEventListener
{
  public abstract void beginEvent();

  public abstract void commitEvent();

  public abstract void rollbackEvent();
}