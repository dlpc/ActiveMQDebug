package org.codehaus.activemq.ra;

import javax.resource.spi.endpoint.MessageEndpointFactory;

public class ActiveMQEndpointActivationKey
{
  private final MessageEndpointFactory messageEndpointFactory;
  private final ActiveMQActivationSpec activationSpec;

  public ActiveMQActivationSpec getActivationSpec()
  {
    return this.activationSpec;
  }

  public MessageEndpointFactory getMessageEndpointFactory()
  {
    return this.messageEndpointFactory;
  }

  public ActiveMQEndpointActivationKey(MessageEndpointFactory messageEndpointFactory, ActiveMQActivationSpec activationSpec)
  {
    this.messageEndpointFactory = messageEndpointFactory;
    this.activationSpec = activationSpec;
  }

  public int hashCode()
  {
    return this.messageEndpointFactory.hashCode() ^ this.activationSpec.hashCode();
  }

  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    ActiveMQEndpointActivationKey o = (ActiveMQEndpointActivationKey)obj;

    return (o.activationSpec == this.activationSpec) && (o.messageEndpointFactory == this.messageEndpointFactory);
  }
}