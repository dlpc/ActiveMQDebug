package org.codehaus.activemq.ra;

import java.io.Serializable;
import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

public class JMSSessionProxy
  implements Session, QueueSession, TopicSession
{
  private final ActiveMQManagedConnection managedConnection;
  boolean closed = false;

  public JMSSessionProxy(ActiveMQManagedConnection managedConnection) {
    this.managedConnection = managedConnection;
  }

  public void close()
    throws JMSException
  {
    cleanup();
  }

  public void cleanup()
  {
    this.closed = true;
  }

  private Session getSession()
    throws JMSException
  {
    if (this.closed) {
      throw new JMSException("Session closed.");
    }
    return this.managedConnection.getPhysicalSession();
  }

  public void commit()
    throws JMSException
  {
    getSession().commit();
  }

  public QueueBrowser createBrowser(Queue queue)
    throws JMSException
  {
    return getSession().createBrowser(queue);
  }

  public QueueBrowser createBrowser(Queue queue, String messageSelector)
    throws JMSException
  {
    return getSession().createBrowser(queue, messageSelector);
  }

  public BytesMessage createBytesMessage()
    throws JMSException
  {
    return getSession().createBytesMessage();
  }

  public MessageConsumer createConsumer(Destination destination)
    throws JMSException
  {
    return getSession().createConsumer(destination);
  }

  public MessageConsumer createConsumer(Destination destination, String messageSelector)
    throws JMSException
  {
    return getSession().createConsumer(destination, messageSelector);
  }

  public MessageConsumer createConsumer(Destination destination, String messageSelector, boolean NoLocal)
    throws JMSException
  {
    return getSession().createConsumer(destination, messageSelector, NoLocal);
  }

  public TopicSubscriber createDurableSubscriber(Topic topic, String name)
    throws JMSException
  {
    return getSession().createDurableSubscriber(topic, name);
  }

  public TopicSubscriber createDurableSubscriber(Topic topic, String name, String messageSelector, boolean noLocal)
    throws JMSException
  {
    return getSession().createDurableSubscriber(topic, name, messageSelector, noLocal);
  }

  public MapMessage createMapMessage()
    throws JMSException
  {
    return getSession().createMapMessage();
  }

  public Message createMessage()
    throws JMSException
  {
    return getSession().createMessage();
  }

  public ObjectMessage createObjectMessage()
    throws JMSException
  {
    return getSession().createObjectMessage();
  }

  public ObjectMessage createObjectMessage(Serializable object)
    throws JMSException
  {
    return getSession().createObjectMessage(object);
  }

  public MessageProducer createProducer(Destination destination)
    throws JMSException
  {
    return getSession().createProducer(destination);
  }

  public Queue createQueue(String queueName)
    throws JMSException
  {
    return getSession().createQueue(queueName);
  }

  public StreamMessage createStreamMessage()
    throws JMSException
  {
    return getSession().createStreamMessage();
  }

  public TemporaryQueue createTemporaryQueue()
    throws JMSException
  {
    return getSession().createTemporaryQueue();
  }

  public TemporaryTopic createTemporaryTopic()
    throws JMSException
  {
    return getSession().createTemporaryTopic();
  }

  public TextMessage createTextMessage()
    throws JMSException
  {
    return getSession().createTextMessage();
  }

  public TextMessage createTextMessage(String text)
    throws JMSException
  {
    return getSession().createTextMessage(text);
  }

  public Topic createTopic(String topicName)
    throws JMSException
  {
    return getSession().createTopic(topicName);
  }

  public int getAcknowledgeMode()
    throws JMSException
  {
    return getSession().getAcknowledgeMode();
  }

  public MessageListener getMessageListener()
    throws JMSException
  {
    return getSession().getMessageListener();
  }

  public boolean getTransacted()
    throws JMSException
  {
    return getSession().getTransacted();
  }

  public void recover()
    throws JMSException
  {
    getSession().recover();
  }

  public void rollback()
    throws JMSException
  {
    getSession().rollback();
  }

  public void setMessageListener(MessageListener listener)
    throws JMSException
  {
    getSession().setMessageListener(listener);
  }

  public void unsubscribe(String name)
    throws JMSException
  {
    getSession().unsubscribe(name);
  }

  public QueueReceiver createReceiver(Queue queue)
    throws JMSException
  {
    return ((QueueSession)getSession()).createReceiver(queue);
  }

  public QueueReceiver createReceiver(Queue queue, String messageSelector)
    throws JMSException
  {
    return ((QueueSession)getSession()).createReceiver(queue, messageSelector);
  }

  public QueueSender createSender(Queue queue)
    throws JMSException
  {
    return ((QueueSession)getSession()).createSender(queue);
  }

  public TopicPublisher createPublisher(Topic topic)
    throws JMSException
  {
    return ((TopicSession)getSession()).createPublisher(topic);
  }

  public TopicSubscriber createSubscriber(Topic topic)
    throws JMSException
  {
    return ((TopicSession)getSession()).createSubscriber(topic);
  }

  public TopicSubscriber createSubscriber(Topic topic, String messageSelector, boolean noLocal)
    throws JMSException
  {
    return ((TopicSession)getSession()).createSubscriber(topic, messageSelector, noLocal);
  }

  public void run()
  {
    throw new RuntimeException("Operation not supported.");
  }

@Override
public MessageConsumer createDurableConsumer(Topic arg0, String arg1) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public MessageConsumer createDurableConsumer(Topic arg0, String arg1, String arg2, boolean arg3) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public MessageConsumer createSharedConsumer(Topic arg0, String arg1) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public MessageConsumer createSharedConsumer(Topic arg0, String arg1, String arg2) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public MessageConsumer createSharedDurableConsumer(Topic arg0, String arg1) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public MessageConsumer createSharedDurableConsumer(Topic arg0, String arg1, String arg2) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}
}