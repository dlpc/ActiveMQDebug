package org.codehaus.activemq.ra;

import java.util.ArrayList;
import java.util.Iterator;
import javax.jms.Connection;
import javax.jms.ConnectionConsumer;
import javax.jms.ConnectionMetaData;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.ServerSessionPool;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;

public class JMSConnectionProxy
  implements Connection, QueueConnection, TopicConnection
{
  private ActiveMQManagedConnection managedConnection;
  private ArrayList sessions = new ArrayList();

  public JMSConnectionProxy(ActiveMQManagedConnection managedConnection) {
    this.managedConnection = managedConnection;
  }

  public void close()
    throws JMSException
  {
    this.managedConnection.proxyClosedEvent(this);
  }

  public void cleanup()
  {
    this.managedConnection = null;
    for (Iterator iter = this.sessions.iterator(); iter.hasNext(); ) {
      JMSSessionProxy p = (JMSSessionProxy)iter.next();
      p.cleanup();
      iter.remove();
    }
  }

  private Connection getConnection()
    throws JMSException
  {
    if (this.managedConnection == null) {
      throw new JMSException("Connection is closed.");
    }
    return this.managedConnection.getPhysicalConnection();
  }

  public Session createSession(boolean transacted, int acknowledgeMode)
    throws JMSException
  {
    return createSessionProxy();
  }

  private JMSSessionProxy createSessionProxy()
  {
    JMSSessionProxy p = new JMSSessionProxy(this.managedConnection);
    this.sessions.add(p);
    return p;
  }

  public QueueSession createQueueSession(boolean transacted, int acknowledgeMode)
    throws JMSException
  {
    return createSessionProxy();
  }

  public TopicSession createTopicSession(boolean transacted, int acknowledgeMode)
    throws JMSException
  {
    return createSessionProxy();
  }

  public String getClientID()
    throws JMSException
  {
    return getConnection().getClientID();
  }

  public ExceptionListener getExceptionListener()
    throws JMSException
  {
    return getConnection().getExceptionListener();
  }

  public ConnectionMetaData getMetaData()
    throws JMSException
  {
    return getConnection().getMetaData();
  }

  public void setClientID(String clientID)
    throws JMSException
  {
  }

  public void setExceptionListener(ExceptionListener listener)
    throws JMSException
  {
    getConnection().setExceptionListener(listener);
  }

  public void start()
    throws JMSException
  {
  }

  public void stop()
    throws JMSException
  {
  }

  public ConnectionConsumer createConnectionConsumer(Queue queue, String messageSelector, ServerSessionPool sessionPool, int maxMessages)
    throws JMSException
  {
    throw new JMSException("Not Supported.");
  }

  public ConnectionConsumer createConnectionConsumer(Topic topic, String messageSelector, ServerSessionPool sessionPool, int maxMessages)
    throws JMSException
  {
    throw new JMSException("Not Supported.");
  }

  public ConnectionConsumer createConnectionConsumer(Destination destination, String messageSelector, ServerSessionPool sessionPool, int maxMessages)
    throws JMSException
  {
    throw new JMSException("Not Supported.");
  }

  public ConnectionConsumer createDurableConnectionConsumer(Topic topic, String subscriptionName, String messageSelector, ServerSessionPool sessionPool, int maxMessages)
    throws JMSException
  {
    throw new JMSException("Not Supported.");
  }

  public ActiveMQManagedConnection getManagedConnection()
  {
    return this.managedConnection;
  }

@Override
public Session createSession() throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Session createSession(int arg0) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ConnectionConsumer createSharedConnectionConsumer(Topic arg0, String arg1, String arg2, ServerSessionPool arg3,
		int arg4) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}

@Override
public ConnectionConsumer createSharedDurableConnectionConsumer(Topic arg0, String arg1, String arg2,
		ServerSessionPool arg3, int arg4) throws JMSException {
	// TODO Auto-generated method stub
	return null;
}
}