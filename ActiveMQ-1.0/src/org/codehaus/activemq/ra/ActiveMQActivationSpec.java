package org.codehaus.activemq.ra;

import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.InvalidPropertyException;
import javax.resource.spi.ResourceAdapter;

public class ActiveMQActivationSpec
  implements ActivationSpec
{
  private ActiveMQResourceAdapter resourceAdapter;
  private String destinationType;
  private String destinationName;
  private String durableSubscriptionName;
  private String messageSelector;
  boolean noLocal = false;

  public void validate()
    throws InvalidPropertyException
  {
  }

  public ResourceAdapter getResourceAdapter()
  {
    return this.resourceAdapter;
  }

  public void setResourceAdapter(ResourceAdapter resourceAdapter)
    throws ResourceException
  {
    if (this.resourceAdapter != null) {
      throw new ResourceException("ResourceAdapter already set");
    }
    if (!(resourceAdapter instanceof ActiveMQResourceAdapter)) {
      throw new ResourceException("ResourceAdapter is not of type: " + ActiveMQResourceAdapter.class.getName());
    }
    this.resourceAdapter = ((ActiveMQResourceAdapter)resourceAdapter);
  }

  public String getDestinationName()
  {
    return this.destinationName;
  }

  public void setDestinationName(String destinationName)
  {
    this.destinationName = destinationName;
  }

  public String getDestinationType()
  {
    return this.destinationType;
  }

  public void setDestinationType(String destinationType)
  {
    this.destinationType = destinationType;
  }

  public String getDurableSubscriptionName()
  {
    return this.durableSubscriptionName;
  }

  public void setDurableSubscriptionName(String durableSubscriptionName)
  {
    this.durableSubscriptionName = durableSubscriptionName;
  }

  public String getMessageSelector()
  {
    return this.messageSelector;
  }

  public void setMessageSelector(String messageSelector)
  {
    this.messageSelector = messageSelector;
  }

  public boolean getNoLocal()
  {
    return this.noLocal;
  }

  public void setNoLocal(boolean noLocal)
  {
    this.noLocal = noLocal;
  }
}