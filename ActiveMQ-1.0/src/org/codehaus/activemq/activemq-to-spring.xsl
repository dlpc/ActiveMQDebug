<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet
  xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
  version='1.0'>

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"
    doctype-public="-//SPRING//DTD BEAN//EN"
    doctype-system="http://www.springframework.org/dtd/spring-beans.dtd"/>


  <xsl:param name="brokerName"/>

  <xsl:template match="*">
    <xsl:copy>
      <xsl:copy-of select="attribute::*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="broker">
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="@class">
          <xsl:value-of select="@class"/>
        </xsl:when>
        <xsl:otherwise>org.codehaus.activemq.broker.impl.BrokerContainerImpl</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <bean id="broker" class="{$type}" destroy-method="stop">
      <constructor-arg>
        <description>Unique Name of Broker</description>
        <value>
          <xsl:choose>
            <xsl:when test="@name">
              <xsl:value-of select="@name"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$brokerName"/>
            </xsl:otherwise>
          </xsl:choose>
        </value>
      </constructor-arg>
      <property name="connectors">
        <list>
          <xsl:apply-templates select="connector"/>
        </list>
      </property>
      <property name="persistenceAdapter">
        <xsl:apply-templates select="*[local-name() != 'connector']"/>
      </property>
    </bean>
  </xsl:template>

  <xsl:template match="connector">
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="@class">
          <xsl:value-of select="@class"/>
        </xsl:when>
        <xsl:otherwise>org.codehaus.activemq.broker.impl.BrokerConnectorImpl</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <bean name="connector" class="{$type}" autowire="constructor"
      init-method="start" destroy-method="stop">
      <constructor-arg index="0">
        <description>Broker</description>
        <ref bean="broker"/>
      </constructor-arg>
      <constructor-arg index="1">
        <description>Transport Server Channel</description>
        <xsl:apply-templates/>
      </constructor-arg>
    </bean>
  </xsl:template>

  <xsl:template match="serverTransport|tcpServerTransport">
    <xsl:choose>
      <xsl:when test="@class">
        <!-- lets use the Spring way to initialise the transport -->
        <bean name="serverTransport" class="{@class}" autowire="constructor">
          <constructor-arg index="0">
            <xsl:call-template name="makeWireFormat"/>
          </constructor-arg>
          <constructor-arg index="1">
            <bean class="java.net.URI">
              <constructor-arg>
                <value>
                  <xsl:value-of select="@uri"/>
                </value>
              </constructor-arg>
            </bean>
          </constructor-arg>
          <xsl:apply-templates select="@*|*" mode="addProperties"/>
        </bean>
      </xsl:when>
      <xsl:otherwise>
        <!-- lets use the factory method -->
        <bean name="serverTransport" class="org.codehaus.activemq.transport.TransportServerChannelProvider"
          factory-method="newInstance">
          <constructor-arg index="0">
            <xsl:call-template name="makeWireFormat"/>
          </constructor-arg>
          <constructor-arg index="1">
            <value>
              <xsl:value-of select="@uri"/>
            </value>
          </constructor-arg>
          <xsl:apply-templates select="@*|*" mode="addProperties"/>
        </bean>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="persistence">
    <bean name="persistenceAdapter" class="{@class}" autowire="constructor">
      <xsl:apply-templates select="@*|*" mode="addProperties"/>
    </bean>
  </xsl:template>

  <xsl:template match="berkeleyDbPersistence">
    <xsl:call-template name="makeBean">
      <xsl:with-param name="defaultType">org.codehaus.activemq.store.bdb.BDbPersistenceAdapter</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="jdbmPersistence">
    <xsl:call-template name="makeBean">
      <xsl:with-param name="defaultType">org.codehaus.activemq.store.jdbm.JdbmPersistenceAdapter</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="jdbcPersistence">
    <xsl:call-template name="makeBean">
      <xsl:with-param name="defaultType">org.codehaus.activemq.store.jdbc.JDBCPersistenceAdapter</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="vmPersistence">
    <xsl:call-template name="makeBean">
      <xsl:with-param name="defaultType">org.codehaus.activemq.store.vm.VMPersistenceAdapter</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="howlPersistence">
    <xsl:param name="defaultType"/>
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="@class">
          <xsl:value-of select="@class"/>
        </xsl:when>
        <xsl:otherwise>org.codehaus.activemq.store.howl.HowlPersistenceAdapter</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <bean name="{local-name()}" class="{$type}" autowire="constructor">
      <xsl:apply-templates select="@*" mode="addProperties"/>
      <property name="longTermPersistence">
        <xsl:apply-templates select="*"/>
      </property>
    </bean>
  </xsl:template>


  <xsl:template name="makeBean">
    <xsl:param name="defaultType"/>
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="@class">
          <xsl:value-of select="@class"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$defaultType"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <bean name="{local-name()}" class="{$type}" autowire="constructor">
      <xsl:apply-templates select="@*|*" mode="addProperties"/>
    </bean>
  </xsl:template>


  <xsl:template name="makeWireFormat">
    <xsl:choose>
      <xsl:when test="wireFormat">
        <apply-template select="wireFormat"/>
      </xsl:when>
      <xsl:otherwise>
        <bean class="org.codehaus.activemq.message.DefaultWireFormat"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="wireFormat">
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="@class">
          <xsl:value-of select="@class"/>
        </xsl:when>
        <xsl:otherwise>org.codehaus.activemq.broker.message.DefaultWireFormat</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <bean name="wireFormat" class="{$type}" init-method="start" destroy-method="stop">
      <xsl:apply-templates select="@*|*" mode="addProperties"/>
    </bean>
  </xsl:template>

  <xsl:template match="*|@*" mode="addProperties">
    <property name="{local-name()}">
      <value>
        <xsl:value-of select="."/>
      </value>
    </property>
  </xsl:template>

  <xsl:template match="@class|@uri|wireFormat" mode="addProperties"/>

</xsl:stylesheet>
