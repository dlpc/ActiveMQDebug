package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class MessageAckReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 15;
  }

  public Packet createPacket()
  {
    return new MessageAck();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    MessageAck ack = (MessageAck)packet;
    ack.setConsumerId(dataIn.readUTF());
    ack.setMessageID(dataIn.readUTF());
    ack.setTransactionId(dataIn.readUTF());
    ack.setMessageRead(dataIn.readBoolean());
    ack.setXaTransacted(dataIn.readBoolean());
  }
}