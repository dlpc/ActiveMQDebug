package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class XATransactionInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 20;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    XATransactionInfo info = (XATransactionInfo)packet;
    dataOut.writeByte(info.getType());
    switch (info.getType()) {
    case 101:
    case 102:
    case 103:
    case 104:
    case 105:
    case 106:
    case 107:
    case 108:
    case 109:
      assert (info.getXid() != null);
      info.getXid().write(dataOut);
      break;
    case 112:
      dataOut.writeInt(info.getTransactionTimeout());
      break;
    case 110:
      break;
    case 111:
    case 113:
      break;
    default:
      throw new IllegalArgumentException("Invalid type code: " + info.getType());
    }
  }
}