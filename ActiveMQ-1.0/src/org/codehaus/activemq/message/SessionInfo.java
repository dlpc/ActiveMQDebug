package org.codehaus.activemq.message;

public class SessionInfo extends AbstractPacket
{
  private String clientId;
  private String sessionId;
  private long startTime;
  private boolean started;

  public int getPacketType()
  {
    return 23;
  }

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof SessionInfo))) {
      SessionInfo info = (SessionInfo)obj;
      result = this.sessionId == info.sessionId;
    }
    return result;
  }

  public int hashCode()
  {
    return this.sessionId != null ? this.sessionId.hashCode() : super.hashCode();
  }

  public String getSessionId()
  {
    return this.sessionId;
  }

  public void setSessionId(String sessionId)
  {
    this.sessionId = sessionId;
  }

  public String getClientId()
  {
    return this.clientId;
  }

  public void setClientId(String newClientId)
  {
    this.clientId = newClientId;
  }

  public boolean isStarted()
  {
    return this.started;
  }

  public void setStarted(boolean flag)
  {
    this.started = flag;
  }

  public long getStartTime()
  {
    return this.startTime;
  }

  public void setStartTime(long newStartTime)
  {
    this.startTime = newStartTime;
  }
}