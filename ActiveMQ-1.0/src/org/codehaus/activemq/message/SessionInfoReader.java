package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class SessionInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 23;
  }

  public Packet createPacket()
  {
    return new SessionInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    SessionInfo info = (SessionInfo)packet;
    info.setClientId(dataIn.readUTF());
    info.setSessionId(dataIn.readUTF());
    info.setStartTime(dataIn.readLong());
    info.setStarted(dataIn.readBoolean());
    info.setReceiptRequired(dataIn.readBoolean());
  }
}