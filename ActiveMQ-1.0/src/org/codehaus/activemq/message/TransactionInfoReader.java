package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class TransactionInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 19;
  }

  public Packet createPacket()
  {
    return new TransactionInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    TransactionInfo info = (TransactionInfo)packet;
    info.setTransactionId(dataIn.readUTF());
    info.setType(dataIn.readByte());
    info.setReceiptRequired(dataIn.readBoolean());
  }
}