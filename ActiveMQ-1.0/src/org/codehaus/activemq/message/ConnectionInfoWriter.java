package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class ConnectionInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 22;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    ConnectionInfo info = (ConnectionInfo)packet;
    super.writeUTF(info.getClientId(), dataOut);
    super.writeUTF(info.getUserName(), dataOut);
    super.writeUTF(info.getPassword(), dataOut);
    super.writeUTF(info.getHostName(), dataOut);
    dataOut.writeLong(info.getStartTime());
    dataOut.writeBoolean(info.isStarted());
    dataOut.writeBoolean(info.isClosed());
    super.writeObject(info.getProperties(), dataOut);
  }
}