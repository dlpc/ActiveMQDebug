package org.codehaus.activemq.message;

public abstract class AbstractPacket
  implements Packet
{
  private String id;
  private boolean receiptRequired;
  private transient int memoryUsage = 2048;
  private transient int memoryUsageReferenceCount;

  public String getId()
  {
    return this.id;
  }

  public void setId(String newId)
  {
    this.id = newId;
  }

  public boolean isReceiptRequired()
  {
    return this.receiptRequired;
  }

  public boolean isReceipt()
  {
    return false;
  }

  public void setReceiptRequired(boolean value)
  {
    this.receiptRequired = value;
  }

  public boolean isJMSMessage()
  {
    return false;
  }

  public boolean equals(Object obj)
  {
    boolean result = this == obj;
    if ((!result) && (obj != null) && ((obj instanceof AbstractPacket))) {
      AbstractPacket other = (AbstractPacket)obj;
      result = other.getId().equals(getId());
    }

    return result;
  }

  public int hashCode()
  {
    return this.id != null ? this.id.hashCode() : super.hashCode();
  }

  public int getMemoryUsage()
  {
    return this.memoryUsage;
  }

  public void setMemoryUsage(int newMemoryUsage)
  {
    this.memoryUsage = newMemoryUsage;
  }

  public synchronized int incrementMemoryReferenceCount()
  {
    return ++this.memoryUsageReferenceCount;
  }

  public synchronized int decrementMemoryReferenceCount()
  {
    return --this.memoryUsageReferenceCount;
  }

  public synchronized int getMemoryUsageReferenceCount()
  {
    return this.memoryUsageReferenceCount;
  }

  public String toString()
  {
    return getPacketTypeAsString(getPacketType()) + ": " + getId();
  }

  protected static String getPacketTypeAsString(int type) {
    String packetTypeStr = "";
    switch (type) {
    case 6:
      packetTypeStr = "ACTIVEMQ_MESSAGE";
      break;
    case 7:
      packetTypeStr = "ACTIVEMQ_TEXT_MESSAGE";
      break;
    case 8:
      packetTypeStr = "ACTIVEMQ_OBJECT_MESSAGE";
      break;
    case 9:
      packetTypeStr = "ACTIVEMQ_BYTES_MESSAGE";
      break;
    case 10:
      packetTypeStr = "ACTIVEMQ_STREAM_MESSAGE";
      break;
    case 11:
      packetTypeStr = "ACTIVEMQ_MAP_MESSAGE";
      break;
    case 15:
      packetTypeStr = "ACTIVEMQ_MSG_ACK";
      break;
    case 16:
      packetTypeStr = "RECEIPT_INFO";
      break;
    case 17:
      packetTypeStr = "CONSUMER_INFO";
      break;
    case 18:
      packetTypeStr = "PRODUCER_INFO";
      break;
    case 19:
      packetTypeStr = "TRANSACTION_INFO";
      break;
    case 20:
      packetTypeStr = "XA_TRANSACTION_INFO";
      break;
    case 21:
      packetTypeStr = "ACTIVEMQ_BROKER_INFO";
      break;
    case 22:
      packetTypeStr = "ACTIVEMQ_CONNECTION_INFO";
      break;
    case 23:
      packetTypeStr = "SESSION_INFO";
      break;
    case 12:
    case 13:
    case 14:
    default:
      packetTypeStr = "UNKNOWN PACKET TYPE: " + type;
    }
    return packetTypeStr;
  }

  protected boolean equals(Object left, Object right)
  {
    return (left == right) || ((left != null) && (left.equals(right)));
  }
}