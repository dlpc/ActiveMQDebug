package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;
import java.util.Properties;

public class ConnectionInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 22;
  }

  public Packet createPacket()
  {
    return new ConnectionInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    ConnectionInfo info = (ConnectionInfo)packet;
    info.setClientId(dataIn.readUTF());
    info.setUserName(dataIn.readUTF());
    info.setPassword(dataIn.readUTF());
    info.setHostName(dataIn.readUTF());
    info.setStartTime(dataIn.readLong());
    info.setStarted(dataIn.readBoolean());
    info.setClosed(dataIn.readBoolean());
    info.setProperties((Properties)super.readObject(dataIn));
  }
}