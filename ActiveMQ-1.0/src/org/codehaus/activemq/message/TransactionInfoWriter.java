package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class TransactionInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 19;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    TransactionInfo info = (TransactionInfo)packet;
    super.writeUTF(info.getTransactionId(), dataOut);
    dataOut.writeByte(info.getType());
    dataOut.writeBoolean(info.isReceiptRequired());
  }
}