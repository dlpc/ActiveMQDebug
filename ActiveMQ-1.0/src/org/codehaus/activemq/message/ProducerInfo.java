package org.codehaus.activemq.message;

public class ProducerInfo extends AbstractPacket
{
  private ActiveMQDestination destination;
  private String producerId;
  private String clientId;
  private String sessionId;
  private long startTime;
  private boolean started;

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof ProducerInfo))) {
      ProducerInfo info = (ProducerInfo)obj;
      result = this.producerId == info.producerId;
    }
    return result;
  }

  public int hashCode()
  {
    return this.producerId != null ? this.producerId.hashCode() : super.hashCode();
  }

  public String getProducerId()
  {
    return this.producerId;
  }

  public void setProducerId(String producerId)
  {
    this.producerId = producerId;
  }

  public String getSessionId()
  {
    return this.sessionId;
  }

  public void setSessionId(String sessionId)
  {
    this.sessionId = sessionId;
  }

  public int getPacketType()
  {
    return 18;
  }

  public String getClientId()
  {
    return this.clientId;
  }

  public void setClientId(String newClientId)
  {
    this.clientId = newClientId;
  }

  public ActiveMQDestination getDestination()
  {
    return this.destination;
  }

  public void setDestination(ActiveMQDestination newDestination)
  {
    this.destination = newDestination;
  }

  public boolean isStarted()
  {
    return this.started;
  }

  public void setStarted(boolean flag)
  {
    this.started = flag;
  }

  public long getStartTime()
  {
    return this.startTime;
  }

  public void setStartTime(long newStartTime)
  {
    this.startTime = newStartTime;
  }
}