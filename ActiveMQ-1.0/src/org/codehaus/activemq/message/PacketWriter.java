package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public abstract interface PacketWriter
{
  public abstract int getPacketType();

  public abstract boolean canWrite(Packet paramPacket);

  public abstract void writePacket(Packet paramPacket, DataOutput paramDataOutput)
    throws IOException;

  public abstract byte[] writePacketToByteArray(Packet paramPacket)
    throws IOException;
}