package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class ProducerInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 18;
  }

  public Packet createPacket()
  {
    return new ProducerInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    ProducerInfo info = (ProducerInfo)packet;
    info.setProducerId(dataIn.readUTF());
    info.setClientId(dataIn.readUTF());
    info.setSessionId(dataIn.readUTF());
    info.setStartTime(dataIn.readLong());
    info.setStarted(dataIn.readBoolean());
    info.setDestination(ActiveMQDestination.readFromStream(dataIn));
  }
}