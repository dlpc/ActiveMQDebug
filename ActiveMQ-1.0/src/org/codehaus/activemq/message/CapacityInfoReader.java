package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class CapacityInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 27;
  }

  public Packet createPacket()
  {
    return new CapacityInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    CapacityInfo info = (CapacityInfo)packet;
    info.setResourceName(dataIn.readUTF());
    info.setCorrelationId(dataIn.readUTF());
    info.setCapacity(dataIn.readByte());
    info.setFlowControlTimeout(dataIn.readInt());
  }
}