package org.codehaus.activemq.message;

import java.util.Properties;

public class ConnectionInfo extends AbstractPacket
{
  String clientId;
  String userName;
  String password;
  String hostName;
  long startTime;
  boolean started;
  boolean closed;
  Properties properties;

  public int getPacketType()
  {
    return 22;
  }

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof ConnectionInfo))) {
      ConnectionInfo info = (ConnectionInfo)obj;
      result = this.clientId == info.clientId;
    }
    return result;
  }

  public int hashCode()
  {
    return this.clientId != null ? this.clientId.hashCode() : super.hashCode();
  }

  public String getClientId()
  {
    return this.clientId;
  }

  public void setClientId(String newClientId)
  {
    this.clientId = newClientId;
  }

  public String getHostName()
  {
    return this.hostName;
  }

  public void setHostName(String newHostName)
  {
    this.hostName = newHostName;
  }

  public String getPassword()
  {
    return this.password;
  }

  public void setPassword(String newPassword)
  {
    this.password = newPassword;
  }

  public Properties getProperties()
  {
    return this.properties;
  }

  public void setProperties(Properties newProperties)
  {
    this.properties = newProperties;
  }

  public long getStartTime()
  {
    return this.startTime;
  }

  public void setStartTime(long newStartTime)
  {
    this.startTime = newStartTime;
  }

  public String getUserName()
  {
    return this.userName;
  }

  public void setUserName(String newUserName)
  {
    this.userName = newUserName;
  }

  public boolean isStarted()
  {
    return this.started;
  }

  public void setStarted(boolean started)
  {
    this.started = started;
  }

  public boolean isClosed()
  {
    return this.closed;
  }

  public void setClosed(boolean closed)
  {
    this.closed = closed;
  }
}