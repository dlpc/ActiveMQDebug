package org.codehaus.activemq.message;

public class IntResponseReceipt extends Receipt
{
  private int result;

  public int getPacketType()
  {
    return 26;
  }

  public int getResult()
  {
    return this.result;
  }

  public void setResult(int result)
  {
    this.result = result;
  }
}