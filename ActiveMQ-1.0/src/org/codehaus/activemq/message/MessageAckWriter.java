package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class MessageAckWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 15;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    MessageAck ack = (MessageAck)packet;
    super.writeUTF(ack.getConsumerId(), dataOut);
    super.writeUTF(ack.getMessageID(), dataOut);
    super.writeUTF(ack.getTransactionId(), dataOut);
    dataOut.writeBoolean(ack.isMessageRead());
    dataOut.writeBoolean(ack.isXaTransacted());
  }
}