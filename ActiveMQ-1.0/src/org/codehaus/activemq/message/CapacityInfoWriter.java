package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class CapacityInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 27;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    CapacityInfo info = (CapacityInfo)packet;
    super.writeUTF(info.getResourceName(), dataOut);
    super.writeUTF(info.getCorrelationId(), dataOut);
    dataOut.writeByte(info.getCapacity());
    dataOut.writeInt((int)info.getFlowControlTimeout());
  }
}