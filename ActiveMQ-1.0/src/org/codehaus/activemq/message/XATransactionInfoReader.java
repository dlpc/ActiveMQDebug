package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class XATransactionInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 20;
  }

  public Packet createPacket()
  {
    return new XATransactionInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    XATransactionInfo info = (XATransactionInfo)packet;
    info.setType(dataIn.readByte());
    switch (info.getType()) {
    case 101:
    case 102:
    case 103:
    case 104:
    case 105:
    case 106:
    case 107:
    case 108:
    case 109:
      info.setXid(ActiveMQXid.read(dataIn));
      break;
    case 112:
      info.setTransactionTimeout(dataIn.readInt());
      break;
    case 110:
      break;
    case 111:
    case 113:
      break;
    default:
      throw new IllegalArgumentException("Invalid type code: " + info.getType());
    }
  }
}