package org.codehaus.activemq.message;

public abstract interface PacketListener
{
  public abstract void consume(Packet paramPacket);
}