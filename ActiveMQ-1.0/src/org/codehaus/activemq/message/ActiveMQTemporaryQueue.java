package org.codehaus.activemq.message;

import javax.jms.JMSException;
import javax.jms.TemporaryQueue;

public class ActiveMQTemporaryQueue extends ActiveMQQueue
  implements TemporaryQueue
{
  public ActiveMQTemporaryQueue()
  {
  }

  public ActiveMQTemporaryQueue(String name)
  {
    super(name);
  }

  public void delete()
    throws JMSException
  {
  }

  public int getDestinationType()
  {
    return 4;
  }
}