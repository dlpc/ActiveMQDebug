package org.codehaus.activemq.message;

public class ReceiptHolder
{
  private Receipt receipt;
  private Object lock = new Object();
  private boolean notified;

  public void setReceipt(Receipt r)
  {
    synchronized (this.lock) {
      this.receipt = r;
      this.notified = true;
      this.lock.notify();
    }
  }

  public Receipt getReceipt()
  {
    return getReceipt(0);
  }

  public Receipt getReceipt(int timeout)
  {
    synchronized (this.lock) {
      if (!this.notified) {
        try {
          this.lock.wait(timeout);
        }
        catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    return this.receipt;
  }

  public void close()
  {
    synchronized (this.lock) {
      this.notified = true;
      this.lock.notifyAll();
    }
  }
}