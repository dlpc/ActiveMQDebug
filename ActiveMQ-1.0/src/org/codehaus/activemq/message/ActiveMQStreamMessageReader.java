package org.codehaus.activemq.message;

public class ActiveMQStreamMessageReader extends ActiveMQMessageReader
{
  public int getPacketType()
  {
    return 10;
  }

  public Packet createPacket()
  {
    return new ActiveMQStreamMessage();
  }
}