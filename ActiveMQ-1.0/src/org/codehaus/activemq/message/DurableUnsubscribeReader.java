package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class DurableUnsubscribeReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 24;
  }

  public Packet createPacket()
  {
    return new DurableUnsubscribe();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    DurableUnsubscribe ds = (DurableUnsubscribe)packet;
    ds.setClientId(dataIn.readUTF());
    ds.setSubscriberName(dataIn.readUTF());
  }
}