package org.codehaus.activemq.message;

public class TransactionInfo extends AbstractPacket
  implements TransactionType
{
  private int type;
  private String transactionId;

  public String getTransactionId()
  {
    return this.transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public int getPacketType()
  {
    return 19;
  }

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof TransactionInfo))) {
      TransactionInfo info = (TransactionInfo)obj;
      result = this.transactionId == info.transactionId;
    }
    return result;
  }

  public int hashCode()
  {
    return this.transactionId != null ? this.transactionId.hashCode() : super.hashCode();
  }

  public int getType()
  {
    return this.type;
  }

  public void setType(int newType)
  {
    this.type = newType;
  }
}