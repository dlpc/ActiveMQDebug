package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class ResponseReceiptWriter extends ReceiptWriter
{
  public int getPacketType()
  {
    return 25;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    ResponseReceipt info = (ResponseReceipt)packet;
    byte[] data = info.getResultBytes();
    if (data == null) {
      dataOut.writeInt(0);
    }
    else {
      dataOut.writeInt(data.length);
      dataOut.write(data);
    }
  }
}