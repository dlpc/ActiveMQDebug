package org.codehaus.activemq.message;

public class CapacityInfo extends AbstractPacket
{
  private String resourceName;
  private String correlationId;
  private int capacity = 100;
  private long flowControlTimeout;

  public int getPacketType()
  {
    return 27;
  }

  public int getCapacity()
  {
    return this.capacity;
  }

  public void setCapacity(int capacity)
  {
    this.capacity = capacity;
  }

  public String getResourceName()
  {
    return this.resourceName;
  }

  public void setResourceName(String resourceName)
  {
    this.resourceName = resourceName;
  }

  public String getCorrelationId()
  {
    return this.correlationId;
  }

  public void setCorrelationId(String correlationId)
  {
    this.correlationId = correlationId;
  }

  public long getFlowControlTimeout()
  {
    return this.flowControlTimeout;
  }

  public void setFlowControlTimeout(long flowControlTimeout)
  {
    this.flowControlTimeout = flowControlTimeout;
  }

  public String toString()
  {
    return "CapacityInfo: cap=" + this.capacity + ",timeout=" + this.flowControlTimeout + ",resource=" + this.resourceName;
  }
}