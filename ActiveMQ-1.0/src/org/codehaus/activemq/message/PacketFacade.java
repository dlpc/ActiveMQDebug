package org.codehaus.activemq.message;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import javax.jms.JMSException;
import org.codehaus.activemq.util.JMSExceptionHelper;

public class PacketFacade
  implements Externalizable
{
  private static final WireFormat wireFormat = new DefaultWireFormat();
  private transient Packet packet;

  public PacketFacade()
  {
  }

  public PacketFacade(Packet packet)
  {
    this.packet = packet;
  }

  public Packet getPacket() {
    return this.packet;
  }

  public void writeExternal(ObjectOutput out) throws IOException {
    try {
      wireFormat.writePacket(this.packet, out);
    }
    catch (JMSException e) {
      throw JMSExceptionHelper.newIOException(e);
    }
  }

  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    int type = in.readByte();
    this.packet = wireFormat.readPacket(type, in);
  }
}