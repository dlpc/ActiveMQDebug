package org.codehaus.activemq.message;

import javax.jms.JMSException;

public abstract interface MessageAcknowledge
{
  public abstract void acknowledge()
    throws JMSException;
}