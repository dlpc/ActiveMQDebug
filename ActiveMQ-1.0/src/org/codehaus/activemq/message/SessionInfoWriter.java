package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class SessionInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 23;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    SessionInfo info = (SessionInfo)packet;
    super.writeUTF(info.getClientId(), dataOut);
    super.writeUTF(info.getSessionId(), dataOut);
    dataOut.writeLong(info.getStartTime());
    dataOut.writeBoolean(info.isStarted());
    dataOut.writeBoolean(info.isReceiptRequired());
  }
}