package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class ConsumerInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 17;
  }

  public Packet createPacket()
  {
    return new ConsumerInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    ConsumerInfo info = (ConsumerInfo)packet;
    info.setConsumerId(dataIn.readUTF());
    info.setClientId(dataIn.readUTF());
    info.setSessionId(dataIn.readUTF());
    info.setSelector(dataIn.readUTF());
    info.setConsumerName(dataIn.readUTF());
    info.setConsumerNo(dataIn.readInt());
    info.setPrefetchNumber(dataIn.readShort());
    info.setStartTime(dataIn.readLong());
    info.setStarted(dataIn.readBoolean());
    info.setReceiptRequired(dataIn.readBoolean());
    info.setNoLocal(dataIn.readBoolean());
    info.setBrowser(dataIn.readBoolean());
    info.setDestination(ActiveMQDestination.readFromStream(dataIn));
  }
}