package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;
import java.util.Properties;

public class BrokerInfoReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 21;
  }

  public Packet createPacket()
  {
    return new BrokerInfo();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    BrokerInfo info = (BrokerInfo)packet;
    info.setBrokerName(dataIn.readUTF());
    info.setClusterName(dataIn.readUTF());
    info.setStartTime(dataIn.readLong());
    info.setProperties((Properties)super.readObject(dataIn));
  }
}