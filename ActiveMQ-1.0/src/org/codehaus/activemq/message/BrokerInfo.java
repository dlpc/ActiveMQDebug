package org.codehaus.activemq.message;

import java.util.Properties;

public class BrokerInfo extends AbstractPacket
{
  private String brokerName;
  private String clusterName;
  private long startTime;
  private Properties properties;

  public int getPacketType()
  {
    return 21;
  }

  public String getBrokerName()
  {
    return this.brokerName;
  }

  public void setBrokerName(String newBrokerName)
  {
    this.brokerName = newBrokerName;
  }

  public String getClusterName()
  {
    return this.clusterName;
  }

  public void setClusterName(String newClusterName)
  {
    this.clusterName = newClusterName;
  }

  public Properties getProperties()
  {
    return this.properties;
  }

  public void setProperties(Properties newProperties)
  {
    this.properties = newProperties;
  }

  public long getStartTime()
  {
    return this.startTime;
  }

  public void setStartTime(long newStartTime)
  {
    this.startTime = newStartTime;
  }
}