package org.codehaus.activemq.message;

public class ActiveMQMapMessageReader extends ActiveMQMessageReader
{
  public int getPacketType()
  {
    return 11;
  }

  public Packet createPacket()
  {
    return new ActiveMQMapMessage();
  }
}