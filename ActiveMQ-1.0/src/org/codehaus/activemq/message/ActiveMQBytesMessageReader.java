package org.codehaus.activemq.message;

public class ActiveMQBytesMessageReader extends ActiveMQMessageReader
{
  public int getPacketType()
  {
    return 9;
  }

  public Packet createPacket()
  {
    return new ActiveMQBytesMessage();
  }
}