package org.codehaus.activemq.message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import javax.jms.JMSException;
import javax.transaction.xa.Xid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ActiveMQXid
  implements Xid, Externalizable, Comparable
{
  private static final long serialVersionUID = -5754338187296859149L;
  private static final Log log = LogFactory.getLog(ActiveMQXid.class);
  private int formatId;
  private byte[] branchQualifier;
  private byte[] globalTransactionId;
  private transient int hash;
  private static final String[] HEX_TABLE = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d", "7e", "7f", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf", "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "da", "db", "dc", "dd", "de", "df", "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff" };

  public static ActiveMQXid fromBytes(byte[] data)
    throws IOException
  {
    return read(new DataInputStream(new ByteArrayInputStream(data)));
  }

  public ActiveMQXid()
  {
  }

  public ActiveMQXid(Xid xid)
  {
    this.formatId = xid.getFormatId();
    this.branchQualifier = xid.getBranchQualifier();
    this.globalTransactionId = xid.getGlobalTransactionId();
  }

  public ActiveMQXid(int formatId, byte[] branchQualifier, byte[] globalTransactionId) {
    this.formatId = formatId;
    this.branchQualifier = branchQualifier;
    this.globalTransactionId = globalTransactionId;
  }

  public ActiveMQXid(String txid)
    throws JMSException
  {
    String[] parts = txid.split(":", 3);
    if (parts.length != 3) {
      throw new JMSException("Invalid XID: " + txid);
    }
    this.formatId = Integer.parseInt(parts[0]);

    if (log.isDebugEnabled()) {
      log.debug("parts:" + parts[0]);
      log.debug("parts:" + parts[1]);
      log.debug("parts:" + parts[2]);
    }
    this.globalTransactionId = toBytesFromHex(parts[1]);
    this.branchQualifier = toBytesFromHex(parts[2]);
  }

  public int hashCode() {
    if (this.hash == 0) {
      this.hash = this.formatId;
      this.hash = hash(this.branchQualifier, this.hash);
      this.hash = hash(this.globalTransactionId, this.hash);
    }
    if (this.hash == 0) {
      this.hash = 11332302;
    }
    return this.hash;
  }

  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if ((hashCode() == that.hashCode()) && ((that instanceof ActiveMQXid))) {
      return equals((ActiveMQXid)that);
    }
    return false;
  }

  public boolean equals(ActiveMQXid that) {
    return (this.formatId == that.formatId) && (equals(this.branchQualifier, that.branchQualifier)) && (equals(this.globalTransactionId, that.globalTransactionId));
  }

  public int compareTo(Object object) {
    if (this == object) {
      return 0;
    }

    if ((object instanceof ActiveMQXid)) {
      ActiveMQXid that = (ActiveMQXid)object;
      int diff = this.formatId - that.formatId;
      if (diff == 0) {
        diff = compareTo(this.branchQualifier, that.branchQualifier);
        if (diff == 0) {
          diff = compareTo(this.globalTransactionId, that.globalTransactionId);
        }
      }
      return diff;
    }

    return -1;
  }

  public String toLocalTransactionId()
  {
    StringBuffer rc = new StringBuffer(13 + this.globalTransactionId.length * 2 + this.branchQualifier.length * 2);
    rc.append(this.formatId);
    rc.append(":");
    rc.append(toHexFromBytes(this.globalTransactionId));
    rc.append(":");
    rc.append(toHexFromBytes(this.branchQualifier));
    return rc.toString();
  }

  public byte[] getBranchQualifier()
  {
    return this.branchQualifier;
  }

  public int getFormatId()
  {
    return this.formatId;
  }

  public byte[] getGlobalTransactionId()
  {
    return this.globalTransactionId;
  }

  public String toString()
  {
    return "XID:" + toLocalTransactionId();
  }

  public void writeExternal(ObjectOutput out) throws IOException
  {
    write(out);
  }

  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    readState(in);
  }

  public void readState(DataInput dataIn) throws IOException {
    this.formatId = dataIn.readInt();
    this.branchQualifier = readBytes(dataIn);
    this.globalTransactionId = readBytes(dataIn);
  }

  public static ActiveMQXid read(DataInput dataIn)
    throws IOException
  {
    ActiveMQXid answer = new ActiveMQXid();
    answer.readState(dataIn);
    return answer;
  }

  public byte[] toBytes() throws IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    write(new DataOutputStream(buffer));
    return buffer.toByteArray();
  }

  public void write(DataOutput dataOut)
    throws IOException
  {
    dataOut.writeInt(this.formatId);
    writeBytes(dataOut, this.branchQualifier);
    writeBytes(dataOut, this.globalTransactionId);
  }

  protected void writeBytes(DataOutput dataOut, byte[] data) throws IOException {
    dataOut.writeInt(data.length);
    dataOut.write(data);
  }

  protected static byte[] readBytes(DataInput dataIn) throws IOException {
    int size = dataIn.readInt();
    byte[] data = new byte[size];
    dataIn.readFully(data);
    return data;
  }

  protected boolean equals(byte[] left, byte[] right)
  {
    if (left == right) {
      return true;
    }
    int size = left.length;
    if (size != right.length) {
      return false;
    }
    for (int i = 0; i < size; i++) {
      if (left[i] != right[i]) {
        return false;
      }
    }
    return true;
  }

  protected int compareTo(byte[] left, byte[] right) {
    if (left == right) {
      return 0;
    }
    int size = left.length;
    int answer = size - right.length;
    if (answer == 0) {
      for (int i = 0; i < size; i++) {
        answer = left[i] - right[i];
        if (answer != 0) {
          break;
        }
      }
    }
    return answer;
  }

  protected int hash(byte[] bytes, int hash) {
    int i = 0; for (int size = bytes.length; i < size; i++) {
      hash ^= bytes[i] << i % 4 * 8;
    }
    return hash;
  }

  private byte[] toBytesFromHex(String hex)
  {
    byte[] rc = new byte[hex.length() / 2];
    for (int i = 0; i < rc.length; i++) {
      String h = hex.substring(i * 2, i * 2 + 2);
      int x = Integer.parseInt(h, 16);
      rc[i] = (byte)x;
    }
    return rc;
  }

  private String toHexFromBytes(byte[] bytes)
  {
    StringBuffer rc = new StringBuffer(bytes.length * 2);
    for (int i = 0; i < bytes.length; i++) {
      rc.append(HEX_TABLE[(0xFF & bytes[i])]);
    }
    return rc.toString();
  }
}