package org.codehaus.activemq.message;

public class ConsumerInfo extends AbstractPacket
{
  private ActiveMQDestination destination;
  private String consumerId;
  private String clientId;
  private String sessionId;
  private String consumerName;
  private String selector;
  private long startTime;
  private boolean started;
  private int consumerNo;
  private boolean noLocal;
  private boolean browser;
  private int prefetchNumber = 100;

  public String getConsumerId()
  {
    return this.consumerId;
  }

  public void setConsumerId(String consumerId)
  {
    this.consumerId = consumerId;
  }

  public String getSessionId()
  {
    return this.sessionId;
  }

  public void setSessionId(String sessionId)
  {
    this.sessionId = sessionId;
  }

  public int getPacketType()
  {
    return 17;
  }

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof ConsumerInfo))) {
      ConsumerInfo that = (ConsumerInfo)obj;
      result = this.consumerId.equals(that.consumerId);
    }
    return result;
  }

  public int hashCode()
  {
    return this.consumerId.hashCode();
  }

  public String toString()
  {
    return super.toString() + " consumerId: " + this.consumerId + " clientId: " + this.clientId + " consumerName: " + this.consumerName + " destination: " + this.destination;
  }

  public String getClientId()
  {
    return this.clientId;
  }

  public void setClientId(String newClientId)
  {
    this.clientId = newClientId;
  }

  public ActiveMQDestination getDestination()
  {
    return this.destination;
  }

  public void setDestination(ActiveMQDestination newDestination)
  {
    this.destination = newDestination;
  }

  public String getSelector()
  {
    return this.selector;
  }

  public void setSelector(String newSelector)
  {
    this.selector = newSelector;
  }

  public boolean isStarted()
  {
    return this.started;
  }

  public void setStarted(boolean flag)
  {
    this.started = flag;
  }

  public long getStartTime()
  {
    return this.startTime;
  }

  public void setStartTime(long newStartTime)
  {
    this.startTime = newStartTime;
  }

  public int getConsumerNo()
  {
    return this.consumerNo;
  }

  public void setConsumerNo(int newConsumerNo)
  {
    this.consumerNo = newConsumerNo;
  }

  public String getConsumerName()
  {
    return this.consumerName;
  }

  public void setConsumerName(String newconsumerName)
  {
    this.consumerName = newconsumerName;
  }

  public boolean isDurableTopic()
  {
    return (this.destination.isTopic()) && (!this.destination.isTemporary()) && (this.consumerName != null) && (this.consumerName.length() > 0);
  }

  public boolean isNoLocal()
  {
    return this.noLocal;
  }

  public void setNoLocal(boolean noLocal)
  {
    this.noLocal = noLocal;
  }

  public boolean isBrowser()
  {
    return this.browser;
  }

  public void setBrowser(boolean browser)
  {
    this.browser = browser;
  }

  public int getPrefetchNumber()
  {
    return this.prefetchNumber;
  }

  public void setPrefetchNumber(int prefetchNumber)
  {
    this.prefetchNumber = prefetchNumber;
  }

  public String getConsumerKey()
  {
    return "[" + getClientId() + ":" + getConsumerName() + "]";
  }
}