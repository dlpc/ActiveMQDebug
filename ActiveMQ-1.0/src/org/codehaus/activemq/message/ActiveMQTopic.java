package org.codehaus.activemq.message;

import javax.jms.Topic;

public class ActiveMQTopic extends ActiveMQDestination
  implements Topic
{
  public ActiveMQTopic()
  {
  }

  public ActiveMQTopic(String name)
  {
    super(name);
  }

  public String getTopicName()
  {
    return super.getPhysicalName();
  }

  public int getDestinationType()
  {
    return 1;
  }
}