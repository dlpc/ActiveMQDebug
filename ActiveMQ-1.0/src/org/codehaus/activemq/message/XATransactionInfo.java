package org.codehaus.activemq.message;

public class XATransactionInfo extends AbstractPacket
  implements TransactionType
{
  private ActiveMQXid xid;
  private int type;
  private int transactionTimeout;

  public int getPacketType()
  {
    return 20;
  }

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof XATransactionInfo))) {
      XATransactionInfo info = (XATransactionInfo)obj;
      result = (this.xid.equals(info.xid)) && (this.type == info.type);
    }
    return result;
  }

  public int hashCode()
  {
    return this.xid.hashCode() ^ this.type;
  }

  public int getType()
  {
    return this.type;
  }

  public void setType(int newType)
  {
    this.type = newType;
  }

  public ActiveMQXid getXid() {
    return this.xid;
  }

  public void setXid(ActiveMQXid xid) {
    this.xid = xid;
  }

  public int getTransactionTimeout()
  {
    return this.transactionTimeout;
  }

  public void setTransactionTimeout(int transactionTimeout)
  {
    this.transactionTimeout = transactionTimeout;
  }
}