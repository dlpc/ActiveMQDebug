package org.codehaus.activemq.message;

public class ActiveMQObjectMessageReader extends ActiveMQMessageReader
{
  public int getPacketType()
  {
    return 8;
  }

  public Packet createPacket()
  {
    return new ActiveMQObjectMessage();
  }
}