package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public abstract interface PacketReader
{
  public abstract int getPacketType();

  public abstract boolean canRead(int paramInt);

  public abstract Packet createPacket();

  public abstract void buildPacket(Packet paramPacket, DataInput paramDataInput)
    throws IOException;

  public abstract Packet readPacketFromByteArray(byte[] paramArrayOfByte)
    throws IOException;
}