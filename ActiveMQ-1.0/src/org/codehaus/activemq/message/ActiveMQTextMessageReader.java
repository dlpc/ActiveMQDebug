package org.codehaus.activemq.message;

public class ActiveMQTextMessageReader extends ActiveMQMessageReader
{
  public int getPacketType()
  {
    return 7;
  }

  public Packet createPacket()
  {
    return new ActiveMQTextMessage();
  }
}