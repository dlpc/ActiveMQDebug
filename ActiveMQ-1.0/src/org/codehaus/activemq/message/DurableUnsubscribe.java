package org.codehaus.activemq.message;

public class DurableUnsubscribe extends AbstractPacket
{
  private String clientId;
  private String subscriberName;

  public int getPacketType()
  {
    return 24;
  }

  public String getClientId()
  {
    return this.clientId;
  }

  public void setClientId(String clientId)
  {
    this.clientId = clientId;
  }

  public String getSubscriberName()
  {
    return this.subscriberName;
  }

  public void setSubscriberName(String subscriberName)
  {
    this.subscriberName = subscriberName;
  }
}