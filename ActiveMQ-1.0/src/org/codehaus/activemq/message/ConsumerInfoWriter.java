package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class ConsumerInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 17;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    ConsumerInfo info = (ConsumerInfo)packet;
    super.writeUTF(info.getConsumerId(), dataOut);
    super.writeUTF(info.getClientId(), dataOut);
    super.writeUTF(info.getSessionId(), dataOut);
    super.writeUTF(info.getSelector(), dataOut);
    super.writeUTF(info.getConsumerName(), dataOut);
    dataOut.writeInt(info.getConsumerNo());
    dataOut.writeShort(info.getPrefetchNumber());
    dataOut.writeLong(info.getStartTime());
    dataOut.writeBoolean(info.isStarted());
    dataOut.writeBoolean(info.isReceiptRequired());
    dataOut.writeBoolean(info.isNoLocal());
    dataOut.writeBoolean(info.isBrowser());
    ActiveMQDestination.writeToStream(info.getDestination(), dataOut);
  }
}