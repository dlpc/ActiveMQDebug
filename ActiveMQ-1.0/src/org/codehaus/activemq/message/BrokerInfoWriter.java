package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class BrokerInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 21;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    BrokerInfo info = (BrokerInfo)packet;
    super.writeUTF(info.getBrokerName(), dataOut);
    super.writeUTF(info.getClusterName(), dataOut);
    dataOut.writeLong(info.getStartTime());
    super.writeObject(info.getProperties(), dataOut);
  }
}