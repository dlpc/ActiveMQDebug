package org.codehaus.activemq.message;

import javax.jms.JMSException;
import javax.jms.TemporaryTopic;

public class ActiveMQTemporaryTopic extends ActiveMQTopic
  implements TemporaryTopic
{
  public ActiveMQTemporaryTopic()
  {
  }

  public ActiveMQTemporaryTopic(String name)
  {
    super(name);
  }

  public void delete()
    throws JMSException
  {
  }

  public int getDestinationType()
  {
    return 4;
  }
}