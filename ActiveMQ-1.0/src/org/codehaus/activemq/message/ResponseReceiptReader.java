package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class ResponseReceiptReader extends ReceiptReader
{
  public int getPacketType()
  {
    return 25;
  }

  public Packet createPacket()
  {
    return new ResponseReceipt();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    ResponseReceipt info = (ResponseReceipt)packet;
    int size = dataIn.readInt();
    byte[] data = null;
    if (size != 0) {
      data = new byte[size];
      dataIn.readFully(data);
    }
    info.setResultBytes(data);
  }
}