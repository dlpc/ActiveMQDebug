package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class ReceiptReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 16;
  }

  public Packet createPacket()
  {
    return new Receipt();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    Receipt info = (Receipt)packet;
    info.setCorrelationId(dataIn.readUTF());
    info.setFailed(dataIn.readBoolean());
    info.setException((Throwable)super.readObject(dataIn));
    info.setBrokerMessageCapacity(dataIn.readByte());
  }
}