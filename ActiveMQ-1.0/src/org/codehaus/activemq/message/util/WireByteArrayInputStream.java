package org.codehaus.activemq.message.util;

import java.io.ByteArrayInputStream;

public class WireByteArrayInputStream extends ByteArrayInputStream
{
  public WireByteArrayInputStream(byte[] buf)
  {
    super(buf);
  }

  public WireByteArrayInputStream(byte[] buf, int offset, int length)
  {
    super(buf, offset, length);
  }

  public WireByteArrayInputStream()
  {
    super(new byte[0]);
  }

  public void restart(byte[] newBuff, int offset, int length)
  {
    this.buf = newBuff;
    this.pos = offset;
    this.count = Math.min(offset + length, newBuff.length);
    this.mark = offset;
  }

  public void restart(byte[] newBuff)
  {
    restart(newBuff, 0, newBuff.length);
  }

  public int read()
  {
    return this.pos < this.count ? this.buf[(this.pos++)] & 0xFF : -1;
  }

  public int read(byte[] b, int off, int len)
  {
    if (b == null) {
      throw new NullPointerException();
    }
    if ((off < 0) || (off > b.length) || (len < 0) || (off + len > b.length) || (off + len < 0)) {
      throw new IndexOutOfBoundsException();
    }
    if (this.pos >= this.count) {
      return -1;
    }
    if (this.pos + len > this.count) {
      len = this.count - this.pos;
    }
    if (len <= 0) {
      return 0;
    }
    System.arraycopy(this.buf, this.pos, b, off, len);
    this.pos += len;
    return len;
  }

  public int available()
  {
    return this.count - this.pos;
  }
}