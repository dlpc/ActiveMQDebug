package org.codehaus.activemq.message.util;

import java.util.List;
import javax.jms.JMSException;
import org.codehaus.activemq.message.Packet;

public abstract interface BoundedPacketQueue
{
  public abstract String getName();

  public abstract int size();

  public abstract void close();

  public abstract void enqueueNoBlock(Packet paramPacket)
    throws JMSException;

  public abstract void enqueue(Packet paramPacket)
    throws InterruptedException, JMSException;

  public abstract Packet dequeue()
    throws InterruptedException, JMSException;

  public abstract Packet dequeue(long paramLong)
    throws InterruptedException, JMSException;

  public abstract Packet dequeueNoWait()
    throws InterruptedException, JMSException;

  public abstract boolean isStarted();

  public abstract void stop();

  public abstract void start();

  public abstract boolean isEmpty();

  public abstract void clear();

  public abstract List getContents();
}