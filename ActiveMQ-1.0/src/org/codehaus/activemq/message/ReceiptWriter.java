package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class ReceiptWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 16;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    Receipt info = (Receipt)packet;
    super.writeUTF(info.getCorrelationId(), dataOut);
    dataOut.writeBoolean(info.isFailed());
    super.writeObject(info.getException(), dataOut);
    dataOut.writeByte(info.getBrokerMessageCapacity());
  }
}