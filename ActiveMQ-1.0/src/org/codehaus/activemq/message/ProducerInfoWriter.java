package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class ProducerInfoWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 18;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    ProducerInfo info = (ProducerInfo)packet;
    super.writeUTF(info.getProducerId(), dataOut);
    super.writeUTF(info.getClientId(), dataOut);
    super.writeUTF(info.getSessionId(), dataOut);
    dataOut.writeLong(info.getStartTime());
    dataOut.writeBoolean(info.isStarted());
    ActiveMQDestination.writeToStream(info.getDestination(), dataOut);
  }
}