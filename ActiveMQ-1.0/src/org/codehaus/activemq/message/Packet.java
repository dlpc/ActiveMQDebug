package org.codehaus.activemq.message;

public abstract interface Packet
{
  public static final int ACTIVEMQ_MESSAGE = 6;
  public static final int ACTIVEMQ_TEXT_MESSAGE = 7;
  public static final int ACTIVEMQ_OBJECT_MESSAGE = 8;
  public static final int ACTIVEMQ_BYTES_MESSAGE = 9;
  public static final int ACTIVEMQ_STREAM_MESSAGE = 10;
  public static final int ACTIVEMQ_MAP_MESSAGE = 11;
  public static final int ACTIVEMQ_MSG_ACK = 15;
  public static final int RECEIPT_INFO = 16;
  public static final int CONSUMER_INFO = 17;
  public static final int PRODUCER_INFO = 18;
  public static final int TRANSACTION_INFO = 19;
  public static final int XA_TRANSACTION_INFO = 20;
  public static final int ACTIVEMQ_BROKER_INFO = 21;
  public static final int ACTIVEMQ_CONNECTION_INFO = 22;
  public static final int SESSION_INFO = 23;
  public static final int DURABLE_UNSUBSCRIBE = 24;
  public static final int RESPONSE_RECEIPT_INFO = 25;
  public static final int INT_RESPONSE_RECEIPT_INFO = 26;
  public static final int CAPACITY_INFO = 27;
  public static final int CAPACITY_INFO_REQUEST = 28;

  public abstract int getPacketType();

  public abstract String getId();

  public abstract void setId(String paramString);

  public abstract boolean isReceiptRequired();

  public abstract boolean isReceipt();

  public abstract void setReceiptRequired(boolean paramBoolean);

  public abstract boolean isJMSMessage();

  public abstract int getMemoryUsage();

  public abstract void setMemoryUsage(int paramInt);

  public abstract int incrementMemoryReferenceCount();

  public abstract int decrementMemoryReferenceCount();

  public abstract int getMemoryUsageReferenceCount();
}