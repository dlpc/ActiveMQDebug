package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class IntResponseReceiptWriter extends ReceiptWriter
{
  public int getPacketType()
  {
    return 26;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    IntResponseReceipt info = (IntResponseReceipt)packet;
    dataOut.writeInt(info.getResult());
  }
}