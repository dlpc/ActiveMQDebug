package org.codehaus.activemq.message;

public class Receipt extends AbstractPacket
{
  private String correlationId;
  private Throwable exception;
  private boolean failed;
  private int brokerMessageCapacity = 100;

  public Throwable getException()
  {
    return this.exception;
  }

  public void setException(Throwable exception)
  {
    this.exception = exception;
  }

  public int getPacketType()
  {
    return 16;
  }

  public boolean isReceipt()
  {
    return true;
  }

  public String getCorrelationId()
  {
    return this.correlationId;
  }

  public void setCorrelationId(String newCorrelationId)
  {
    this.correlationId = newCorrelationId;
  }

  public boolean isFailed()
  {
    return this.failed;
  }

  public void setFailed(boolean newFailed)
  {
    this.failed = newFailed;
  }

  public String toString()
  {
    String str = super.toString();
    str = str + " correlationId = " + this.correlationId + " failed = " + this.failed + " exp = " + this.exception;
    str = str + " , brokerMessageCapacity = " + this.brokerMessageCapacity;
    return str;
  }

  public int getBrokerMessageCapacity()
  {
    return this.brokerMessageCapacity;
  }

  public void setBrokerMessageCapacity(int brokerMessageCapacity)
  {
    this.brokerMessageCapacity = brokerMessageCapacity;
  }
}