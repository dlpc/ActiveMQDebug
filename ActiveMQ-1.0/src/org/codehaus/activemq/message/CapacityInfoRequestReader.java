package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class CapacityInfoRequestReader extends AbstractPacketReader
{
  public int getPacketType()
  {
    return 28;
  }

  public Packet createPacket()
  {
    return new CapacityInfoRequest();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
  }
}