package org.codehaus.activemq.message;

import javax.jms.Queue;

public class ActiveMQQueue extends ActiveMQDestination
  implements Queue
{
  public ActiveMQQueue()
  {
  }

  public ActiveMQQueue(String name)
  {
    super(name);
  }

  public String getQueueName()
  {
    return super.getPhysicalName();
  }

  public int getDestinationType()
  {
    return 3;
  }
}