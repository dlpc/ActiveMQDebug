package org.codehaus.activemq.message;

import java.io.DataInput;
import java.io.IOException;

public class IntResponseReceiptReader extends ReceiptReader
{
  public int getPacketType()
  {
    return 26;
  }

  public Packet createPacket()
  {
    return new IntResponseReceipt();
  }

  public void buildPacket(Packet packet, DataInput dataIn)
    throws IOException
  {
    super.buildPacket(packet, dataIn);
    IntResponseReceipt info = (IntResponseReceipt)packet;
    info.setResult(dataIn.readInt());
  }
}