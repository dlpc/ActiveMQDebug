package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class CapacityInfoRequestWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 28;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
  }
}