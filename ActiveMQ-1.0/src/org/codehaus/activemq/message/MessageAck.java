package org.codehaus.activemq.message;

import org.codehaus.activemq.service.MessageIdentity;

public class MessageAck extends AbstractPacket
{
  private String consumerId;
  private String messageID;
  private String transactionId;
  private boolean messageRead;
  private boolean xaTransacted;
  private transient MessageIdentity messageIdentity;

  public int getPacketType()
  {
    return 15;
  }

  public String toString()
  {
    String str = super.toString();
    str = str + " consumerId = " + this.consumerId;
    str = str + " , messageId = " + this.messageID;
    str = str + " ,read = " + this.messageRead;
    str = str + " ,trans = " + this.transactionId;
    return str;
  }

  public String getTransactionId()
  {
    return this.transactionId;
  }

  public void setTransactionId(String newTransactionId)
  {
    this.transactionId = newTransactionId;
  }

  public boolean isPartOfTransaction()
  {
    return (this.transactionId != null) && (this.transactionId.length() > 0);
  }

  public String getMessageID()
  {
    return this.messageID;
  }

  public void setMessageID(String messageID)
  {
    this.messageID = messageID;
  }

  public boolean isMessageRead()
  {
    return this.messageRead;
  }

  public void setMessageRead(boolean messageRead)
  {
    this.messageRead = messageRead;
  }

  public String getConsumerId()
  {
    return this.consumerId;
  }

  public void setConsumerId(String consumerId)
  {
    this.consumerId = consumerId;
  }

  public boolean isXaTransacted()
  {
    return this.xaTransacted;
  }

  public void setXaTransacted(boolean xaTransacted)
  {
    this.xaTransacted = xaTransacted;
  }

  public MessageIdentity getMessageIdentity() {
    if (this.messageIdentity == null) {
      this.messageIdentity = new MessageIdentity(this.messageID);
    }
    return this.messageIdentity;
  }
}