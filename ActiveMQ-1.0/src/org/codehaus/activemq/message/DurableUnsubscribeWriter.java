package org.codehaus.activemq.message;

import java.io.DataOutput;
import java.io.IOException;

public class DurableUnsubscribeWriter extends AbstractPacketWriter
{
  public int getPacketType()
  {
    return 24;
  }

  public void writePacket(Packet packet, DataOutput dataOut)
    throws IOException
  {
    super.writePacket(packet, dataOut);
    DurableUnsubscribe ds = (DurableUnsubscribe)packet;
    super.writeUTF(ds.getClientId(), dataOut);
    super.writeUTF(ds.getSubscriberName(), dataOut);
  }
}