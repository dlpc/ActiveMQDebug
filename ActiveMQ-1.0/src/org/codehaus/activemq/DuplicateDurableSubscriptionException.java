package org.codehaus.activemq;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ConsumerInfo;

public class DuplicateDurableSubscriptionException extends JMSException
{
  private String clientID;
  private String consumerName;

  public DuplicateDurableSubscriptionException(ConsumerInfo info)
  {
    super("Duplicate JMS subscription for clientID: " + info.getClientId() + " and consumer: " + info.getConsumerName(), "AMQ-1000");
    this.clientID = info.getClientId();
    this.consumerName = info.getConsumerName();
  }

  public String getClientID() {
    return this.clientID;
  }

  public String getConsumerName() {
    return this.consumerName;
  }
}