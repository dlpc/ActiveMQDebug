package org.codehaus.activemq.selector;

import java.io.IOException;
import java.io.PrintStream;

public class SelectorParserTokenManager
  implements SelectorParserConstants
{
  public PrintStream debugStream = System.out;

  static final long[] jjbitVec0 = { -2L, -1L, -1L, -1L };

  static final long[] jjbitVec2 = { 0L, 0L, -1L, -1L };

  static final int[] jjnextStates = { 26, 27, 28, 33, 34, 20, 21, 22, 1, 2, 4, 8, 9, 11, 16, 17, 31, 32, 35, 36 };

  public static final String[] jjstrLiteralImages = { "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "=", "<>", ">", ">=", "<", "<=", "(", ",", ")", "+", "-", "*", "/", "%" };

  public static final String[] lexStateNames = { "DEFAULT" };

  static final long[] jjtoToken = { 274875809537L };

  static final long[] jjtoSkip = { 254L };

  static final long[] jjtoSpecial = { 62L };
  private SimpleCharStream input_stream;
  private final int[] jjrounds = new int[37];
  private final int[] jjstateSet = new int[74];
  protected char curChar;
  int curLexState = 0;
  int defaultLexState = 0;
  int jjnewStateCnt;
  int jjround;
  int jjmatchedPos;
  int jjmatchedKind;

  public void setDebugStream(PrintStream ds)
  {
    this.debugStream = ds;
  }

  private final int jjStopAtPos(int pos, int kind) {
    this.jjmatchedKind = kind;
    this.jjmatchedPos = pos;
    return pos + 1;
  }

  private final int jjMoveStringLiteralDfa0_0() {
    switch (this.curChar) {
    case '\t':
      this.jjmatchedKind = 2;
      return jjMoveNfa_0(5, 0);
    case '\n':
      this.jjmatchedKind = 3;
      return jjMoveNfa_0(5, 0);
    case '\f':
      this.jjmatchedKind = 5;
      return jjMoveNfa_0(5, 0);
    case '\r':
      this.jjmatchedKind = 4;
      return jjMoveNfa_0(5, 0);
    case ' ':
      this.jjmatchedKind = 1;
      return jjMoveNfa_0(5, 0);
    case '%':
      this.jjmatchedKind = 37;
      return jjMoveNfa_0(5, 0);
    case '(':
      this.jjmatchedKind = 30;
      return jjMoveNfa_0(5, 0);
    case ')':
      this.jjmatchedKind = 32;
      return jjMoveNfa_0(5, 0);
    case '*':
      this.jjmatchedKind = 35;
      return jjMoveNfa_0(5, 0);
    case '+':
      this.jjmatchedKind = 33;
      return jjMoveNfa_0(5, 0);
    case ',':
      this.jjmatchedKind = 31;
      return jjMoveNfa_0(5, 0);
    case '-':
      this.jjmatchedKind = 34;
      return jjMoveNfa_0(5, 0);
    case '/':
      this.jjmatchedKind = 36;
      return jjMoveNfa_0(5, 0);
    case '<':
      this.jjmatchedKind = 28;
      return jjMoveStringLiteralDfa1_0(570425344L);
    case '=':
      this.jjmatchedKind = 24;
      return jjMoveNfa_0(5, 0);
    case '>':
      this.jjmatchedKind = 26;
      return jjMoveStringLiteralDfa1_0(134217728L);
    case 'A':
      return jjMoveStringLiteralDfa1_0(512L);
    case 'B':
      return jjMoveStringLiteralDfa1_0(2048L);
    case 'E':
      return jjMoveStringLiteralDfa1_0(8192L);
    case 'F':
      return jjMoveStringLiteralDfa1_0(131072L);
    case 'I':
      return jjMoveStringLiteralDfa1_0(49152L);
    case 'L':
      return jjMoveStringLiteralDfa1_0(4096L);
    case 'N':
      return jjMoveStringLiteralDfa1_0(262400L);
    case 'O':
      return jjMoveStringLiteralDfa1_0(1024L);
    case 'T':
      return jjMoveStringLiteralDfa1_0(65536L);
    case 'a':
      return jjMoveStringLiteralDfa1_0(512L);
    case 'b':
      return jjMoveStringLiteralDfa1_0(2048L);
    case 'e':
      return jjMoveStringLiteralDfa1_0(8192L);
    case 'f':
      return jjMoveStringLiteralDfa1_0(131072L);
    case 'i':
      return jjMoveStringLiteralDfa1_0(49152L);
    case 'l':
      return jjMoveStringLiteralDfa1_0(4096L);
    case 'n':
      return jjMoveStringLiteralDfa1_0(262400L);
    case 'o':
      return jjMoveStringLiteralDfa1_0(1024L);
    case 't':
      return jjMoveStringLiteralDfa1_0(65536L);
    case '\013':
    case '\016':
    case '\017':
    case '\020':
    case '\021':
    case '\022':
    case '\023':
    case '\024':
    case '\025':
    case '\026':
    case '\027':
    case '\030':
    case '\031':
    case '\032':
    case '\033':
    case '\034':
    case '\035':
    case '\036':
    case '\037':
    case '!':
    case '"':
    case '#':
    case '$':
    case '&':
    case '\'':
    case '.':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case ':':
    case ';':
    case '?':
    case '@':
    case 'C':
    case 'D':
    case 'G':
    case 'H':
    case 'J':
    case 'K':
    case 'M':
    case 'P':
    case 'Q':
    case 'R':
    case 'S':
    case 'U':
    case 'V':
    case 'W':
    case 'X':
    case 'Y':
    case 'Z':
    case '[':
    case '\\':
    case ']':
    case '^':
    case '_':
    case '`':
    case 'c':
    case 'd':
    case 'g':
    case 'h':
    case 'j':
    case 'k':
    case 'm':
    case 'p':
    case 'q':
    case 'r':
    case 's': } return jjMoveNfa_0(5, 0);
  }

  private final int jjMoveStringLiteralDfa1_0(long active0)
  {
    try {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      return jjMoveNfa_0(5, 0);
    }
    switch (this.curChar) {
    case '=':
      if ((active0 & 0x8000000) != 0L) {
        this.jjmatchedKind = 27;
        this.jjmatchedPos = 1;
      } else {
        if ((active0 & 0x20000000) == 0L) break;
        this.jjmatchedKind = 29;
        this.jjmatchedPos = 1; } break;
    case '>':
      if ((active0 & 0x2000000) == 0L) break;
      this.jjmatchedKind = 25;
      this.jjmatchedPos = 1; break;
    case 'A':
      return jjMoveStringLiteralDfa2_0(active0, 131072L);
    case 'E':
      return jjMoveStringLiteralDfa2_0(active0, 2048L);
    case 'I':
      return jjMoveStringLiteralDfa2_0(active0, 4096L);
    case 'N':
      if ((active0 & 0x4000) != 0L) {
        this.jjmatchedKind = 14;
        this.jjmatchedPos = 1;
      }
      return jjMoveStringLiteralDfa2_0(active0, 512L);
    case 'O':
      return jjMoveStringLiteralDfa2_0(active0, 256L);
    case 'R':
      if ((active0 & 0x400) != 0L) {
        this.jjmatchedKind = 10;
        this.jjmatchedPos = 1;
      }
      return jjMoveStringLiteralDfa2_0(active0, 65536L);
    case 'S':
      if ((active0 & 0x8000) != 0L) {
        this.jjmatchedKind = 15;
        this.jjmatchedPos = 1;
      }
      return jjMoveStringLiteralDfa2_0(active0, 8192L);
    case 'U':
      return jjMoveStringLiteralDfa2_0(active0, 262144L);
    case 'a':
      return jjMoveStringLiteralDfa2_0(active0, 131072L);
    case 'e':
      return jjMoveStringLiteralDfa2_0(active0, 2048L);
    case 'i':
      return jjMoveStringLiteralDfa2_0(active0, 4096L);
    case 'n':
      if ((active0 & 0x4000) != 0L) {
        this.jjmatchedKind = 14;
        this.jjmatchedPos = 1;
      }
      return jjMoveStringLiteralDfa2_0(active0, 512L);
    case 'o':
      return jjMoveStringLiteralDfa2_0(active0, 256L);
    case 'r':
      if ((active0 & 0x400) != 0L) {
        this.jjmatchedKind = 10;
        this.jjmatchedPos = 1;
      }
      return jjMoveStringLiteralDfa2_0(active0, 65536L);
    case 's':
      if ((active0 & 0x8000) != 0L) {
        this.jjmatchedKind = 15;
        this.jjmatchedPos = 1;
      }
      return jjMoveStringLiteralDfa2_0(active0, 8192L);
    case 'u':
      return jjMoveStringLiteralDfa2_0(active0, 262144L);
    case '?':
    case '@':
    case 'B':
    case 'C':
    case 'D':
    case 'F':
    case 'G':
    case 'H':
    case 'J':
    case 'K':
    case 'L':
    case 'M':
    case 'P':
    case 'Q':
    case 'T':
    case 'V':
    case 'W':
    case 'X':
    case 'Y':
    case 'Z':
    case '[':
    case '\\':
    case ']':
    case '^':
    case '_':
    case '`':
    case 'b':
    case 'c':
    case 'd':
    case 'f':
    case 'g':
    case 'h':
    case 'j':
    case 'k':
    case 'l':
    case 'm':
    case 'p':
    case 'q':
    case 't': } return jjMoveNfa_0(5, 1);
  }

  private final int jjMoveStringLiteralDfa2_0(long old0, long active0) {
    if ((active0 &= old0) == 0L)
      return jjMoveNfa_0(5, 1);
    try
    {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      return jjMoveNfa_0(5, 1);
    }
    switch (this.curChar) {
    case 'C':
      return jjMoveStringLiteralDfa3_0(active0, 8192L);
    case 'D':
      if ((active0 & 0x200) == 0L) break;
      this.jjmatchedKind = 9;
      this.jjmatchedPos = 2; break;
    case 'K':
      return jjMoveStringLiteralDfa3_0(active0, 4096L);
    case 'L':
      return jjMoveStringLiteralDfa3_0(active0, 393216L);
    case 'T':
      if ((active0 & 0x100) != 0L) {
        this.jjmatchedKind = 8;
        this.jjmatchedPos = 2;
      }
      return jjMoveStringLiteralDfa3_0(active0, 2048L);
    case 'U':
      return jjMoveStringLiteralDfa3_0(active0, 65536L);
    case 'c':
      return jjMoveStringLiteralDfa3_0(active0, 8192L);
    case 'd':
      if ((active0 & 0x200) == 0L) break;
      this.jjmatchedKind = 9;
      this.jjmatchedPos = 2; break;
    case 'k':
      return jjMoveStringLiteralDfa3_0(active0, 4096L);
    case 'l':
      return jjMoveStringLiteralDfa3_0(active0, 393216L);
    case 't':
      if ((active0 & 0x100) != 0L) {
        this.jjmatchedKind = 8;
        this.jjmatchedPos = 2;
      }
      return jjMoveStringLiteralDfa3_0(active0, 2048L);
    case 'u':
      return jjMoveStringLiteralDfa3_0(active0, 65536L);
    }

    return jjMoveNfa_0(5, 2);
  }

  private final int jjMoveStringLiteralDfa3_0(long old0, long active0) {
    if ((active0 &= old0) == 0L)
      return jjMoveNfa_0(5, 2);
    try
    {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      return jjMoveNfa_0(5, 2);
    }
    switch (this.curChar) {
    case 'A':
      return jjMoveStringLiteralDfa4_0(active0, 8192L);
    case 'E':
      if ((active0 & 0x1000) != 0L) {
        this.jjmatchedKind = 12;
        this.jjmatchedPos = 3;
      } else {
        if ((active0 & 0x10000) == 0L) break;
        this.jjmatchedKind = 16;
        this.jjmatchedPos = 3; } break;
    case 'L':
      if ((active0 & 0x40000) == 0L) break;
      this.jjmatchedKind = 18;
      this.jjmatchedPos = 3; break;
    case 'S':
      return jjMoveStringLiteralDfa4_0(active0, 131072L);
    case 'W':
      return jjMoveStringLiteralDfa4_0(active0, 2048L);
    case 'a':
      return jjMoveStringLiteralDfa4_0(active0, 8192L);
    case 'e':
      if ((active0 & 0x1000) != 0L) {
        this.jjmatchedKind = 12;
        this.jjmatchedPos = 3;
      } else {
        if ((active0 & 0x10000) == 0L) break;
        this.jjmatchedKind = 16;
        this.jjmatchedPos = 3; } break;
    case 'l':
      if ((active0 & 0x40000) == 0L) break;
      this.jjmatchedKind = 18;
      this.jjmatchedPos = 3; break;
    case 's':
      return jjMoveStringLiteralDfa4_0(active0, 131072L);
    case 'w':
      return jjMoveStringLiteralDfa4_0(active0, 2048L);
    }

    return jjMoveNfa_0(5, 3);
  }

  private final int jjMoveStringLiteralDfa4_0(long old0, long active0) {
    if ((active0 &= old0) == 0L)
      return jjMoveNfa_0(5, 3);
    try
    {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      return jjMoveNfa_0(5, 3);
    }
    switch (this.curChar) {
    case 'E':
      if ((active0 & 0x20000) != 0L) {
        this.jjmatchedKind = 17;
        this.jjmatchedPos = 4;
      }
      return jjMoveStringLiteralDfa5_0(active0, 2048L);
    case 'P':
      return jjMoveStringLiteralDfa5_0(active0, 8192L);
    case 'e':
      if ((active0 & 0x20000) != 0L) {
        this.jjmatchedKind = 17;
        this.jjmatchedPos = 4;
      }
      return jjMoveStringLiteralDfa5_0(active0, 2048L);
    case 'p':
      return jjMoveStringLiteralDfa5_0(active0, 8192L);
    }

    return jjMoveNfa_0(5, 4);
  }

  private final int jjMoveStringLiteralDfa5_0(long old0, long active0) {
    if ((active0 &= old0) == 0L)
      return jjMoveNfa_0(5, 4);
    try
    {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      return jjMoveNfa_0(5, 4);
    }
    switch (this.curChar) {
    case 'E':
      if ((active0 & 0x2000) != 0L) {
        this.jjmatchedKind = 13;
        this.jjmatchedPos = 5;
      }
      return jjMoveStringLiteralDfa6_0(active0, 2048L);
    case 'e':
      if ((active0 & 0x2000) != 0L) {
        this.jjmatchedKind = 13;
        this.jjmatchedPos = 5;
      }
      return jjMoveStringLiteralDfa6_0(active0, 2048L);
    }

    return jjMoveNfa_0(5, 5);
  }

  private final int jjMoveStringLiteralDfa6_0(long old0, long active0) {
    if ((active0 &= old0) == 0L)
      return jjMoveNfa_0(5, 5);
    try
    {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      return jjMoveNfa_0(5, 5);
    }
    switch (this.curChar) {
    case 'N':
      if ((active0 & 0x800) == 0L) break;
      this.jjmatchedKind = 11;
      this.jjmatchedPos = 6; break;
    case 'n':
      if ((active0 & 0x800) == 0L) break;
      this.jjmatchedKind = 11;
      this.jjmatchedPos = 6; break;
    }

    return jjMoveNfa_0(5, 6);
  }

  private final void jjCheckNAdd(int state) {
    if (this.jjrounds[state] != this.jjround) {
      this.jjstateSet[(this.jjnewStateCnt++)] = state;
      this.jjrounds[state] = this.jjround;
    }
  }

  private final void jjAddStates(int start, int end) {
    do {
      this.jjstateSet[(this.jjnewStateCnt++)] = jjnextStates[start];
    }
    while (start++ != end);
  }

  private final void jjCheckNAddTwoStates(int state1, int state2) {
    jjCheckNAdd(state1);
    jjCheckNAdd(state2);
  }

  private final void jjCheckNAddStates(int start, int end) {
    do {
      jjCheckNAdd(jjnextStates[start]);
    }
    while (start++ != end);
  }

  private final void jjCheckNAddStates(int start) {
    jjCheckNAdd(jjnextStates[start]);
    jjCheckNAdd(jjnextStates[(start + 1)]);
  }

  private final int jjMoveNfa_0(int startState, int curPos)
  {
    int strKind = this.jjmatchedKind;
    int strPos = this.jjmatchedPos;
    int seenUpto;
    this.input_stream.backup(seenUpto = curPos + 1);
    try {
      this.curChar = this.input_stream.readChar();
    }
    catch (IOException e) {
      throw new Error("Internal Error");
    }
    curPos = 0;

    int startsAt = 0;
    this.jjnewStateCnt = 37;
    int i = 1;
    this.jjstateSet[0] = startState;
    int kind = 2147483647;
    while (true) {
      if (++this.jjround == 2147483647) {
        ReInitRounds();
      }
      if (this.curChar < '@') {
        long l = 1L << this.curChar;
        do {
          i--; switch (this.jjstateSet[i]) {
          case 5:
            if ((0x0 & l) != 0L) {
              if (kind > 19) {
                kind = 19;
              }
              jjCheckNAddStates(0, 4);
            }
            else if (this.curChar == '$') {
              if (kind > 23) {
                kind = 23;
              }
              jjCheckNAdd(24);
            }
            else if (this.curChar == '\'') {
              jjCheckNAddStates(5, 7);
            }
            else if (this.curChar == '.') {
              jjCheckNAdd(14);
            }
            else if (this.curChar == '/') {
              this.jjstateSet[(this.jjnewStateCnt++)] = 6;
            } else {
              if (this.curChar != '-') continue;
              this.jjstateSet[(this.jjnewStateCnt++)] = 0; } break;
          case 0:
            if (this.curChar != '-') continue;
            jjCheckNAddStates(8, 10); break;
          case 1:
            if ((0xFFFFDBFF & l) == 0L) continue;
            jjCheckNAddStates(8, 10); break;
          case 2:
            if (((0x2400 & l) == 0L) || (kind <= 6)) continue;
            kind = 6; break;
          case 3:
            if ((this.curChar != '\n') || (kind <= 6)) continue;
            kind = 6; break;
          case 4:
            if (this.curChar != '\r') continue;
            this.jjstateSet[(this.jjnewStateCnt++)] = 3; break;
          case 6:
            if (this.curChar != '*') continue;
            jjCheckNAddTwoStates(7, 8); break;
          case 7:
            if ((0xFFFFFFFF & l) == 0L) continue;
            jjCheckNAddTwoStates(7, 8); break;
          case 8:
            if (this.curChar != '*') continue;
            jjCheckNAddStates(11, 13); break;
          case 9:
            if ((0xFFFFFFFF & l) == 0L) continue;
            jjCheckNAddTwoStates(10, 8); break;
          case 10:
            if ((0xFFFFFFFF & l) == 0L) continue;
            jjCheckNAddTwoStates(10, 8); break;
          case 11:
            if ((this.curChar != '/') || (kind <= 7)) continue;
            kind = 7; break;
          case 12:
            if (this.curChar != '/') continue;
            this.jjstateSet[(this.jjnewStateCnt++)] = 6; break;
          case 13:
            if (this.curChar != '.') continue;
            jjCheckNAdd(14); break;
          case 14:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 20) {
              kind = 20;
            }
            jjCheckNAddTwoStates(14, 15);
            break;
          case 16:
            if ((0x0 & l) == 0L) continue;
            jjCheckNAdd(17); break;
          case 17:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 20) {
              kind = 20;
            }
            jjCheckNAdd(17);
            break;
          case 18:
          case 19:
            if (this.curChar != '\'') continue;
            jjCheckNAddStates(5, 7); break;
          case 20:
            if (this.curChar != '\'') continue;
            this.jjstateSet[(this.jjnewStateCnt++)] = 19; break;
          case 21:
            if ((0xFFFFFFFF & l) == 0L) continue;
            jjCheckNAddStates(5, 7); break;
          case 22:
            if ((this.curChar != '\'') || (kind <= 22)) continue;
            kind = 22; break;
          case 23:
            if (this.curChar != '$') {
              continue;
            }
            if (kind > 23) {
              kind = 23;
            }
            jjCheckNAdd(24);
            break;
          case 24:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 23) {
              kind = 23;
            }
            jjCheckNAdd(24);
            break;
          case 25:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 19) {
              kind = 19;
            }
            jjCheckNAddStates(0, 4);
            break;
          case 26:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 19) {
              kind = 19;
            }
            jjCheckNAdd(26);
            break;
          case 27:
            if ((0x0 & l) == 0L) continue;
            jjCheckNAddTwoStates(27, 28); break;
          case 28:
            if (this.curChar != '.') {
              continue;
            }
            if (kind > 20) {
              kind = 20;
            }
            jjCheckNAddTwoStates(29, 30);
            break;
          case 29:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 20) {
              kind = 20;
            }
            jjCheckNAddTwoStates(29, 30);
            break;
          case 31:
            if ((0x0 & l) == 0L) continue;
            jjCheckNAdd(32); break;
          case 32:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 20) {
              kind = 20;
            }
            jjCheckNAdd(32);
            break;
          case 33:
            if ((0x0 & l) == 0L) continue;
            jjCheckNAddTwoStates(33, 34); break;
          case 35:
            if ((0x0 & l) == 0L) continue;
            jjCheckNAdd(36); break;
          case 36:
            if ((0x0 & l) == 0L) {
              continue;
            }
            if (kind > 20) {
              kind = 20;
            }
            jjCheckNAdd(36);
          case 15:
          case 30:
          case 34:
          }
        }
        while (i != startsAt);
      }
      else if (this.curChar < '') {
        long l = 1L << (this.curChar & 0x3F);
        do {
          i--; switch (this.jjstateSet[i]) {
          case 5:
          case 24:
            if ((0x87FFFFFE & l) == 0L) {
              continue;
            }
            if (kind > 23) {
              kind = 23;
            }
            jjCheckNAdd(24);
            break;
          case 1:
            jjAddStates(8, 10);
            break;
          case 7:
            jjCheckNAddTwoStates(7, 8);
            break;
          case 9:
          case 10:
            jjCheckNAddTwoStates(10, 8);
            break;
          case 15:
            if ((0x20 & l) == 0L) continue;
            jjAddStates(14, 15); break;
          case 21:
            jjAddStates(5, 7);
            break;
          case 30:
            if ((0x20 & l) == 0L) continue;
            jjAddStates(16, 17); break;
          case 34:
            if ((0x20 & l) == 0L) continue;
            jjAddStates(18, 19);
          case 2:
          case 3:
          case 4:
          case 6:
          case 8:
          case 11:
          case 12:
          case 13:
          case 14:
          case 16:
          case 17:
          case 18:
          case 19:
          case 20:
          case 22:
          case 23:
          case 25:
          case 26:
          case 27:
          case 28:
          case 29:
          case 31:
          case 32:
          case 33: }  } while (i != startsAt);
      }
      else {
        int hiByte = this.curChar >> '\b';
        int i1 = hiByte >> 6;
        long l1 = 1L << (hiByte & 0x3F);
        int i2 = (this.curChar & 0xFF) >> '\006';
        long l2 = 1L << (this.curChar & 0x3F);
        do {
          i--; switch (this.jjstateSet[i]) {
          case 1:
            if (!jjCanMove_0(hiByte, i1, i2, l1, l2)) continue;
            jjAddStates(8, 10); break;
          case 7:
            if (!jjCanMove_0(hiByte, i1, i2, l1, l2)) continue;
            jjCheckNAddTwoStates(7, 8); break;
          case 9:
          case 10:
            if (!jjCanMove_0(hiByte, i1, i2, l1, l2)) continue;
            jjCheckNAddTwoStates(10, 8); break;
          case 21:
            if (!jjCanMove_0(hiByte, i1, i2, l1, l2)) continue;
            jjAddStates(5, 7);
          }

        }

        while (i != startsAt);
      }
      if (kind != 2147483647) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = curPos;
        kind = 2147483647;
      }
      curPos++;
      if ((i = this.jjnewStateCnt) != (startsAt = 37 - (this.jjnewStateCnt = startsAt)))
      {
        try
        {
          this.curChar = this.input_stream.readChar();
        }
        catch (IOException e) {
        }
      }
    }
//    if (this.jjmatchedPos > strPos) {
//      return curPos;
//    }
//
//    int toRet = Math.max(curPos, seenUpto);
//
//    if (curPos < toRet) {
//      for (i = toRet - Math.min(curPos, seenUpto); i-- > 0; ) {
//        try {
//          this.curChar = this.input_stream.readChar();
//        }
//        catch (IOException e) {
//          throw new Error("Internal Error : Please send a bug report.");
//        }
//      }
//    }
//
//    if (this.jjmatchedPos < strPos) {
//      this.jjmatchedKind = strKind;
//      this.jjmatchedPos = strPos;
//    }
//    else if ((this.jjmatchedPos == strPos) && (this.jjmatchedKind > strKind)) {
//      this.jjmatchedKind = strKind;
//    }
//
//    return toRet;
  }

  private static final boolean jjCanMove_0(int hiByte, int i1, int i2, long l1, long l2)
  {
    switch (hiByte) {
    case 0:
      return (jjbitVec2[i2] & l2) != 0L;
    }

    return (jjbitVec0[i1] & l1) != 0L;
  }

  public SelectorParserTokenManager(SimpleCharStream stream)
  {
    this.input_stream = stream;
  }

  public SelectorParserTokenManager(SimpleCharStream stream, int lexState) {
    this(stream);
    SwitchTo(lexState);
  }

  public void ReInit(SimpleCharStream stream) {
    this.jjmatchedPos = (this.jjnewStateCnt = 0);
    this.curLexState = this.defaultLexState;
    this.input_stream = stream;
    ReInitRounds();
  }

  private final void ReInitRounds()
  {
    this.jjround = -2147483647;
    for (int i = 37; i-- > 0; )
      this.jjrounds[i] = -2147483648;
  }

  public void ReInit(SimpleCharStream stream, int lexState)
  {
    ReInit(stream);
    SwitchTo(lexState);
  }

  public void SwitchTo(int lexState) {
    if ((lexState >= 1) || (lexState < 0)) {
      throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", 2);
    }

    this.curLexState = lexState;
  }

  private final Token jjFillToken()
  {
    Token t = Token.newToken(this.jjmatchedKind);
    t.kind = this.jjmatchedKind;
    String im = jjstrLiteralImages[this.jjmatchedKind];
    t.image = (im == null ? this.input_stream.GetImage() : im);
    t.beginLine = this.input_stream.getBeginLine();
    t.beginColumn = this.input_stream.getBeginColumn();
    t.endLine = this.input_stream.getEndLine();
    t.endColumn = this.input_stream.getEndColumn();
    return t;
  }

  public final Token getNextToken()
  {
    Token specialToken = null;

    int curPos = 0;
    while (true)
    {
      try
      {
        this.curChar = this.input_stream.BeginToken();
      }
      catch (IOException e) {
        this.jjmatchedKind = 0;
        Token matchedToken = jjFillToken();
        matchedToken.specialToken = specialToken;
        return matchedToken;
      }

      this.jjmatchedKind = 2147483647;
      this.jjmatchedPos = 0;
      curPos = jjMoveStringLiteralDfa0_0();
      if (this.jjmatchedKind == 2147483647) break;
      if (this.jjmatchedPos + 1 < curPos) {
        this.input_stream.backup(curPos - this.jjmatchedPos - 1);
      }
      if ((jjtoToken[(this.jjmatchedKind >> 6)] & 1L << (this.jjmatchedKind & 0x3F)) != 0L) {
        Token matchedToken = jjFillToken();
        matchedToken.specialToken = specialToken;
        return matchedToken;
      }

      if ((jjtoSpecial[(this.jjmatchedKind >> 6)] & 1L << (this.jjmatchedKind & 0x3F)) != 0L) {
        Token matchedToken = jjFillToken();
        if (specialToken == null) {
          specialToken = matchedToken; continue;
        }

        matchedToken.specialToken = specialToken;
        specialToken = specialToken.next = matchedToken;
      }

    }

    int error_line = this.input_stream.getEndLine();
    int error_column = this.input_stream.getEndColumn();
    String error_after = null;
    boolean EOFSeen = false;
    try {
      this.input_stream.readChar();
      this.input_stream.backup(1);
    }
    catch (IOException e1) {
      EOFSeen = true;
      error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
      if ((this.curChar == '\n') || (this.curChar == '\r')) {
        error_line++;
        error_column = 0;
      }
      else {
        error_column++;
      }
    }
    if (!EOFSeen) {
      this.input_stream.backup(1);
      error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
    }
    throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
  }
}