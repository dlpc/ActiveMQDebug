package org.codehaus.activemq.store;

import javax.transaction.xa.XAException;
import org.codehaus.activemq.message.ActiveMQXid;
import org.codehaus.activemq.service.Service;
import org.codehaus.activemq.service.Transaction;
import org.codehaus.activemq.service.TransactionManager;

public abstract interface PreparedTransactionStore extends Service
{
  public abstract ActiveMQXid[] getXids()
    throws XAException;

  public abstract void remove(ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void put(ActiveMQXid paramActiveMQXid, Transaction paramTransaction)
    throws XAException;

  public abstract void loadPreparedTransactions(TransactionManager paramTransactionManager)
    throws XAException;
}