package org.codehaus.activemq.store.jdbc.adapter;

import org.codehaus.activemq.store.jdbc.StatementProvider;

public class DefaultStatementProvider
  implements StatementProvider
{
  protected String tablePrefix = "";
  protected String messageTableName = "ACTIVEMQ_MSGS";
  protected String txTableName = "ACTIVEMQ_TXS";
  protected String durableSubAcksTableName = "ACTIVEMQ_ACKS";

  protected String binaryDataType = "BLOB";
  protected String containerNameDataType = "VARCHAR(250)";
  protected String xidDataType = "VARCHAR(250)";
  protected String msgIdDataType = "VARCHAR(250)";
  protected String subscriptionIdDataType = "VARCHAR(250)";
  protected String sequenceDataType = "INTEGER";
  protected String stringIdDataType = "VARCHAR(250)";
  private int subscriberID;
  private String clientID;
  private String consumerName;
  private String destination;
  private String selector;

  public String[] getCreateSchemaStatments()
  {
    return new String[] { "CREATE TABLE " + this.tablePrefix + this.messageTableName + "(" + "ID " + this.sequenceDataType + ", CONTAINER " + this.containerNameDataType + ", MSGID " + this.msgIdDataType + ", MSG " + this.binaryDataType + ")", "CREATE TABLE " + this.tablePrefix + this.txTableName + "(" + "XID " + this.xidDataType + ", TX " + this.binaryDataType + ")", "CREATE TABLE " + this.tablePrefix + this.durableSubAcksTableName + "(" + "SUB " + this.subscriptionIdDataType + ", CONTAINER " + this.containerNameDataType + ", LAST_ACKED_ID " + this.sequenceDataType + ", SE_ID INTEGER" + ", SE_CLIENT_ID " + this.stringIdDataType + ", SE_CONSUMER_NAME " + this.stringIdDataType + ", SE_SELECTOR " + this.stringIdDataType + ")" };
  }

  public String[] getDropSchemaStatments()
  {
    return new String[] { "DROP TABLE " + this.tablePrefix + this.durableSubAcksTableName + "", "DROP TABLE " + this.tablePrefix + this.messageTableName + "", "DROP TABLE " + this.tablePrefix + this.txTableName + "" };
  }

  public String getAddMessageStatment()
  {
    return "INSERT INTO " + this.tablePrefix + this.messageTableName + "(ID, CONTAINER, MSGID, MSG) VALUES (?, ?, ?, ?)";
  }
  public String getUpdateMessageStatment() {
    return "UPDATE " + this.tablePrefix + this.messageTableName + " SET MSG=? WHERE ID=?";
  }
  public String getRemoveMessageStatment() {
    return "DELETE FROM " + this.tablePrefix + this.messageTableName + " WHERE ID=?";
  }
  public String getFindMessageStatment() {
    return "SELECT MSG FROM " + this.tablePrefix + this.messageTableName + " WHERE ID=?";
  }
  public String getFindAllMessagesStatment() {
    return "SELECT ID, MSGID FROM " + this.tablePrefix + this.messageTableName + " WHERE CONTAINER=? ORDER BY ID";
  }
  public String getFindLastSequenceId() {
    return "SELECT MAX(ID) FROM " + this.tablePrefix + this.messageTableName + "";
  }

  public String getAddXidStatment() {
    return "INSERT INTO " + this.tablePrefix + this.txTableName + "(XID, TX) VALUES (?, ?)";
  }
  public String getUpdateXidStatment() {
    return "UPDATE " + this.tablePrefix + this.txTableName + " SET TX=? WHERE XID=?";
  }
  public String getRemoveXidStatment() {
    return "DELETE FROM " + this.tablePrefix + this.txTableName + " WHERE XID=?";
  }
  public String getFindXidStatment() {
    return "SELECT TX FROM " + this.tablePrefix + this.txTableName + " WHERE XID=?";
  }
  public String getFindAllXidStatment() {
    return "SELECT XID FROM " + this.tablePrefix + this.txTableName + "";
  }
  public String getFindAllTxStatment() {
    return "SELECT XID, TX FROM " + this.tablePrefix + this.txTableName + "";
  }

  public String getCreateDurableSubStatment() {
    return "INSERT INTO " + this.tablePrefix + this.durableSubAcksTableName + "(SE_ID, SE_CLIENT_ID, SE_CONSUMER_NAME, SE_SELECTOR, SUB, CONTAINER) VALUES (?, ?, ?, ?, ?, ?)";
  }

  public String getUpdateDurableSubStatment()
  {
    return "UPDATE " + this.tablePrefix + this.durableSubAcksTableName + " SET SE_ID=?, SE_CLIENT_ID=?, SE_CONSUMER_NAME=?, SE_SELECTOR? WHERE SUB=? AND CONTAINER=?";
  }

  public String getFindDurableSubStatment()
  {
    return "SELECT SE_ID, SE_CLIENT_ID, SE_CONSUMER_NAME, SE_SELECTOR, CONTAINER=? " + this.tablePrefix + this.durableSubAcksTableName + " WHERE SUB=? AND CONTAINER=?";
  }

  public String getUpdateLastAckOfDurableSub()
  {
    return "UPDATE " + this.tablePrefix + this.durableSubAcksTableName + " SET LAST_ACKED_ID=? WHERE SUB=? AND CONTAINER=?";
  }

  public String getFindAllDurableSubMessagesStatment()
  {
    return "SELECT M.ID, M.MSGID FROM " + this.tablePrefix + this.messageTableName + " M, " + this.tablePrefix + this.durableSubAcksTableName + " D " + " WHERE D.CONTAINER=? AND D.SUB=? " + " AND M.CONTAINER=D.CONTAINER AND M.ID > D.LAST_ACKED_ID" + " ORDER BY M.ID";
  }

  public String getContainerNameDataType()
  {
    return this.containerNameDataType;
  }

  public void setContainerNameDataType(String containerNameDataType)
  {
    this.containerNameDataType = containerNameDataType;
  }

  public String getBinaryDataType()
  {
    return this.binaryDataType;
  }

  public void setBinaryDataType(String messageDataType)
  {
    this.binaryDataType = messageDataType;
  }

  public String getMessageTableName()
  {
    return this.messageTableName;
  }

  public void setMessageTableName(String messageTableName)
  {
    this.messageTableName = messageTableName;
  }

  public String getMsgIdDataType()
  {
    return this.msgIdDataType;
  }

  public void setMsgIdDataType(String msgIdDataType)
  {
    this.msgIdDataType = msgIdDataType;
  }

  public String getSequenceDataType()
  {
    return this.sequenceDataType;
  }

  public void setSequenceDataType(String sequenceDataType)
  {
    this.sequenceDataType = sequenceDataType;
  }

  public String getTablePrefix()
  {
    return this.tablePrefix;
  }

  public void setTablePrefix(String tablePrefix)
  {
    this.tablePrefix = tablePrefix;
  }

  public String getTxTableName()
  {
    return this.txTableName;
  }

  public void setTxTableName(String txTableName)
  {
    this.txTableName = txTableName;
  }

  public String getXidDataType()
  {
    return this.xidDataType;
  }

  public void setXidDataType(String xidDataType)
  {
    this.xidDataType = xidDataType;
  }

  public String getDurableSubAcksTableName()
  {
    return this.durableSubAcksTableName;
  }

  public void setDurableSubAcksTableName(String durableSubAcksTableName)
  {
    this.durableSubAcksTableName = durableSubAcksTableName;
  }

  public String getSubscriptionIdDataType()
  {
    return this.subscriptionIdDataType;
  }

  public void setSubscriptionIdDataType(String subscriptionIdDataType)
  {
    this.subscriptionIdDataType = subscriptionIdDataType;
  }
}