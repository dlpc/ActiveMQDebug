package org.codehaus.activemq.store.jdbc.adapter;

import org.codehaus.activemq.store.jdbc.StatementProvider;

public class CachingStatementProvider
  implements StatementProvider
{
  private final StatementProvider statementProvider;
  private String addMessageStatment;
  private String addXidStatment;
  private String[] createSchemaStatments;
  private String[] dropSchemaStatments;
  private String findAllMessagesStatment;
  private String findAllTxStatment;
  private String findAllXidStatment;
  private String findLastSequenceId;
  private String findMessageStatment;
  private String findXidStatment;
  private String removeMessageStatment;
  private String removeXidStatment;
  private String updateMessageStatment;
  private String updateXidStatment;
  private String createDurableSubStatment;
  private String updateDurableSubStatment;
  private String findDurableSubStatment;
  private String findAllDurableSubMessagesStatment;
  private String updateLastAckOfDurableSub;

  public CachingStatementProvider(StatementProvider statementProvider)
  {
    this.statementProvider = statementProvider;
  }

  public StatementProvider getNext() {
    return this.statementProvider;
  }

  public String getAddMessageStatment()
  {
    if (this.addMessageStatment == null) {
      this.addMessageStatment = this.statementProvider.getAddMessageStatment();
    }
    return this.addMessageStatment;
  }

  public String getAddXidStatment()
  {
    if (this.addXidStatment == null) {
      this.addXidStatment = this.statementProvider.getAddXidStatment();
    }
    return this.addXidStatment;
  }

  public String[] getCreateSchemaStatments()
  {
    if (this.createSchemaStatments == null) {
      this.createSchemaStatments = this.statementProvider.getCreateSchemaStatments();
    }
    return this.createSchemaStatments;
  }

  public String[] getDropSchemaStatments()
  {
    if (this.dropSchemaStatments == null) {
      this.dropSchemaStatments = this.statementProvider.getDropSchemaStatments();
    }
    return this.dropSchemaStatments;
  }

  public String getFindAllMessagesStatment()
  {
    if (this.findAllMessagesStatment == null) {
      this.findAllMessagesStatment = this.statementProvider.getFindAllMessagesStatment();
    }
    return this.findAllMessagesStatment;
  }

  public String getFindAllTxStatment()
  {
    if (this.findAllTxStatment == null) {
      this.findAllTxStatment = this.statementProvider.getFindAllTxStatment();
    }
    return this.findAllTxStatment;
  }

  public String getFindAllXidStatment()
  {
    if (this.findAllXidStatment == null) {
      this.findAllXidStatment = this.statementProvider.getFindAllXidStatment();
    }
    return this.findAllXidStatment;
  }

  public String getFindLastSequenceId()
  {
    if (this.findLastSequenceId == null) {
      this.findLastSequenceId = this.statementProvider.getFindLastSequenceId();
    }
    return this.findLastSequenceId;
  }

  public String getFindMessageStatment()
  {
    if (this.findMessageStatment == null) {
      this.findMessageStatment = this.statementProvider.getFindMessageStatment();
    }
    return this.findMessageStatment;
  }

  public String getFindXidStatment()
  {
    if (this.findXidStatment == null) {
      this.findXidStatment = this.statementProvider.getFindXidStatment();
    }
    return this.findXidStatment;
  }

  public String getRemoveMessageStatment()
  {
    if (this.removeMessageStatment == null) {
      this.removeMessageStatment = this.statementProvider.getRemoveMessageStatment();
    }
    return this.removeMessageStatment;
  }

  public String getRemoveXidStatment()
  {
    if (this.removeXidStatment == null) {
      this.removeXidStatment = this.statementProvider.getRemoveXidStatment();
    }
    return this.removeXidStatment;
  }

  public String getUpdateMessageStatment()
  {
    if (this.updateMessageStatment == null) {
      this.updateMessageStatment = this.statementProvider.getUpdateMessageStatment();
    }
    return this.updateMessageStatment;
  }

  public String getUpdateXidStatment()
  {
    if (this.updateXidStatment == null) {
      this.updateXidStatment = this.statementProvider.getUpdateXidStatment();
    }
    return this.updateXidStatment;
  }

  public String getCreateDurableSubStatment()
  {
    if (this.createDurableSubStatment == null) {
      this.createDurableSubStatment = this.statementProvider.getCreateDurableSubStatment();
    }
    return this.createDurableSubStatment;
  }

  public String getUpdateDurableSubStatment()
  {
    if (this.updateDurableSubStatment == null) {
      this.updateDurableSubStatment = this.statementProvider.getUpdateDurableSubStatment();
    }
    return this.updateDurableSubStatment;
  }

  public String getFindDurableSubStatment()
  {
    if (this.findDurableSubStatment == null) {
      this.findDurableSubStatment = this.statementProvider.getFindDurableSubStatment();
    }
    return this.findDurableSubStatment;
  }

  public String getFindAllDurableSubMessagesStatment()
  {
    if (this.findAllDurableSubMessagesStatment == null) {
      this.findAllDurableSubMessagesStatment = this.statementProvider.getFindAllDurableSubMessagesStatment();
    }
    return this.findAllDurableSubMessagesStatment;
  }

  public String getUpdateLastAckOfDurableSub()
  {
    if (this.updateLastAckOfDurableSub == null) {
      this.updateLastAckOfDurableSub = this.statementProvider.getUpdateLastAckOfDurableSub();
    }
    return this.updateLastAckOfDurableSub;
  }
}