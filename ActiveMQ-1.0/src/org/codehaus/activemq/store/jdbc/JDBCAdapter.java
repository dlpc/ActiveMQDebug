package org.codehaus.activemq.store.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.jms.JMSException;
import javax.transaction.xa.XAException;
import org.codehaus.activemq.message.ActiveMQXid;
import org.codehaus.activemq.service.SubscriberEntry;
import org.codehaus.activemq.service.TransactionManager;

public abstract interface JDBCAdapter
{
  public abstract SequenceGenerator getSequenceGenerator();

  public abstract void doCreateTables(Connection paramConnection)
    throws SQLException;

  public abstract void initSequenceGenerator(Connection paramConnection);

  public abstract void doAddMessage(Connection paramConnection, long paramLong, String paramString1, String paramString2, byte[] paramArrayOfByte)
    throws SQLException, JMSException;

  public abstract byte[] doGetMessage(Connection paramConnection, long paramLong)
    throws SQLException;

  public abstract void doRemoveMessage(Connection paramConnection, long paramLong)
    throws SQLException;

  public abstract void doRecover(Connection paramConnection, String paramString, MessageListResultHandler paramMessageListResultHandler)
    throws SQLException, JMSException;

  public abstract void doGetXids(Connection paramConnection, List paramList)
    throws SQLException;

  public abstract void doRemoveXid(Connection paramConnection, ActiveMQXid paramActiveMQXid)
    throws SQLException, XAException;

  public abstract void doAddXid(Connection paramConnection, ActiveMQXid paramActiveMQXid, byte[] paramArrayOfByte)
    throws SQLException, XAException;

  public abstract void doLoadPreparedTransactions(Connection paramConnection, TransactionManager paramTransactionManager)
    throws SQLException;

  public abstract void doSetLastAck(Connection paramConnection, String paramString1, String paramString2, long paramLong)
    throws SQLException, JMSException;

  public abstract void doRecoverSubscription(Connection paramConnection, String paramString1, String paramString2, MessageListResultHandler paramMessageListResultHandler)
    throws SQLException, JMSException;

  public abstract void doSetSubscriberEntry(Connection paramConnection, String paramString1, String paramString2, SubscriberEntry paramSubscriberEntry)
    throws SQLException, JMSException;

  public abstract SubscriberEntry doGetSubscriberEntry(Connection paramConnection, String paramString1, String paramString2)
    throws SQLException, JMSException;

  public static abstract interface MessageListResultHandler
  {
    public abstract void onMessage(long paramLong, String paramString)
      throws JMSException;
  }
}