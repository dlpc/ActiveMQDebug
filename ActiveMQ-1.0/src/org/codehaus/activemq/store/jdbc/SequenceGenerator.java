package org.codehaus.activemq.store.jdbc;

public class SequenceGenerator
{
  long lastSequenceId = 0L;

  public synchronized void setLastSequenceId(long value) {
    this.lastSequenceId = value;
  }

  public synchronized long getLastSequenceId() {
    return this.lastSequenceId;
  }

  public synchronized long getNextSequenceId() {
    return this.lastSequenceId++;
  }
}