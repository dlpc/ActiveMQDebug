package org.codehaus.activemq.store.jdbc;

public abstract interface StatementProvider
{
  public abstract String[] getCreateSchemaStatments();

  public abstract String[] getDropSchemaStatments();

  public abstract String getAddMessageStatment();

  public abstract String getUpdateMessageStatment();

  public abstract String getRemoveMessageStatment();

  public abstract String getFindMessageStatment();

  public abstract String getFindAllMessagesStatment();

  public abstract String getFindLastSequenceId();

  public abstract String getAddXidStatment();

  public abstract String getUpdateXidStatment();

  public abstract String getRemoveXidStatment();

  public abstract String getFindXidStatment();

  public abstract String getFindAllXidStatment();

  public abstract String getFindAllTxStatment();

  public abstract String getCreateDurableSubStatment();

  public abstract String getUpdateDurableSubStatment();

  public abstract String getFindDurableSubStatment();

  public abstract String getUpdateLastAckOfDurableSub();

  public abstract String getFindAllDurableSubMessagesStatment();
}