package org.codehaus.activemq.store.vm;

import java.io.File;
import javax.jms.JMSException;
import org.codehaus.activemq.service.impl.PersistenceAdapterSupport;
import org.codehaus.activemq.store.MessageStore;
import org.codehaus.activemq.store.PreparedTransactionStore;
import org.codehaus.activemq.store.TopicMessageStore;

public class VMPersistenceAdapter extends PersistenceAdapterSupport
{
  public static VMPersistenceAdapter newInstance(File file)
  {
    return new VMPersistenceAdapter();
  }

  public MessageStore createQueueMessageStore(String destinationName) throws JMSException {
    return new VMMessageStore();
  }

  public TopicMessageStore createTopicMessageStore(String destinationName) throws JMSException {
    return new VMTopicMessageStore();
  }

  public PreparedTransactionStore createPreparedTransactionStore() throws JMSException {
    return new VMPreparedTransactionStoreImpl();
  }

  public void beginTransaction()
  {
  }

  public void commitTransaction()
  {
  }

  public void rollbackTransaction()
  {
  }

  public void start()
    throws JMSException
  {
  }

  public void stop()
    throws JMSException
  {
  }
}