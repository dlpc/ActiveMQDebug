package org.codehaus.activemq.store.vm;

import EDU.oswego.cs.dl.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.Map;
import javax.jms.JMSException;
import org.codehaus.activemq.message.ActiveMQXid;
import org.codehaus.activemq.service.Transaction;
import org.codehaus.activemq.service.TransactionManager;
import org.codehaus.activemq.store.PreparedTransactionStore;

public class VMPreparedTransactionStoreImpl
  implements PreparedTransactionStore
{
  private Map prepared = new ConcurrentHashMap();

  public ActiveMQXid[] getXids() {
    ArrayList list = new ArrayList(this.prepared.keySet());
    ActiveMQXid[] answer = new ActiveMQXid[list.size()];
    list.toArray(answer);
    return answer;
  }

  public void remove(ActiveMQXid xid) {
    this.prepared.remove(xid);
  }

  public void put(ActiveMQXid xid, Transaction transaction) {
    this.prepared.put(xid, transaction);
  }

  public void loadPreparedTransactions(TransactionManager transactionManager)
  {
  }

  public void start()
    throws JMSException
  {
  }

  public void stop()
    throws JMSException
  {
  }
}