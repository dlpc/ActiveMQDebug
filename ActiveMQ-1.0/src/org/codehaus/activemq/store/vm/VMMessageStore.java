package org.codehaus.activemq.store.vm;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.MessageIdentity;
import org.codehaus.activemq.service.QueueMessageContainer;
import org.codehaus.activemq.store.MessageStore;

public class VMMessageStore
  implements MessageStore
{
  private static final Log log = LogFactory.getLog(VMMessageStore.class);
  protected Map messageTable;

  public VMMessageStore()
  {
    this(new LinkedHashMap());
  }

  public VMMessageStore(LinkedHashMap messageTable) {
    this.messageTable = Collections.synchronizedMap(messageTable);
  }

  public MessageIdentity addMessage(ActiveMQMessage message) throws JMSException {
    this.messageTable.put(message.getJMSMessageID(), message);
    return message.getJMSMessageIdentity();
  }

  public ActiveMQMessage getMessage(MessageIdentity identity) throws JMSException {
    String messageID = identity.getMessageID();
    return (ActiveMQMessage)this.messageTable.get(messageID);
  }

  public void removeMessage(MessageIdentity identity, MessageAck ack) throws JMSException {
    String messageID = identity.getMessageID();
    this.messageTable.remove(messageID);
  }

  public void recover(QueueMessageContainer container) throws JMSException {
  }

  public void start() throws JMSException {
  }

  public void stop() throws JMSException {
    this.messageTable.clear();
  }
}