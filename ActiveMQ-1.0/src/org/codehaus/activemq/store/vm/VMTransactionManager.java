package org.codehaus.activemq.store.vm;

import org.codehaus.activemq.broker.Broker;
import org.codehaus.activemq.service.impl.TransactionManagerImpl;
import org.codehaus.activemq.store.PreparedTransactionStore;

public class VMTransactionManager extends TransactionManagerImpl
{
  public VMTransactionManager(Broker broker)
  {
    this(broker, new VMPreparedTransactionStoreImpl());
  }

  public VMTransactionManager(Broker broker, PreparedTransactionStore preparedTransactions) {
    super(broker, preparedTransactions);
  }
}