package org.codehaus.activemq.store;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.MessageContainer;
import org.codehaus.activemq.service.MessageIdentity;
import org.codehaus.activemq.service.SubscriberEntry;
import org.codehaus.activemq.service.Subscription;

public abstract interface TopicMessageStore extends MessageStore
{
  public abstract void setMessageContainer(MessageContainer paramMessageContainer);

  public abstract void incrementMessageCount(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void decrementMessageCountAndMaybeDelete(MessageIdentity paramMessageIdentity, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void setLastAcknowledgedMessageIdentity(Subscription paramSubscription, MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void recoverSubscription(Subscription paramSubscription, MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract MessageIdentity getLastestMessageIdentity()
    throws JMSException;

  public abstract SubscriberEntry getSubscriberEntry(ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void setSubscriberEntry(ConsumerInfo paramConsumerInfo, SubscriberEntry paramSubscriberEntry)
    throws JMSException;
}