package org.codehaus.activemq.store.bdb;

import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.SecondaryDatabase;
import com.sleepycat.je.SecondaryKeyCreator;
import java.io.PrintStream;

public class MockSecondaryKeyGenerator
  implements SecondaryKeyCreator
{
  long counter = 100L;

  public synchronized boolean createSecondaryKey(SecondaryDatabase secondaryDatabase, DatabaseEntry keyEntry, DatabaseEntry valueEntry, DatabaseEntry resultEntry) throws DatabaseException {
    long value = ++this.counter;
    System.out.println("Creating new counter key of value: " + value);
    resultEntry.setData(BDbHelper.asBytes(value));
    return true;
  }
}