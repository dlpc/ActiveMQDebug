package org.codehaus.activemq.store.howl;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.message.Packet;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.service.MessageIdentity;
import org.codehaus.activemq.service.QueueMessageContainer;
import org.codehaus.activemq.store.MessageStore;
import org.codehaus.activemq.util.Callback;
import org.codehaus.activemq.util.JMSExceptionHelper;
import org.codehaus.activemq.util.TransactionTemplate;
import org.objectweb.howl.log.LogConfigurationException;
import org.objectweb.howl.log.LogException;
import org.objectweb.howl.log.LogRecord;
import org.objectweb.howl.log.Logger;
import org.objectweb.howl.log.ReplayListener;

public class HowlMessageStore implements MessageStore {
	private static final int DEFAULT_RECORD_SIZE = 65536;
	private static final Log log = LogFactory.getLog(HowlMessageStore.class);
	private HowlPersistenceAdapter longTermPersistence;
	private MessageStore longTermStore;
	private Logger transactionLog;
	private WireFormat wireFormat;
	private TransactionTemplate transactionTemplate;
	private int maximumCacheSize = 100;
	private Map map = new LinkedHashMap();
	private boolean sync = true;
	private long lastLogMark;
	private Exception firstException;

	public HowlMessageStore(HowlPersistenceAdapter adapter, MessageStore checkpointStore, Logger transactionLog,
			WireFormat wireFormat) {
		this.longTermPersistence = adapter;
		this.longTermStore = checkpointStore;
		this.transactionLog = transactionLog;
		this.wireFormat = wireFormat;
		this.transactionTemplate = new TransactionTemplate(adapter);
	}

	public synchronized MessageIdentity addMessage(ActiveMQMessage message) throws JMSException {
		writePacket(message);

		if (!addMessageToCache(message)) {
			log.warn(
					"Not enough RAM to store the active transaction log and so we're having to forcea checkpoint so that we can ensure that reads are efficient and do not have to replay the transaction log");

			checkpoint(message);

			this.longTermStore.addMessage(message);
		}
		return message.getJMSMessageIdentity();
	}

	public ActiveMQMessage getMessage(MessageIdentity identity) throws JMSException {
		ActiveMQMessage answer = null;
		synchronized (this.map) {
			answer = (ActiveMQMessage) this.map.get(identity.getMessageID());
		}
		if (answer == null) {
			answer = this.longTermStore.getMessage(identity);
		}
		return answer;
	}

	public void removeMessage(MessageIdentity identity, MessageAck ack) throws JMSException {
		writePacket(ack);

		synchronized (this.map) {
			this.map.remove(identity.getMessageID());
		}
		this.longTermPersistence.onMessageRemove(this);
	}

	public synchronized void recover(QueueMessageContainer container) throws JMSException {
		this.longTermStore.recover(container);

		this.firstException = null;
		try {
			this.transactionLog.replay(new ReplayListener() {
				LogRecord record = new LogRecord(65536);
				private final QueueMessageContainer val$container=container;

				public void onRecord(LogRecord logRecord) {
					HowlMessageStore.this.readPacket(logRecord, this.val$container);
				}

				public void onError(LogException e) {
					HowlMessageStore.log.error("Error while recovering Howl transaction log: " + e, e);
				}

				public LogRecord getLogRecord() {
					return this.record;
				}
			});
		} catch (LogConfigurationException e) {
			throw createRecoveryFailedException(e);
		}
		if (this.firstException != null) {
			if ((this.firstException instanceof JMSException)) {
				throw ((JMSException) this.firstException);
			}

			throw createRecoveryFailedException(this.firstException);
		}
	}

	public synchronized void start() throws JMSException {
		this.longTermStore.start();
	}

	public synchronized void stop() throws JMSException {
		this.longTermStore.stop();
	}

	public synchronized void checkpoint() throws JMSException {
		checkpoint(null);
	}

	public int getMaximumCacheSize() {
		return this.maximumCacheSize;
	}

	public void setMaximumCacheSize(int maximumCacheSize) {
		this.maximumCacheSize = maximumCacheSize;
	}

	protected void checkpoint(ActiveMQMessage message) throws JMSException {
		ActiveMQMessage[] temp = null;
		synchronized (this.map) {
			temp = new ActiveMQMessage[this.map.size()];
			this.map.values().toArray(temp);

			this.map.clear();
		}

		ActiveMQMessage[] data = temp;
		this.transactionTemplate.run(new Callback() {
			private final ActiveMQMessage[] val$data=data;
			private final ActiveMQMessage val$message=message;

			public void execute() throws Throwable {
				int i = 0;
				for (int size = this.val$data.length; i < size; i++) {
					HowlMessageStore.this.longTermStore.addMessage(this.val$data[i]);
				}
				if (this.val$message != null)
					HowlMessageStore.this.longTermStore.addMessage(this.val$message);
			}
		});
		try {
			this.transactionLog.mark(this.lastLogMark);
		} catch (Exception e) {
			throw JMSExceptionHelper.newJMSException("Failed to checkpoint the Howl transaction log: " + e, e);
		}
	}

	protected boolean addMessageToCache(ActiveMQMessage message) {
		synchronized (this.map) {
			if ((this.map.size() < this.maximumCacheSize) && (this.longTermPersistence.hasCacheCapacity(this))) {
				this.map.put(message.getJMSMessageID(), message);
				return true;
			}
		}
		return false;
	}

	protected void readPacket(LogRecord logRecord, QueueMessageContainer container) {
		if ((!logRecord.isCTRL()) && (!logRecord.isEOB()) && (logRecord.length > 0))
			try {
				Packet packet = this.wireFormat.fromBytes(logRecord.data, 2, logRecord.length - 2);
				if ((packet instanceof ActiveMQMessage)) {
					container.addMessage((ActiveMQMessage) packet);
				} else if ((packet instanceof MessageAck)) {
					MessageAck ack = (MessageAck) packet;
					container.delete(ack.getMessageIdentity(), ack);
				} else {
					log.error("Unknown type of packet in transaction log which will be discarded: " + packet);
				}
			} catch (Exception e) {
				if (this.firstException == null)
					this.firstException = e;
			}
	}

	protected synchronized void writePacket(Packet packet) throws JMSException {
		try {
			byte[] data = this.wireFormat.toBytes(packet);
			this.lastLogMark = this.transactionLog.put(data, this.sync);
		} catch (IOException e) {
			throw createWriteException(packet, e);
		} catch (LogException e) {
			throw createWriteException(packet, e);
		} catch (InterruptedException e) {
			throw createWriteException(packet, e);
		}
	}

	protected JMSException createRecoveryFailedException(Exception e) {
		return JMSExceptionHelper.newJMSException("Failed to recover from Howl transaction log. Reason: " + e, e);
	}

	protected JMSException createWriteException(Packet packet, Exception e) {
		return JMSExceptionHelper
				.newJMSException("Failed to write to Howl transaction log for: " + packet + ". Reason: " + e, e);
	}
}