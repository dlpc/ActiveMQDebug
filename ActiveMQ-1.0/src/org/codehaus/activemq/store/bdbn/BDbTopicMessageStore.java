package org.codehaus.activemq.store.bdbn;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.MessageIdentity;
import org.codehaus.activemq.service.SubscriberEntry;
import org.codehaus.activemq.service.Subscription;
import org.codehaus.activemq.store.TopicMessageStore;

public class BDbTopicMessageStore extends BDbMessageStore
  implements TopicMessageStore
{
  public void incrementMessageCount(MessageIdentity messageId)
    throws JMSException
  {
  }

  public void decrementMessageCountAndMaybeDelete(MessageIdentity messageIdentity, MessageAck ack)
    throws JMSException
  {
  }

  public void setLastAcknowledgedMessageIdentity(Subscription subscription, MessageIdentity messageIdentity)
    throws JMSException
  {
  }

  public void recoverSubscription(Subscription subscription, MessageIdentity lastDispatchedMessage)
  {
  }

  public MessageIdentity getLastestMessageIdentity()
    throws JMSException
  {
    return null;
  }

  public SubscriberEntry getSubscriberEntry(ConsumerInfo info) throws JMSException {
    return null;
  }

  public void setSubscriberEntry(ConsumerInfo info, SubscriberEntry subscriberEntry) throws JMSException
  {
  }

  public void stop() throws JMSException {
    super.stop();
  }
}