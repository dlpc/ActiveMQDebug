package org.codehaus.activemq.store.bdbn;

import com.sleepycat.db.Db;
import com.sleepycat.db.DbEnv;
import com.sleepycat.db.DbException;
import java.io.FileNotFoundException;
import javax.jms.JMSException;
import org.codehaus.activemq.service.impl.PersistenceAdapterSupport;
import org.codehaus.activemq.store.MessageStore;
import org.codehaus.activemq.store.PreparedTransactionStore;
import org.codehaus.activemq.store.TopicMessageStore;
import org.codehaus.activemq.util.JMSExceptionHelper;

public class BDbPersistenceAdapter extends PersistenceAdapterSupport
{
  protected DbEnv environment;

  public BDbPersistenceAdapter(DbEnv environment)
  {
    this.environment = environment;
  }

  public MessageStore createQueueMessageStore(String destinationName) throws JMSException {
    return null;
  }

  public TopicMessageStore createTopicMessageStore(String destinationName) {
    return null;
  }

  public PreparedTransactionStore createPreparedTransactionStore() throws JMSException {
    return null;
  }

  public void beginTransaction() throws JMSException {
    try {
      BDbHelper.createTransaction(this.environment);
    }
    catch (DbException e) {
      throw JMSExceptionHelper.newJMSException("Failed to commit transaction. Reason: " + e, e);
    }
  }

  public void commitTransaction() throws JMSException {
    BDbHelper.commitTransaction(BDbHelper.getTransaction());
  }

  public void rollbackTransaction() {
    BDbHelper.rollbackTransaction(BDbHelper.getTransaction());
  }

  public void start() throws JMSException {
  }

  public void stop() throws JMSException {
    try {
      this.environment.close(0);
    }
    catch (DbException e) {
      throw JMSExceptionHelper.newJMSException("Failed to close environment. Reason: " + e, e);
    }
  }

  protected Db createDatabase(String name)
    throws JMSException, FileNotFoundException, DbException
  {
    return BDbHelper.open(this.environment, name, false);
  }
}