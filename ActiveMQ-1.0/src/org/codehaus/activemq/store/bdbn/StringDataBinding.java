package org.codehaus.activemq.store.bdbn;

import com.sleepycat.bdb.bind.DataBinding;
import com.sleepycat.bdb.bind.DataBuffer;
import com.sleepycat.bdb.bind.DataFormat;
import java.io.IOException;

public class StringDataBinding
  implements DataBinding
{
  private static StringDataFormat format = new StringDataFormat();

  public Object dataToObject(DataBuffer buffer) throws IOException {
    return new String(buffer.getDataBytes(), buffer.getDataOffset(), buffer.getDataLength());
  }

  public void objectToData(Object object, DataBuffer buffer) throws IOException {
    String text = (String)object;
    byte[] data = text.getBytes();
    buffer.setData(data, 0, data.length);
  }

  public DataFormat getDataFormat() {
    return format;
  }
}