package org.codehaus.activemq.store;

import javax.jms.JMSException;
import org.codehaus.activemq.service.QueueMessageContainer;
import org.codehaus.activemq.service.Service;
import org.codehaus.activemq.service.TopicMessageContainer;

public abstract interface PersistenceAdapter extends Service
{
  public abstract MessageStore createQueueMessageStore(String paramString)
    throws JMSException;

  public abstract TopicMessageStore createTopicMessageStore(String paramString)
    throws JMSException;

  public abstract PreparedTransactionStore createPreparedTransactionStore()
    throws JMSException;

  public abstract QueueMessageContainer createQueueMessageContainer(String paramString)
    throws JMSException;

  public abstract TopicMessageContainer createTopicMessageContainer(String paramString)
    throws JMSException;

  public abstract void beginTransaction()
    throws JMSException;

  public abstract void commitTransaction()
    throws JMSException;

  public abstract void rollbackTransaction();
}