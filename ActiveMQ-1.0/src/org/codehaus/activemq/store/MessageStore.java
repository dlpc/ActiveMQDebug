package org.codehaus.activemq.store;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.MessageIdentity;
import org.codehaus.activemq.service.QueueMessageContainer;
import org.codehaus.activemq.service.Service;

public abstract interface MessageStore extends Service
{
  public abstract MessageIdentity addMessage(ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract ActiveMQMessage getMessage(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void removeMessage(MessageIdentity paramMessageIdentity, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void recover(QueueMessageContainer paramQueueMessageContainer)
    throws JMSException;
}