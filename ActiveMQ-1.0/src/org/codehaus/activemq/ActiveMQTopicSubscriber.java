package org.codehaus.activemq;

import javax.jms.JMSException;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import org.codehaus.activemq.message.ActiveMQDestination;

public class ActiveMQTopicSubscriber extends ActiveMQMessageConsumer
  implements TopicSubscriber
{
  protected ActiveMQTopicSubscriber(ActiveMQSession theSession, ActiveMQDestination dest, String name, String selector, int cnum, int prefetch, boolean noLocalValue, boolean browserValue)
    throws JMSException
  {
    super(theSession, dest, name, selector, cnum, prefetch, noLocalValue, browserValue);

    if (name != null)
    {
      theSession.connection.checkClientIDWasManuallySpecified();
    }
  }

  public Topic getTopic()
    throws JMSException
  {
    checkClosed();
    return (Topic)super.getDestination();
  }

  public boolean getNoLocal()
    throws JMSException
  {
    checkClosed();
    return super.isNoLocal();
  }
}