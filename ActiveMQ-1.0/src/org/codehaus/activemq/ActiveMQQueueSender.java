package org.codehaus.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueSender;
import org.codehaus.activemq.message.ActiveMQDestination;

public class ActiveMQQueueSender extends ActiveMQMessageProducer
  implements QueueSender
{
  protected ActiveMQQueueSender(ActiveMQSession session, ActiveMQDestination destination)
    throws JMSException
  {
    super(session, destination);
  }

  public Queue getQueue()
    throws JMSException
  {
    return (Queue)super.getDestination();
  }

  public void send(Queue queue, Message message)
    throws JMSException
  {
    super.send(queue, message);
  }

  public void send(Queue queue, Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException
  {
    super.send(queue, message, deliveryMode, priority, timeToLive);
  }
}