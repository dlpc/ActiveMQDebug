package org.codehaus.activemq.service;

import javax.jms.JMSException;

public abstract interface QueueListEntry
{
  public abstract Object getElement()
    throws JMSException;
}