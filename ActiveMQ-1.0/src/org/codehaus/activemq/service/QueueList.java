package org.codehaus.activemq.service;

import javax.jms.JMSException;

public abstract interface QueueList
{
  public static final Object[] EMPTY_ARRAY = new Object[0];

  public abstract Object getFirst()
    throws JMSException;

  public abstract Object getLast()
    throws JMSException;

  public abstract Object removeFirst()
    throws JMSException;

  public abstract void rotate()
    throws JMSException;

  public abstract Object removeLast()
    throws JMSException;

  public abstract QueueListEntry addFirst(Object paramObject)
    throws JMSException;

  public abstract QueueListEntry addLast(Object paramObject)
    throws JMSException;

  public abstract boolean contains(Object paramObject)
    throws JMSException;

  public abstract int size()
    throws JMSException;

  public abstract boolean isEmpty()
    throws JMSException;

  public abstract QueueListEntry add(Object paramObject)
    throws JMSException;

  public abstract boolean remove(Object paramObject)
    throws JMSException;

  public abstract void clear()
    throws JMSException;

  public abstract Object get(int paramInt)
    throws JMSException;

  public abstract Object set(int paramInt, Object paramObject)
    throws JMSException;

  public abstract void add(int paramInt, Object paramObject)
    throws JMSException;

  public abstract Object remove(int paramInt)
    throws JMSException;

  public abstract int indexOf(Object paramObject)
    throws JMSException;

  public abstract int lastIndexOf(Object paramObject)
    throws JMSException;

  public abstract QueueListEntry getFirstEntry()
    throws JMSException;

  public abstract QueueListEntry getLastEntry()
    throws JMSException;

  public abstract QueueListEntry getNextEntry(QueueListEntry paramQueueListEntry)
    throws JMSException;

  public abstract QueueListEntry getPrevEntry(QueueListEntry paramQueueListEntry)
    throws JMSException;

  public abstract QueueListEntry addBefore(Object paramObject, QueueListEntry paramQueueListEntry)
    throws JMSException;

  public abstract void remove(QueueListEntry paramQueueListEntry)
    throws JMSException;

  public abstract Object[] toArray()
    throws JMSException;
}