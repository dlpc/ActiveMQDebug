package org.codehaus.activemq.service;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ActiveMQDestination;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.MessageAck;

public abstract interface Subscription
{
  public abstract void setActiveConsumer(ConsumerInfo paramConsumerInfo);

  public abstract void clear()
    throws JMSException;

  public abstract void reset()
    throws JMSException;

  public abstract String getClientId();

  public abstract String getSubscriberName();

  public abstract ActiveMQDestination getDestination();

  public abstract String getSelector();

  public abstract boolean isActive();

  public abstract void setActive(boolean paramBoolean)
    throws JMSException;

  public abstract int getConsumerNumber();

  public abstract String getConsumerId();

  public abstract boolean isTarget(ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void addMessage(MessageContainer paramMessageContainer, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void messageConsumed(MessageAck paramMessageAck)
    throws JMSException;

  public abstract void redeliverMessage(MessageContainer paramMessageContainer, MessageAck paramMessageAck)
    throws JMSException;

  public abstract ActiveMQMessage[] getMessagesToDispatch()
    throws JMSException;

  public abstract boolean isReadyToDispatch()
    throws JMSException;

  public abstract boolean isAtPrefetchLimit()
    throws JMSException;

  public abstract boolean isDurableTopic()
    throws JMSException;

  public abstract boolean isBrowser()
    throws JMSException;

  public abstract MessageIdentity getLastMessageIdentity()
    throws JMSException;

  public abstract void setLastMessageIdentifier(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract boolean isWildcard();

  public abstract String getPersistentKey();

  public abstract boolean isSameDurableSubscription(ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void onAcknowledgeTransactedMessageBeforeCommit(MessageAck paramMessageAck)
    throws JMSException;
}