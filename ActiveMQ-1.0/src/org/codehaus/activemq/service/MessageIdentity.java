package org.codehaus.activemq.service;

import java.io.Serializable;

public class MessageIdentity
  implements Comparable, Serializable
{
  private String messageID;
  private Object sequenceNumber;

  public MessageIdentity()
  {
  }

  public MessageIdentity(String messageID)
  {
    this.messageID = messageID;
  }

  public MessageIdentity(String messageID, Object sequenceNumber) {
    this.messageID = messageID;
    this.sequenceNumber = sequenceNumber;
  }

  public int hashCode() {
    return this.messageID.hashCode() ^ 0xCAFEBABE;
  }

  public boolean equals(Object that) {
    return ((that instanceof MessageIdentity)) && (equals((MessageIdentity)that));
  }

  public boolean equals(MessageIdentity that) {
    return (this.messageID == that.messageID) || (this.messageID.equals(that.messageID));
  }

  public int compareTo(Object object) {
    if (this == object) {
      return 0;
    }

    if ((object instanceof MessageIdentity)) {
      MessageIdentity that = (MessageIdentity)object;
      return this.messageID.compareTo(that.messageID);
    }

    return -1;
  }

  public String toString()
  {
    return super.toString() + "[id=" + this.messageID + "; sequenceNo=" + this.sequenceNumber + "]";
  }

  public String getMessageID() {
    return this.messageID;
  }

  public void setMessageID(String messageID) {
    this.messageID = messageID;
  }

  public Object getSequenceNumber()
  {
    return this.sequenceNumber;
  }

  public void setSequenceNumber(Object sequenceNumber) {
    this.sequenceNumber = sequenceNumber;
  }
}