package org.codehaus.activemq.service.impl;

import javax.jms.JMSException;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.MessageContainer;
import org.codehaus.activemq.service.MessageIdentity;

class MessagePointer
{
  private MessageContainer container;
  private MessageIdentity messageIdentity;
  private boolean dispatched;
  private boolean read;

  public MessagePointer(MessageContainer container, MessageIdentity messageIdentity)
    throws JMSException
  {
    this.container = container;
    this.messageIdentity = messageIdentity;
    this.container.registerMessageInterest(this.messageIdentity);
  }

  public void reset()
  {
    this.dispatched = false;
    this.read = false;
  }

  public void clear()
    throws JMSException
  {
    this.container.unregisterMessageInterest(this.messageIdentity, null);
  }

  public void delete(MessageAck ack)
    throws JMSException
  {
    clear();
    this.container.delete(this.messageIdentity, ack);
  }

  public MessageContainer getContainer()
  {
    return this.container;
  }

  public void setContainer(MessageContainer container)
  {
    this.container = container;
  }

  public boolean isDispatched()
  {
    return this.dispatched;
  }

  public void setDispatched(boolean dispatched)
  {
    this.dispatched = dispatched;
  }

  public boolean isRead()
  {
    return this.read;
  }

  public void setRead(boolean read)
  {
    this.read = read;
  }

  public MessageIdentity getMessageIdentity() {
    return this.messageIdentity;
  }

  public void setMessageIdentity(MessageIdentity messageIdentity) {
    this.messageIdentity = messageIdentity;
  }
}