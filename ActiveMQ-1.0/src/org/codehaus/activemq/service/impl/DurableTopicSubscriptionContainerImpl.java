package org.codehaus.activemq.service.impl;

import org.codehaus.activemq.filter.Filter;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.service.Dispatcher;
import org.codehaus.activemq.service.Subscription;

public class DurableTopicSubscriptionContainerImpl extends SubscriptionContainerImpl
{
  protected Subscription createSubscription(Dispatcher dispatcher, ConsumerInfo info, Filter filter)
  {
    return new DurableTopicSubscription(dispatcher, info, filter);
  }
}