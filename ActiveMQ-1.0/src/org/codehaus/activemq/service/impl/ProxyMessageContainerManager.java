package org.codehaus.activemq.service.impl;

import javax.jms.JMSException;
import org.codehaus.activemq.broker.BrokerClient;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.MessageContainer;
import org.codehaus.activemq.service.MessageContainerManager;

public abstract class ProxyMessageContainerManager
  implements MessageContainerManager
{
  private MessageContainerManager delegate;

  public ProxyMessageContainerManager()
  {
  }

  public ProxyMessageContainerManager(MessageContainerManager delegate)
  {
    this.delegate = delegate;
  }

  public void acknowledgeMessage(BrokerClient client, MessageAck ack) throws JMSException {
    getDelegate().acknowledgeMessage(client, ack);
  }

  public void acknowledgeTransactedMessage(BrokerClient client, String transactionId, MessageAck ack) throws JMSException {
    getDelegate().acknowledgeTransactedMessage(client, transactionId, ack);
  }

  public void redeliverMessage(BrokerClient client, MessageAck ack) throws JMSException {
    getDelegate().redeliverMessage(client, ack);
  }

  public void addMessageConsumer(BrokerClient client, ConsumerInfo info) throws JMSException {
    getDelegate().addMessageConsumer(client, info);
  }

  public void commitTransaction(BrokerClient client, String transactionId) throws JMSException {
    getDelegate().commitTransaction(client, transactionId);
  }

  public void deleteSubscription(String clientId, String subscriberName) throws JMSException {
    getDelegate().deleteSubscription(clientId, subscriberName);
  }

  public void poll() throws JMSException {
    getDelegate().poll();
  }

  public void removeMessageConsumer(BrokerClient client, ConsumerInfo info) throws JMSException {
    getDelegate().removeMessageConsumer(client, info);
  }

  public void rollbackTransaction(BrokerClient client, String transactionId) {
    getDelegate().rollbackTransaction(client, transactionId);
  }

  public void sendMessage(BrokerClient client, ActiveMQMessage message) throws JMSException {
    getDelegate().sendMessage(client, message);
  }

  public MessageContainer getContainer(String physicalName) throws JMSException {
    return getDelegate().getContainer(physicalName);
  }

  public void start() throws JMSException {
    getDelegate().start();
  }

  public void stop() throws JMSException {
    getDelegate().stop();
  }

  protected MessageContainerManager getDelegate()
  {
    return this.delegate;
  }

  protected void setDelegate(MessageContainerManager delegate) {
    this.delegate = delegate;
  }
}