package org.codehaus.activemq.service.impl;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import javax.jms.JMSException;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.DefaultWireFormat;
import org.codehaus.activemq.message.WireFormat;
import org.codehaus.activemq.util.JMSExceptionHelper;

public class MessageEntry
  implements Externalizable
{
  private static final long serialVersionUID = -3590625465815936811L;
  private static final WireFormat wireFormat = new DefaultWireFormat();
  private ActiveMQMessage message;

  public MessageEntry()
  {
  }

  public MessageEntry(ActiveMQMessage msg)
  {
    this.message = msg;
  }

  public ActiveMQMessage getMessage()
  {
    return this.message;
  }

  public int hashCode()
  {
    return this.message != null ? this.message.hashCode() : super.hashCode();
  }

  public boolean equals(Object obj)
  {
    boolean result = false;
    if ((obj != null) && ((obj instanceof MessageEntry))) {
      MessageEntry other = (MessageEntry)obj;
      result = ((this.message != null) && (other.message != null) && (this.message.equals(other.message))) || ((this.message == null) && (other.message == null));
    }

    return result;
  }

  public void writeExternal(ObjectOutput out) throws IOException {
    try {
      wireFormat.writePacket(this.message, out);
    }
    catch (JMSException e) {
      throw JMSExceptionHelper.newIOException(e);
    }
  }

  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    this.message = ((ActiveMQMessage)wireFormat.readPacket(in));
  }
}