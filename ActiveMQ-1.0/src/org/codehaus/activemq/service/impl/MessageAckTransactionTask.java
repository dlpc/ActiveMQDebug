package org.codehaus.activemq.service.impl;

import org.codehaus.activemq.broker.Broker;
import org.codehaus.activemq.broker.BrokerClient;
import org.codehaus.activemq.message.MessageAck;

public class MessageAckTransactionTask extends PacketTransactionTask
{
  public MessageAckTransactionTask(BrokerClient brokerClient, MessageAck ack)
  {
    super(brokerClient, ack);
  }

  public void execute(Broker broker) throws Throwable {
    MessageAck ack = (MessageAck)getPacket();
    broker.acknowledgeMessage(getBrokerClient(ack.getConsumerId()), ack);
  }
}