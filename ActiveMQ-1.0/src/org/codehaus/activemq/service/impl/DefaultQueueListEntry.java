package org.codehaus.activemq.service.impl;

import org.codehaus.activemq.service.QueueListEntry;

public final class DefaultQueueListEntry
  implements QueueListEntry
{
  Object element;
  DefaultQueueListEntry next;
  DefaultQueueListEntry previous;

  public Object getElement()
  {
    return this.element;
  }

  DefaultQueueListEntry(Object element, DefaultQueueListEntry next, DefaultQueueListEntry previous)
  {
    this.element = element;
    this.next = next;
    this.previous = previous;
  }
}