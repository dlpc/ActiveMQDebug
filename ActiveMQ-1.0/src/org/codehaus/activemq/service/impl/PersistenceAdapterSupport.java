package org.codehaus.activemq.service.impl;

import javax.jms.JMSException;
import org.codehaus.activemq.service.QueueMessageContainer;
import org.codehaus.activemq.service.TopicMessageContainer;
import org.codehaus.activemq.store.PersistenceAdapter;

public abstract class PersistenceAdapterSupport
  implements PersistenceAdapter
{
  public TopicMessageContainer createTopicMessageContainer(String destinationName)
    throws JMSException
  {
    return new DurableTopicMessageContainer(createTopicMessageStore(destinationName), destinationName);
  }

  public QueueMessageContainer createQueueMessageContainer(String destinationName) throws JMSException {
    return new DurableQueueMessageContainer(this, createQueueMessageStore(destinationName), destinationName);
  }
}