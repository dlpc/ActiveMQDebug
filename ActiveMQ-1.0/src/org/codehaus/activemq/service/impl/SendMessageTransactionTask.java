package org.codehaus.activemq.service.impl;

import org.codehaus.activemq.broker.Broker;
import org.codehaus.activemq.broker.BrokerClient;
import org.codehaus.activemq.message.ActiveMQMessage;

public class SendMessageTransactionTask extends PacketTransactionTask
{
  public SendMessageTransactionTask(BrokerClient brokerClient, ActiveMQMessage message)
  {
    super(brokerClient, message);
  }

  public void execute(Broker broker) throws Throwable {
    ActiveMQMessage message = (ActiveMQMessage)getPacket();
    broker.sendMessage(getBrokerClient(message.getConsumerId()), message);
  }
}