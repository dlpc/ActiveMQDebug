package org.codehaus.activemq.service;

import javax.jms.JMSException;
import org.codehaus.activemq.broker.BrokerClient;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.MessageAck;

public abstract interface MessageContainerManager extends Service
{
  public abstract void addMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void removeMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void deleteSubscription(String paramString1, String paramString2)
    throws JMSException;

  public abstract void sendMessage(BrokerClient paramBrokerClient, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void acknowledgeMessage(BrokerClient paramBrokerClient, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void redeliverMessage(BrokerClient paramBrokerClient, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void poll()
    throws JMSException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, String paramString);

  public abstract MessageContainer getContainer(String paramString)
    throws JMSException;

  public abstract void acknowledgeTransactedMessage(BrokerClient paramBrokerClient, String paramString, MessageAck paramMessageAck)
    throws JMSException;
}