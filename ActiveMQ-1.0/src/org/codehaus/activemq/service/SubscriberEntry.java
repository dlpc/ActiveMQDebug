package org.codehaus.activemq.service;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class SubscriberEntry
  implements Externalizable
{
  private static final long serialVersionUID = -5754338187296859149L;
  private int subscriberID;
  private String clientID;
  private String consumerName;
  private String destination;
  private String selector;

  public void readExternal(ObjectInput in)
    throws IOException, ClassNotFoundException
  {
    this.subscriberID = in.readInt();
    this.clientID = in.readUTF();
    this.consumerName = in.readUTF();
    this.destination = in.readUTF();
    this.selector = in.readUTF();
  }

  public void writeExternal(ObjectOutput out) throws IOException {
    out.writeInt(this.subscriberID);
    out.writeUTF(this.clientID);
    out.writeUTF(this.consumerName);
    out.writeUTF(this.destination);
    out.writeUTF(this.selector);
  }

  public String getClientID()
  {
    return this.clientID;
  }

  public void setClientID(String clientID) {
    this.clientID = clientID;
  }

  public String getConsumerName() {
    return this.consumerName;
  }

  public void setConsumerName(String consumerName) {
    this.consumerName = consumerName;
  }

  public String getDestination() {
    return this.destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getSelector() {
    return this.selector;
  }

  public void setSelector(String selector) {
    this.selector = selector;
  }

  public int getSubscriberID() {
    return this.subscriberID;
  }

  public void setSubscriberID(int subscriberID) {
    this.subscriberID = subscriberID;
  }
}