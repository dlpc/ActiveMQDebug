package org.codehaus.activemq.service;

import org.codehaus.activemq.broker.BrokerClient;

public abstract interface Dispatcher extends Service
{
  public abstract void register(MessageContainerManager paramMessageContainerManager);

  public abstract void wakeup(Subscription paramSubscription);

  public abstract void wakeup();

  public abstract void addActiveSubscription(BrokerClient paramBrokerClient, Subscription paramSubscription);

  public abstract void removeActiveSubscription(BrokerClient paramBrokerClient, Subscription paramSubscription);
}