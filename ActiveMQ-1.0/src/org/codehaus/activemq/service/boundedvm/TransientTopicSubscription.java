package org.codehaus.activemq.service.boundedvm;

import javax.jms.JMSException;
import org.codehaus.activemq.filter.Filter;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ConsumerInfo;

public class TransientTopicSubscription
{
  private Filter filter;
  private ConsumerInfo consumerInfo;

  public TransientTopicSubscription(Filter filter, ConsumerInfo info)
  {
    this.filter = filter;
    this.consumerInfo = info;
  }

  public boolean isTarget(ActiveMQMessage message)
    throws JMSException
  {
    boolean result = false;
    if (message != null) {
      result = this.filter.matches(message);

      if ((result) && (this.consumerInfo.isDurableTopic())) {
        result = message.getJMSDeliveryMode() == 1;
      }
    }
    return result;
  }

  public ConsumerInfo getConsumerInfo()
  {
    return this.consumerInfo;
  }

  public void setConsumerInfo(ConsumerInfo consumerInfo)
  {
    this.consumerInfo = consumerInfo;
  }
}