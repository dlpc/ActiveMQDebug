package org.codehaus.activemq.service;

import java.io.Serializable;
import javax.transaction.xa.XAException;
import org.codehaus.activemq.broker.Broker;

public abstract interface Transaction extends Serializable
{
  public abstract void addPrePrepareTask(TransactionTask paramTransactionTask);

  public abstract void addPostCommitTask(TransactionTask paramTransactionTask);

  public abstract void addPostRollbackTask(TransactionTask paramTransactionTask);

  public abstract void commit(boolean paramBoolean)
    throws XAException;

  public abstract void rollback()
    throws XAException;

  public abstract int prepare()
    throws XAException;

  public abstract void setBroker(Broker paramBroker);
}