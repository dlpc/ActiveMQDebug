package org.codehaus.activemq.service;

import javax.jms.JMSException;

public abstract interface Service
{
  public abstract void start()
    throws JMSException;

  public abstract void stop()
    throws JMSException;
}