package org.codehaus.activemq.service;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.MessageAck;

public abstract interface MessageContainer extends Service
{
  public abstract String getDestinationName();

  public abstract MessageIdentity addMessage(ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void delete(MessageIdentity paramMessageIdentity, MessageAck paramMessageAck)
    throws JMSException;

  public abstract ActiveMQMessage getMessage(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void registerMessageInterest(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void unregisterMessageInterest(MessageIdentity paramMessageIdentity, MessageAck paramMessageAck)
    throws JMSException;

  public abstract boolean containsMessage(MessageIdentity paramMessageIdentity)
    throws JMSException;
}