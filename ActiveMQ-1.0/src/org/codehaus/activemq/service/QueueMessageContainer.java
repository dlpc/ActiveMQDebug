package org.codehaus.activemq.service;

import javax.jms.JMSException;
import org.codehaus.activemq.message.ActiveMQMessage;

public abstract interface QueueMessageContainer extends MessageContainer
{
  public abstract ActiveMQMessage poll()
    throws JMSException;

  public abstract ActiveMQMessage peekNext(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void returnMessage(MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void reset()
    throws JMSException;

  public abstract void start()
    throws JMSException;

  public abstract void recoverMessageToBeDelivered(MessageIdentity paramMessageIdentity)
    throws JMSException;
}