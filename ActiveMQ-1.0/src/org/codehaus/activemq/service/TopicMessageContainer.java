package org.codehaus.activemq.service;

import javax.jms.JMSException;

public abstract interface TopicMessageContainer extends MessageContainer
{
  public abstract void setLastAcknowledgedMessageID(Subscription paramSubscription, MessageIdentity paramMessageIdentity)
    throws JMSException;

  public abstract void recoverSubscription(Subscription paramSubscription)
    throws JMSException;
}