package org.codehaus.activemq.service;

import java.util.Iterator;
import javax.jms.JMSException;
import org.codehaus.activemq.broker.BrokerClient;
import org.codehaus.activemq.filter.Filter;
import org.codehaus.activemq.message.ConsumerInfo;

public abstract interface SubscriptionContainer
{
  public abstract Subscription getSubscription(String paramString);

  public abstract Subscription removeSubscription(String paramString);

  public abstract Iterator subscriptionIterator();

  public abstract Subscription makeSubscription(Dispatcher paramDispatcher, ConsumerInfo paramConsumerInfo, Filter paramFilter);

  public abstract void checkForDuplicateDurableSubscription(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;
}