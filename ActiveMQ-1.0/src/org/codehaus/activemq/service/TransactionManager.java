package org.codehaus.activemq.service;

import javax.jms.JMSException;
import javax.transaction.xa.XAException;
import org.codehaus.activemq.broker.BrokerClient;
import org.codehaus.activemq.message.ActiveMQXid;

public abstract interface TransactionManager extends Service
{
  public abstract void setContexTransaction(Transaction paramTransaction);

  public abstract Transaction getContexTransaction();

  public abstract Transaction createLocalTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract Transaction getLocalTransaction(String paramString)
    throws JMSException;

  public abstract Transaction createXATransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract Transaction getXATransaction(ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract ActiveMQXid[] getPreparedXATransactions()
    throws XAException;

  public abstract void cleanUpClient(BrokerClient paramBrokerClient)
    throws JMSException;

  public abstract void loadTransaction(ActiveMQXid paramActiveMQXid, Transaction paramTransaction)
    throws XAException;
}