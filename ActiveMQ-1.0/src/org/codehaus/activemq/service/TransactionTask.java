package org.codehaus.activemq.service;

import java.io.Serializable;
import org.codehaus.activemq.broker.Broker;

public abstract interface TransactionTask extends Serializable
{
  public abstract void execute(Broker paramBroker)
    throws Throwable;
}