package org.codehaus.activemq.gbean;

import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.geronimo.gbean.GAttributeInfo;
import org.apache.geronimo.gbean.GBeanInfo;
import org.apache.geronimo.gbean.GBeanInfoFactory;
import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.GConstructorInfo;
import org.apache.geronimo.gbean.GReferenceInfo;
import org.apache.geronimo.gbean.WaitingException;
import org.codehaus.activemq.ActiveMQConnectionFactory;
import org.codehaus.activemq.broker.BrokerConnector;
import org.codehaus.activemq.broker.impl.BrokerConnectorImpl;
import org.codehaus.activemq.message.DefaultWireFormat;
import org.codehaus.activemq.message.WireFormat;

public class ActiveMQConnector
  implements GBeanLifecycle
{
  private Log log = LogFactory.getLog(getClass().getName());
  private BrokerConnector brokerConnector;
  private ActiveMQContainer container;
  private WireFormat wireFormat = new DefaultWireFormat();
  private String url = "tcp://localhost:61616";
  public static final GBeanInfo GBEAN_INFO=null;

  public ActiveMQConnector(ActiveMQContainer container)
  {
    this.container = container;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public WireFormat getWireFormat() {
    return this.wireFormat;
  }

  public void setWireFormat(WireFormat wireFormat)
  {
    if (wireFormat == null) {
      this.wireFormat = new DefaultWireFormat();
    }
    else
      this.wireFormat = wireFormat;
  }

  public synchronized void doStart() throws WaitingException, Exception
  {
    if (this.brokerConnector == null) {
      this.brokerConnector = createBrokerConnector();
      this.brokerConnector.start();
      ActiveMQConnectionFactory.registerBroker(this.url, this.brokerConnector);
    }
  }

  public synchronized void doStop() throws WaitingException, Exception {
    if (this.brokerConnector != null) {
      ActiveMQConnectionFactory.unregisterBroker(this.url);
      BrokerConnector temp = this.brokerConnector;
      this.brokerConnector = null;
      temp.stop();
    }
  }

  public synchronized void doFail() {
    if (this.brokerConnector != null) {
      BrokerConnector temp = this.brokerConnector;
      this.brokerConnector = null;
      try {
        temp.stop();
      }
      catch (JMSException e) {
        this.log.info("Caught while closing due to failure: " + e, e);
      }
    }
  }

  protected BrokerConnector createBrokerConnector() throws Exception {
    return new BrokerConnectorImpl(this.container.getBrokerContainer(), this.url, this.wireFormat);
  }

  public static GBeanInfo getGBeanInfo()
  {
    return GBEAN_INFO;
  }

  static
  {
   // GBeanInfoFactory infoFactory = new GBeanInfoFactory("ActiveMQ Message Broker Connector", ActiveMQConnector.class.getName());
    //infoFactory.addAttribute(new GAttributeInfo("Url", String.class.getName(), true));
    //infoFactory.addAttribute(new GAttributeInfo("WireFormat", WireFormat.class.getName(), true));
   // infoFactory.addReference(new GReferenceInfo("ActiveMQContainer", ActiveMQContainer.class));
   // infoFactory.setConstructor(new GConstructorInfo(new String[] { "ActiveMQContainer" }));
    //GBEAN_INFO = infoFactory.getBeanInfo();
  }
}