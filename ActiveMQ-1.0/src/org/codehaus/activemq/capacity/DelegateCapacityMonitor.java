package org.codehaus.activemq.capacity;

import EDU.oswego.cs.dl.util.concurrent.CopyOnWriteArrayList;
import java.util.Iterator;

public class DelegateCapacityMonitor
  implements CapacityMonitor
{
  String name;
  CapacityMonitor monitor;
  CopyOnWriteArrayList listeners = new CopyOnWriteArrayList();

  public DelegateCapacityMonitor()
  {
  }

  public DelegateCapacityMonitor(String name, CapacityMonitor cm)
  {
    this.name = name;
    this.monitor = cm;
  }

  public void setDelegate(CapacityMonitor cm)
  {
    this.monitor = cm;
    Iterator i;
    if (cm != null)
      for (i = this.listeners.iterator(); i.hasNext(); ) {
        CapacityMonitorEventListener listener = (CapacityMonitorEventListener)i.next();
        cm.addCapacityEventListener(listener);
      }
  }

  public String getName()
  {
    return this.name;
  }

  public void setName(String newName)
  {
    this.name = newName;
  }

  public int getRoundingFactor()
  {
    return this.monitor == null ? 0 : this.monitor.getRoundingFactor();
  }

  public void setRoundingFactor(int newRoundingFactor)
  {
    if (this.monitor != null)
      this.monitor.setRoundingFactor(newRoundingFactor);
  }

  public void addCapacityEventListener(CapacityMonitorEventListener l)
  {
    this.listeners.add(l);
    if (this.monitor != null)
      this.monitor.addCapacityEventListener(l);
  }

  public void removeCapacityEventListener(CapacityMonitorEventListener l)
  {
    this.listeners.remove(l);
    if (this.monitor != null)
      this.monitor.removeCapacityEventListener(l);
  }

  public int getCurrentCapacity()
  {
    return this.monitor == null ? 100 : this.monitor.getCurrentCapacity();
  }

  public int getRoundedCapacity()
  {
    return this.monitor == null ? 100 : this.monitor.getRoundedCapacity();
  }

  public long getCurrentValue()
  {
    return this.monitor == null ? 100L : this.monitor.getCurrentValue();
  }

  public void setCurrentValue(long newCurrentValue)
  {
    if (this.monitor != null)
      this.monitor.setCurrentValue(newCurrentValue);
  }

  public long getValueLimit()
  {
    return this.monitor == null ? 100L : this.monitor.getValueLimit();
  }

  public void setValueLimit(long newValueLimit)
  {
    if (this.monitor != null)
      this.monitor.setValueLimit(newValueLimit);
  }
}