package org.codehaus.activemq.capacity;

public abstract interface CapacityMonitorEventListener
{
  public abstract void capacityChanged(CapacityMonitorEvent paramCapacityMonitorEvent);
}