package org.codehaus.activemq.capacity;

public abstract interface CapacityMonitor
{
  public abstract String getName();

  public abstract void setName(String paramString);

  public abstract int getRoundingFactor();

  public abstract void setRoundingFactor(int paramInt);

  public abstract void addCapacityEventListener(CapacityMonitorEventListener paramCapacityMonitorEventListener);

  public abstract void removeCapacityEventListener(CapacityMonitorEventListener paramCapacityMonitorEventListener);

  public abstract int getCurrentCapacity();

  public abstract int getRoundedCapacity();

  public abstract long getCurrentValue();

  public abstract void setCurrentValue(long paramLong);

  public abstract long getValueLimit();

  public abstract void setValueLimit(long paramLong);

  public static class BasicCapacityMonitor
  {
  }
}