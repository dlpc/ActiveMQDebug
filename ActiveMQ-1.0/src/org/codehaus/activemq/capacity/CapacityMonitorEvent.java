package org.codehaus.activemq.capacity;

public class CapacityMonitorEvent
{
  private String monitorName;
  private int capacity;

  public CapacityMonitorEvent()
  {
  }

  public CapacityMonitorEvent(String name, int newCapacity)
  {
    this.monitorName = name;
    this.capacity = newCapacity;
  }

  public int getCapacity()
  {
    return this.capacity;
  }

  public void setCapacity(int capacity)
  {
    this.capacity = capacity;
  }

  public String getMonitorName()
  {
    return this.monitorName;
  }

  public void setMonitorName(String monitorName)
  {
    this.monitorName = monitorName;
  }

  public String toString()
  {
    return this.monitorName + ": capacity = " + this.capacity;
  }
}