package org.codehaus.activemq.web;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ConnectionManager
  implements HttpSessionListener
{
  private static final Log log = LogFactory.getLog(ConnectionManager.class);

  public void sessionCreated(HttpSessionEvent event)
  {
  }

  public void sessionDestroyed(HttpSessionEvent event)
  {
  }
}