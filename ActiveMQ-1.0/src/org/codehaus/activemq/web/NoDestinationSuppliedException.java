package org.codehaus.activemq.web;

import javax.servlet.ServletException;

public class NoDestinationSuppliedException extends ServletException
{
  public NoDestinationSuppliedException()
  {
    super("Could not perform the JMS operation as no Destination was supplied");
  }
}