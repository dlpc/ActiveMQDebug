package org.codehaus.activemq;

import java.util.Enumeration;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import org.codehaus.activemq.message.ActiveMQDestination;
import org.codehaus.activemq.message.util.MemoryBoundedQueue;

public class ActiveMQQueueBrowser extends ActiveMQMessageConsumer
  implements QueueBrowser, Enumeration
{
  protected ActiveMQQueueBrowser(ActiveMQSession theSession, ActiveMQDestination dest, String selector, int cnum)
    throws JMSException
  {
    super(theSession, dest, "", selector, cnum, theSession.connection.getPrefetchPolicy().getQueueBrowserPrefetch(), false, true);
  }

  public Queue getQueue()
    throws JMSException
  {
    return (Queue)getDestination();
  }

  public Enumeration getEnumeration()
    throws JMSException
  {
    checkClosed();

    if (this.messageQueue.size() == 0)
      try {
        Thread.sleep(1000L);
      }
      catch (InterruptedException e)
      {
      }
    return this;
  }

  public boolean hasMoreElements()
  {
    return this.messageQueue.size() > 0;
  }

  public Object nextElement()
  {
    Object answer = null;
    try {
      answer = super.receiveNoWait();
    }
    catch (JMSException e) {
      e.printStackTrace();
    }
    return answer;
  }
}