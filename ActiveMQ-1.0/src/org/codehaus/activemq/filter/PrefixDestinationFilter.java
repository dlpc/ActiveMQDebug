package org.codehaus.activemq.filter;

import javax.jms.Destination;

public class PrefixDestinationFilter extends DestinationFilter
{
  private String[] prefixes;

  public PrefixDestinationFilter(String[] prefixes)
  {
    this.prefixes = prefixes;
  }

  public boolean matches(Destination destination) {
    String[] path = DestinationPath.getDestinationPaths(destination);
    int length = this.prefixes.length;
    if (path.length >= length) {
      int i = 0; for (int size = length - 1; i < size; i++) {
        if (!this.prefixes[i].equals(path[i])) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public String getText() {
    return DestinationPath.toString(this.prefixes);
  }

  public String toString() {
    return super.toString() + "[destination: " + getText() + "]";
  }

  public boolean isWildcard() {
    return true;
  }
}