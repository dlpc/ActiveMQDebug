package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public class PropertyExpression
  implements Expression
{
  private String name;

  public PropertyExpression(String name)
  {
    this.name = name;
  }

  public Object evaluate(Message message) throws JMSException {
    Object result = null;
    if (this.name != null) {
      result = message.getObjectProperty(this.name);
    }
    if (result == null)
    {
      if (this.name.equals("JMSType")) {
        result = message.getJMSType();
      }
      else if (this.name.equals("JMSMessageID")) {
        result = message.getJMSMessageID();
      }
      else if (this.name.equals("JMSCorrelationID")) {
        result = message.getJMSCorrelationID();
      }
      else if (this.name.equals("JMSPriority")) {
        result = new Integer(message.getJMSPriority());
      }
      else if (this.name.equals("JMSTimestamp")) {
        result = new Long(message.getJMSTimestamp());
      }
    }
    return result;
  }

  public String getName() {
    return this.name;
  }

  public String toString()
  {
    return this.name;
  }

  public int hashCode()
  {
    return this.name.hashCode();
  }

  public boolean equals(Object o)
  {
    if ((o == null) || (!getClass().equals(o.getClass()))) {
      return false;
    }
    return this.name.equals(((PropertyExpression)o).name);
  }
}