package org.codehaus.activemq.filter;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

public abstract class DestinationFilter
  implements Filter
{
  public static final String ANY_DESCENDENT = ">";
  public static final String ANY_CHILD = "*";

  public boolean matches(Message message)
    throws JMSException
  {
    return matches(message.getJMSDestination());
  }
  public abstract boolean matches(Destination paramDestination);

  public static DestinationFilter parseFilter(Destination destination) {
    String[] paths = DestinationPath.getDestinationPaths(destination);
    int idx = paths.length - 1;
    if (idx >= 0) {
      String lastPath = paths[idx];
      if (lastPath.equals(">")) {
        return new PrefixDestinationFilter(paths);
      }
      if (lastPath.equals("*")) {
        return new WildcardDestinationFilter(paths);
      }

    }

    return new SimpleDestinationFilter(destination);
  }
}