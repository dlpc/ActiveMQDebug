package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public abstract class LogicExpression extends BinaryExpression
  implements BooleanExpression
{
  public static BooleanExpression createOR(BooleanExpression lvalue, BooleanExpression rvalue)
  {
    return new LogicExpression(lvalue, rvalue) {
      protected Object evaluate(Boolean lv, Boolean rv) {
        return (lv.booleanValue()) || (rv.booleanValue()) ? Boolean.TRUE : Boolean.FALSE;
      }

      public String getExpressionSymbol() {
        return "OR";
      } } ;
  }

  public static BooleanExpression createAND(BooleanExpression lvalue, BooleanExpression rvalue) {
    return new LogicExpression(lvalue, rvalue) {
      protected Object evaluate(Boolean lv, Boolean rv) {
        return (lv.booleanValue()) && (rv.booleanValue()) ? Boolean.TRUE : Boolean.FALSE;
      }

      public String getExpressionSymbol() {
        return "AND";
      }
    };
  }

  public LogicExpression(BooleanExpression left, BooleanExpression right)
  {
    super(left, right);
  }

  public Object evaluate(Message message) throws JMSException {
    Boolean lv = (Boolean)this.left.evaluate(message);
    if (lv == null) {
      return null;
    }
    Boolean rv = (Boolean)this.right.evaluate(message);
    if (rv == null) {
      return null;
    }
    return evaluate(lv, rv);
  }

  protected abstract Object evaluate(Boolean paramBoolean1, Boolean paramBoolean2);
}