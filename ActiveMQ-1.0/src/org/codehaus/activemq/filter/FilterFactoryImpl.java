package org.codehaus.activemq.filter;

import javax.jms.Destination;
import javax.jms.JMSException;
import org.codehaus.activemq.selector.SelectorParser;

public class FilterFactoryImpl
  implements FilterFactory
{
  public Filter createFilter(Destination destination, String selector)
    throws JMSException
  {
    Filter destinationFilter = DestinationFilter.parseFilter(destination);
    if ((selector != null) && (selector.trim().length() > 0)) {
      Filter selectorFilter = new SelectorParser().parse(selector);
      if (selectorFilter != null) {
        return new AndFilter(destinationFilter, selectorFilter);
      }
    }
    return destinationFilter;
  }
}