package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public class ExpressionFilter
  implements Filter
{
  private Expression expression;

  public ExpressionFilter(Expression expression)
  {
    this.expression = expression;
  }

  public boolean matches(Message message) throws JMSException {
    Object value = this.expression.evaluate(message);
    if ((value != null) && ((value instanceof Boolean))) {
      return ((Boolean)value).booleanValue();
    }
    return false;
  }

  public boolean isWildcard() {
    return false;
  }

  public Expression getExpression()
  {
    return this.expression;
  }
}