package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public class NotFilter
  implements Filter
{
  private Filter filter;

  public NotFilter(Filter filter)
  {
    this.filter = filter;
  }

  public boolean matches(Message message) throws JMSException {
    return !this.filter.matches(message);
  }

  public boolean isWildcard() {
    return false;
  }
}