package org.codehaus.activemq.filter;

import javax.jms.Destination;

public class SimpleDestinationFilter extends DestinationFilter
{
  private Destination destination;

  public SimpleDestinationFilter(Destination destination)
  {
    this.destination = destination;
  }

  public boolean matches(Destination destination) {
    return this.destination.equals(destination);
  }

  public boolean isWildcard() {
    return false;
  }
}