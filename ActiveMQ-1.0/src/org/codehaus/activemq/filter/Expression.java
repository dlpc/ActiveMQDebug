package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public abstract interface Expression
{
  public abstract Object evaluate(Message paramMessage)
    throws JMSException;
}