package org.codehaus.activemq.filter;

import javax.jms.Destination;
import javax.jms.JMSException;

public abstract interface FilterFactory
{
  public abstract Filter createFilter(Destination paramDestination, String paramString)
    throws JMSException;
}