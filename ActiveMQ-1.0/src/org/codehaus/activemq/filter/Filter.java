package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public abstract interface Filter
{
  public abstract boolean matches(Message paramMessage)
    throws JMSException;

  public abstract boolean isWildcard();
}