package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;

public class OrFilter
  implements Filter
{
  private Filter left;
  private Filter right;

  public OrFilter(Filter left, Filter right)
  {
    this.left = left;
    this.right = right;
  }

  public boolean matches(Message message) throws JMSException {
    if (this.left.matches(message)) {
      return true;
    }

    return this.right.matches(message);
  }

  public boolean isWildcard()
  {
    return (this.left.isWildcard()) || (this.right.isWildcard());
  }

  public Filter getLeft() {
    return this.left;
  }

  public Filter getRight() {
    return this.right;
  }
}