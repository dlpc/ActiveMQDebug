package org.codehaus.activemq.filter;

import javax.jms.Destination;

public class WildcardDestinationFilter extends DestinationFilter
{
  private String[] prefixes;

  public WildcardDestinationFilter(String[] prefixes)
  {
    this.prefixes = new String[prefixes.length];
    for (int i = 0; i < prefixes.length; i++) {
      String prefix = prefixes[i];
      if (!prefix.equals("*"))
        this.prefixes[i] = prefix;
    }
  }

  public boolean matches(Destination destination)
  {
    String[] path = DestinationPath.getDestinationPaths(destination);
    int length = this.prefixes.length;
    if (path.length == length) {
      int i = 0; for (int size = length; i < size; i++) {
        String prefix = this.prefixes[i];
        if ((prefix != null) && (!prefix.equals(path[i]))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public String getText()
  {
    return DestinationPath.toString(this.prefixes);
  }

  public String toString() {
    return super.toString() + "[destination: " + getText() + "]";
  }

  public boolean isWildcard() {
    return true;
  }
}