package org.codehaus.activemq.filter;

import javax.jms.JMSException;
import javax.jms.Message;
import org.codehaus.activemq.message.ActiveMQMessage;

public class NoLocalFilter
  implements Filter
{
  private String clientId;

  public NoLocalFilter(String newClientId)
  {
    this.clientId = newClientId;
  }

  public boolean matches(Message message)
    throws JMSException
  {
    if ((message != null) && ((message instanceof ActiveMQMessage))) {
      ActiveMQMessage activeMQMessage = (ActiveMQMessage)message;

      String producerID = activeMQMessage.getProducerID();
      if ((producerID != null) && 
        (producerID.equals(this.clientId))) {
        return false;
      }

      if (this.clientId.equals(activeMQMessage.getJMSClientID())) {
        return false;
      }
    }
    return true;
  }

  public boolean isWildcard() {
    return false;
  }
}