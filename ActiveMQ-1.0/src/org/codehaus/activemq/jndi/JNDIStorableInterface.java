package org.codehaus.activemq.jndi;

import java.util.Properties;
import javax.naming.Referenceable;

public abstract interface JNDIStorableInterface extends Referenceable
{
  public abstract void setProperties(Properties paramProperties);

  public abstract Properties getProperties();
}