package org.codehaus.activemq.jndi;

import java.util.Properties;
import javax.naming.NamingException;
import javax.naming.Reference;

public abstract class JNDIBaseStorable
  implements JNDIStorableInterface
{
  private Properties properties = null;

  protected abstract void buildFromProperties(Properties paramProperties);

  protected abstract void populateProperties(Properties paramProperties);

  public synchronized void setProperties(Properties props)
  {
    this.properties = props;
    buildFromProperties(props);
  }

  public synchronized Properties getProperties()
  {
    if (this.properties == null) {
      this.properties = new Properties();
    }
    populateProperties(this.properties);
    return this.properties;
  }

  public Reference getReference()
    throws NamingException
  {
    return JNDIReferenceFactory.createReference(getClass().getName(), this);
  }
}