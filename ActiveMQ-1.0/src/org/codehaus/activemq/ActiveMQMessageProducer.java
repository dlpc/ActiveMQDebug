package org.codehaus.activemq;

import javax.jms.CompletionListener;
import javax.jms.Destination;
import javax.jms.IllegalStateException;
import javax.jms.InvalidDestinationException;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.management.j2ee.statistics.Stats;
import org.codehaus.activemq.management.JMSProducerStatsImpl;
import org.codehaus.activemq.management.StatsCapable;
import org.codehaus.activemq.message.ActiveMQDestination;
import org.codehaus.activemq.util.IdGenerator;

public class ActiveMQMessageProducer
  implements MessageProducer, StatsCapable
{
  protected ActiveMQSession session;
  protected String producerId;
  private IdGenerator idGenerator;
  protected boolean closed;
  private boolean disableMessageID;
  private boolean disableMessageTimestamp;
  private int defaultDeliveryMode;
  private int defaultPriority;
  private long defaultTimeToLive;
  protected ActiveMQDestination defaultDestination;
  private long startTime;
  private JMSProducerStatsImpl stats;

  protected ActiveMQMessageProducer(ActiveMQSession theSession, ActiveMQDestination destination)
    throws JMSException
  {
    this.session = theSession;
    this.defaultDestination = destination;
    this.idGenerator = new IdGenerator();
    this.disableMessageID = false;
    this.disableMessageTimestamp = false;
    this.defaultDeliveryMode = 2;
    this.defaultPriority = 4;
    this.defaultTimeToLive = 0L;
    this.startTime = System.currentTimeMillis();
    this.session.addProducer(this);
    this.stats = new JMSProducerStatsImpl(theSession.getSessionStats(), destination);
  }

  public Stats getStats() {
    return this.stats;
  }

  public JMSProducerStatsImpl getProducerStats() {
    return this.stats;
  }

  public void setDisableMessageID(boolean value)
    throws JMSException
  {
    checkClosed();
    this.disableMessageID = value;
  }

  public boolean getDisableMessageID()
    throws JMSException
  {
    checkClosed();
    return this.disableMessageID;
  }

  public void setDisableMessageTimestamp(boolean value)
    throws JMSException
  {
    checkClosed();
    this.disableMessageTimestamp = value;
  }

  public boolean getDisableMessageTimestamp()
    throws JMSException
  {
    checkClosed();
    return this.disableMessageTimestamp;
  }

  public void setDeliveryMode(int newDeliveryMode)
    throws JMSException
  {
    if ((newDeliveryMode != 2) && (newDeliveryMode != 1)) {
      throw new IllegalStateException("unkown delivery mode: " + newDeliveryMode);
    }
    checkClosed();
    this.defaultDeliveryMode = newDeliveryMode;
  }

  public int getDeliveryMode()
    throws JMSException
  {
    checkClosed();
    return this.defaultDeliveryMode;
  }

  public void setPriority(int newDefaultPriority)
    throws JMSException
  {
    if ((newDefaultPriority < 0) || (newDefaultPriority > 9)) {
      throw new IllegalStateException("default priority must be a value between 0 and 9");
    }
    checkClosed();
    this.defaultPriority = newDefaultPriority;
  }

  public int getPriority()
    throws JMSException
  {
    checkClosed();
    return this.defaultPriority;
  }

  public void setTimeToLive(long timeToLive)
    throws JMSException
  {
    if (timeToLive < 0L) {
      throw new IllegalStateException("cannot set a negative timeToLive");
    }
    checkClosed();
    this.defaultTimeToLive = timeToLive;
  }

  public long getTimeToLive()
    throws JMSException
  {
    checkClosed();
    return this.defaultTimeToLive;
  }

  public Destination getDestination()
    throws JMSException
  {
    checkClosed();
    return this.defaultDestination;
  }

  public void close()
    throws JMSException
  {
    this.session.removeProducer(this);
    this.closed = true;
  }

  protected void checkClosed() throws IllegalStateException {
    if (this.closed)
      throw new IllegalStateException("The producer is closed");
  }

  public void send(Message message)
    throws JMSException
  {
    send(this.defaultDestination, message, this.defaultDeliveryMode, this.defaultPriority, this.defaultTimeToLive);
  }

  public void send(Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException
  {
    send(this.defaultDestination, message, deliveryMode, priority, timeToLive);
  }

  public void send(Destination destination, Message message)
    throws JMSException
  {
    send(destination, message, this.defaultDeliveryMode, this.defaultPriority, this.defaultTimeToLive);
  }

  public void send(Destination destination, Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException
  {
    checkClosed();
    if (destination == null) {
      throw new InvalidDestinationException("Dont understand null destinations");
    }
    this.session.send(this, destination, message, deliveryMode, priority, timeToLive);
    this.stats.onMessage(message);
  }

  protected String getProducerId()
  {
    return this.producerId;
  }

  protected void setProducerId(String producerId)
  {
    this.producerId = producerId;
  }

  protected long getStartTime() {
    return this.startTime;
  }

  protected IdGenerator getIdGenerator() {
    return this.idGenerator;
  }

@Override
public long getDeliveryDelay() throws JMSException {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public void send(Message arg0, CompletionListener arg1) throws JMSException {
	// TODO Auto-generated method stub
	
}

@Override
public void send(Destination arg0, Message arg1, CompletionListener arg2) throws JMSException {
	// TODO Auto-generated method stub
	
}

@Override
public void send(Message arg0, int arg1, int arg2, long arg3, CompletionListener arg4) throws JMSException {
	// TODO Auto-generated method stub
	
}

@Override
public void send(Destination arg0, Message arg1, int arg2, int arg3, long arg4, CompletionListener arg5)
		throws JMSException {
	// TODO Auto-generated method stub
	
}

@Override
public void setDeliveryDelay(long arg0) throws JMSException {
	// TODO Auto-generated method stub
	
}
}