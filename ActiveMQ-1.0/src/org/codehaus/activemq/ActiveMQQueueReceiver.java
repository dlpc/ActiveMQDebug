package org.codehaus.activemq;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import org.codehaus.activemq.message.ActiveMQDestination;

public class ActiveMQQueueReceiver extends ActiveMQMessageConsumer
  implements QueueReceiver
{
  protected ActiveMQQueueReceiver(ActiveMQSession theSession, ActiveMQDestination dest, String selector, int cnum, int prefetch)
    throws JMSException
  {
    super(theSession, dest, "", selector, cnum, prefetch, false, false);
  }

  public Queue getQueue()
    throws JMSException
  {
    checkClosed();
    return (Queue)super.getDestination();
  }
}