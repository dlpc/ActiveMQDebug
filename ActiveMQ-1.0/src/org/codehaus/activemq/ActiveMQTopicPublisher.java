package org.codehaus.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Topic;
import javax.jms.TopicPublisher;
import org.codehaus.activemq.message.ActiveMQDestination;

public class ActiveMQTopicPublisher extends ActiveMQMessageProducer
  implements TopicPublisher
{
  protected ActiveMQTopicPublisher(ActiveMQSession session, ActiveMQDestination destination)
    throws JMSException
  {
    super(session, destination);
  }

  public Topic getTopic()
    throws JMSException
  {
    return (Topic)super.getDestination();
  }

  public void publish(Message message)
    throws JMSException
  {
    super.send(message);
  }

  public void publish(Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException
  {
    super.send(message, deliveryMode, priority, timeToLive);
  }

  public void publish(Topic topic, Message message)
    throws JMSException
  {
    super.send(topic, message);
  }

  public void publish(Topic topic, Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException
  {
    super.send(topic, message, deliveryMode, priority, timeToLive);
  }
}