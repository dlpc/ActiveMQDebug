package org.codehaus.activemq;

import java.util.Enumeration;
import java.util.Hashtable;
import javax.jms.ConnectionMetaData;

public class ActiveMQConnectionMetaData
  implements ConnectionMetaData
{
  public String getJMSVersion()
  {
    return "1.1";
  }

  public int getJMSMajorVersion()
  {
    return 1;
  }

  public int getJMSMinorVersion()
  {
    return 1;
  }

  public String getJMSProviderName()
  {
    return "Protique";
  }

  public String getProviderVersion()
  {
    return "0.1";
  }

  public int getProviderMajorVersion()
  {
    return 0;
  }

  public int getProviderMinorVersion()
  {
    return 1;
  }

  public Enumeration getJMSXPropertyNames()
  {
    Hashtable jmxProperties = new Hashtable();
    jmxProperties.put("JMSXGroupID", "1");
    jmxProperties.put("JMSXGroupSeq", "1");

    return jmxProperties.keys();
  }
}