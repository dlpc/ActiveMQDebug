package org.codehaus.activemq;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.TopicConnection;
import javax.jms.XAConnection;
import javax.jms.XAConnectionFactory;
import javax.jms.XAJMSContext;
import javax.jms.XAQueueConnection;
import javax.jms.XAQueueConnectionFactory;
import javax.jms.XATopicConnection;
import javax.jms.XATopicConnectionFactory;

public class ActiveMQXAConnectionFactory extends ActiveMQConnectionFactory
  implements XAConnectionFactory, XAQueueConnectionFactory, XATopicConnectionFactory
{
  public ActiveMQXAConnectionFactory()
  {
  }

  public ActiveMQXAConnectionFactory(String brokerURL)
  {
    super(brokerURL);
  }

  public ActiveMQXAConnectionFactory(String userName, String password, String brokerURL) {
    super(userName, password, brokerURL);
  }

  public XAConnection createXAConnection() throws JMSException {
    return createActiveMQXAConnection(this.userName, this.password);
  }

  public XAConnection createXAConnection(String userName, String password) throws JMSException {
    return createActiveMQXAConnection(userName, password);
  }

  public XAQueueConnection createXAQueueConnection() throws JMSException {
    return createActiveMQXAConnection(this.userName, this.password);
  }

  public XAQueueConnection createXAQueueConnection(String userName, String password) throws JMSException {
    return createActiveMQXAConnection(userName, password);
  }

  public XATopicConnection createXATopicConnection() throws JMSException {
    return createActiveMQXAConnection(this.userName, this.password);
  }

  public XATopicConnection createXATopicConnection(String userName, String password) throws JMSException {
    return createActiveMQXAConnection(userName, password);
  }

  public Connection createConnection() throws JMSException {
    return createActiveMQXAConnection(this.userName, this.password);
  }

  public Connection createConnection(String userName, String password) throws JMSException {
    return createActiveMQXAConnection(userName, password);
  }

  public QueueConnection createQueueConnection() throws JMSException {
    return createActiveMQXAConnection(this.userName, this.password);
  }

  public QueueConnection createQueueConnection(String userName, String password) throws JMSException {
    return createActiveMQXAConnection(userName, password);
  }

  public TopicConnection createTopicConnection() throws JMSException {
    return createActiveMQXAConnection(this.userName, this.password);
  }

  public TopicConnection createTopicConnection(String userName, String password) throws JMSException {
    return createActiveMQXAConnection(userName, password);
  }

  protected ActiveMQXAConnection createActiveMQXAConnection(String userName, String password) throws JMSException {
    ActiveMQXAConnection connection = new ActiveMQXAConnection(this, userName, password, createTransportChannel(this.brokerURL));
    if ((this.clientID != null) && (this.clientID.length() > 0)) {
      connection.setClientID(this.clientID);
    }
    return connection;
  }

@Override
public XAJMSContext createXAContext() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public XAJMSContext createXAContext(String arg0, String arg1) {
	// TODO Auto-generated method stub
	return null;
}
}