package org.codehaus.activemq;

public class ActiveMQPrefetchPolicy
{
  private int queuePrefetch;
  private int queueBrowserPrefetch;
  private int topicPrefetch;
  private int durableTopicPrefetch;

  public ActiveMQPrefetchPolicy()
  {
    this.queuePrefetch = 10;
    this.queueBrowserPrefetch = 500;
    this.topicPrefetch = 1000;
    this.durableTopicPrefetch = 100;
  }

  public int getDurableTopicPrefetch()
  {
    return this.durableTopicPrefetch;
  }

  public void setDurableTopicPrefetch(int durableTopicPrefetch)
  {
    this.durableTopicPrefetch = durableTopicPrefetch;
  }

  public int getQueuePrefetch()
  {
    return this.queuePrefetch;
  }

  public void setQueuePrefetch(int queuePrefetch)
  {
    this.queuePrefetch = queuePrefetch;
  }

  public int getQueueBrowserPrefetch()
  {
    return this.queueBrowserPrefetch;
  }

  public void setQueueBrowserPrefetch(int queueBrowserPrefetch)
  {
    this.queueBrowserPrefetch = queueBrowserPrefetch;
  }

  public int getTopicPrefetch()
  {
    return this.topicPrefetch;
  }

  public void setTopicPrefetch(int topicPrefetch)
  {
    this.topicPrefetch = topicPrefetch;
  }
}