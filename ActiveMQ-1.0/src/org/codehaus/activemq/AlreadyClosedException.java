package org.codehaus.activemq;

import javax.jms.JMSException;

public class AlreadyClosedException extends JMSException
{
  public AlreadyClosedException(String description)
  {
    super("Cannot use " + description + " as it has already been closed", "AMQ-1001");
  }
}