package org.codehaus.activemq.util;

import javax.jms.JMSException;
import org.codehaus.activemq.store.PersistenceAdapter;

public class TransactionTemplate
{
  private PersistenceAdapter persistenceAdapter;

  public TransactionTemplate(PersistenceAdapter persistenceAdapter)
  {
    this.persistenceAdapter = persistenceAdapter;
  }

  public void run(Callback task) throws JMSException {
    this.persistenceAdapter.beginTransaction();
    Throwable throwable = null;
    try {
      task.execute();
    }
    catch (Throwable t) {
      throwable = t;
    }
    if (throwable == null) {
      this.persistenceAdapter.commitTransaction();
    }
    else
      this.persistenceAdapter.rollbackTransaction();
  }
}