package org.codehaus.activemq.util;

public abstract interface Callback
{
  public abstract void execute()
    throws Throwable;
}