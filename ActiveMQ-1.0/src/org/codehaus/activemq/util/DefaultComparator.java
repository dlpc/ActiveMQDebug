package org.codehaus.activemq.util;

import java.io.Serializable;
import java.util.Comparator;

public class DefaultComparator
  implements Comparator, Serializable
{
  static final long serialVersionUID = 1L;

  public int compare(Object o1, Object o2)
  {
    Comparable c1 = (Comparable)o1;
    return c1.compareTo(o2);
  }
}