package org.codehaus.activemq.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class LRUCache extends LinkedHashMap
{
  protected static final int DEFAULT_INITIAL_CAPACITY = 1000;
  protected static final float DEFAULT_LOAD_FACTOR = 0.75F;
  private int maxSize;

  public LRUCache(int initialCapacity, float loadFactor, boolean accessOrder, int maxSize)
  {
    super(initialCapacity, loadFactor, accessOrder);
    this.maxSize = maxSize;
  }

  public LRUCache(int maxSize) {
    this(1000, 0.75F, true, maxSize);
  }

  public LRUCache(int maxSize, boolean accessOrder) {
    this(1000, 0.75F, accessOrder, maxSize);
  }

  protected boolean removeEldestEntry(Map.Entry eldest) {
    return size() > this.maxSize;
  }
}