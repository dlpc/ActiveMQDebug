package org.codehaus.activemq.util;

import javax.jms.Connection;
import javax.jms.JMSException;
import org.codehaus.activemq.ActiveMQConnection;

public class JmsLogAppender extends JmsLogAppenderSupport
{
  private String uri = "tcp://localhost:61616";
  private String userName;
  private String password;

  public String getUri()
  {
    return this.uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getUserName() {
    return this.userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  protected Connection createConnection() throws JMSException {
    if (this.userName != null) {
      return ActiveMQConnection.makeConnection(this.userName, this.password, this.uri);
    }

    return ActiveMQConnection.makeConnection(this.uri);
  }
}