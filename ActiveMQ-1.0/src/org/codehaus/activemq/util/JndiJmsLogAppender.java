package org.codehaus.activemq.util;

import java.util.Hashtable;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.helpers.LogLog;

public class JndiJmsLogAppender extends JmsLogAppenderSupport
{
  private String jndiName;
  private String userName;
  private String password;
  private String initialContextFactoryName;
  private String providerURL;
  private String urlPkgPrefixes;
  private String securityPrincipalName;
  private String securityCredentials;

  public String getJndiName()
  {
    return this.jndiName;
  }

  public void setJndiName(String jndiName) {
    this.jndiName = jndiName;
  }

  public String getUserName() {
    return this.userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getInitialContextFactoryName()
  {
    return this.initialContextFactoryName;
  }

  public void setInitialContextFactoryName(String initialContextFactoryName) {
    this.initialContextFactoryName = initialContextFactoryName;
  }

  public String getProviderURL() {
    return this.providerURL;
  }

  public void setProviderURL(String providerURL) {
    this.providerURL = providerURL;
  }

  public String getUrlPkgPrefixes() {
    return this.urlPkgPrefixes;
  }

  public void setUrlPkgPrefixes(String urlPkgPrefixes) {
    this.urlPkgPrefixes = urlPkgPrefixes;
  }

  public String getSecurityPrincipalName() {
    return this.securityPrincipalName;
  }

  public void setSecurityPrincipalName(String securityPrincipalName) {
    this.securityPrincipalName = securityPrincipalName;
  }

  public String getSecurityCredentials() {
    return this.securityCredentials;
  }

  public void setSecurityCredentials(String securityCredentials) {
    this.securityCredentials = securityCredentials;
  }

  protected Connection createConnection()
    throws JMSException, NamingException
  {
    InitialContext context = createInitialContext();
    LogLog.debug("Looking up ConnectionFactory with jndiName: " + this.jndiName);
    ConnectionFactory factory = (ConnectionFactory)context.lookup(this.jndiName);
    if (factory == null) {
      throw new JMSException("No such ConnectionFactory for name: " + this.jndiName);
    }
    if (this.userName != null) {
      return factory.createConnection(this.userName, this.password);
    }

    return factory.createConnection();
  }

  protected InitialContext createInitialContext() throws NamingException
  {
    if (this.initialContextFactoryName == null) {
      return new InitialContext();
    }

    Hashtable env = new Hashtable();
    env.put("java.naming.factory.initial", this.initialContextFactoryName);
    if (this.providerURL != null) {
      env.put("java.naming.provider.url", this.providerURL);
    }
    else {
      LogLog.warn("You have set InitialContextFactoryName option but not the ProviderURL. This is likely to cause problems.");
    }

    if (this.urlPkgPrefixes != null) {
      env.put("java.naming.factory.url.pkgs", this.urlPkgPrefixes);
    }

    if (this.securityPrincipalName != null) {
      env.put("java.naming.security.principal", this.securityPrincipalName);
      if (this.securityCredentials != null) {
        env.put("java.naming.security.credentials", this.securityCredentials);
      }
      else {
        LogLog.warn("You have set SecurityPrincipalName option but not the SecurityCredentials. This is likely to cause problems.");
      }
    }

    LogLog.debug("Looking up JNDI context with environment: " + env);
    return new InitialContext(env);
  }
}