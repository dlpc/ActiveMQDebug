package org.codehaus.activemq.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationHelper
{
  public static byte[] serialize(Object object)
    throws IOException
  {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    ObjectOutputStream out = new ObjectOutputStream(buffer);
    out.writeObject(object);
    out.close();
    return buffer.toByteArray();
  }

  public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
    ByteArrayInputStream buffer = new ByteArrayInputStream(data);
    ObjectInputStream in = new ObjectInputStream(buffer);
    Object answer = in.readObject();
    in.close();
    return answer;
  }
}