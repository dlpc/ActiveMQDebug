package org.codehaus.activemq.util;

import javax.jms.JMSException;

public class ExceptionTemplate
{
  private Throwable firstException;

  public void run(Callback task)
  {
    try
    {
      task.execute();
    }
    catch (Throwable t) {
      if (this.firstException == null)
        this.firstException = t;
    }
  }

  public Throwable getFirstException()
  {
    return this.firstException;
  }

  public void throwJMSException()
    throws JMSException
  {
    if (this.firstException != null) {
      if ((this.firstException instanceof JMSException)) {
        throw ((JMSException)this.firstException);
      }
      if ((this.firstException instanceof Exception)) {
        throw JMSExceptionHelper.newJMSException(this.firstException.getMessage(), (Exception)this.firstException);
      }

      throw JMSExceptionHelper.newJMSException(this.firstException.getMessage(), new Exception(this.firstException));
    }
  }
}