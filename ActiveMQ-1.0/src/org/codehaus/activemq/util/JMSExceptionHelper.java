package org.codehaus.activemq.util;

import java.io.IOException;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JMSExceptionHelper
{
  private static final Log log = LogFactory.getLog(JMSExceptionHelper.class);

  public static JMSException newJMSException(String message, Throwable cause) {
    if ((cause instanceof Exception)) {
      return newJMSException(message, (Exception)cause);
    }

    return newJMSException(message, new Exception(cause));
  }

  public static JMSException newJMSException(String message, Exception cause)
  {
    log.trace(message, cause);

    JMSException jmsEx = new JMSException(message);
    jmsEx.setLinkedException(cause);
    jmsEx.initCause(cause);
    return jmsEx;
  }

  public static IOException newIOException(JMSException e) {
    IOException answer = new IOException(e.getMessage());
    answer.setStackTrace(e.getStackTrace());
    return answer;
  }
}