package org.codehaus.activemq.util;

import EDU.oswego.cs.dl.util.concurrent.Executor;
import EDU.oswego.cs.dl.util.concurrent.PooledExecutor;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.activemq.service.Service;

public class ExecutorHelper
{
  private static final Log log = LogFactory.getLog(ExecutorHelper.class);

  public static void stopExecutor(Executor executor)
    throws InterruptedException, JMSException
  {
    if ((executor instanceof Service)) {
      Service service = (Service)executor;
      service.stop();
    }
    else if ((executor instanceof PooledExecutor)) {
      PooledExecutor pe = (PooledExecutor)executor;
      pe.shutdownAfterProcessingCurrentlyQueuedTasks();

      pe.awaitTerminationAfterShutdown();
    }
    else if (executor != null) {
      log.warn("Don't know how to cleanly close down the given executor: " + executor + ". Consider deriving from this class to implement the Service interface to shut down cleanly");
    }
  }
}