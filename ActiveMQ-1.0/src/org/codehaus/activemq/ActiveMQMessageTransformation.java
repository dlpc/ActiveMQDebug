package org.codehaus.activemq;

import java.util.Enumeration;
import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import org.codehaus.activemq.message.ActiveMQBytesMessage;
import org.codehaus.activemq.message.ActiveMQDestination;
import org.codehaus.activemq.message.ActiveMQMapMessage;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ActiveMQObjectMessage;
import org.codehaus.activemq.message.ActiveMQQueue;
import org.codehaus.activemq.message.ActiveMQStreamMessage;
import org.codehaus.activemq.message.ActiveMQTemporaryQueue;
import org.codehaus.activemq.message.ActiveMQTemporaryTopic;
import org.codehaus.activemq.message.ActiveMQTextMessage;
import org.codehaus.activemq.message.ActiveMQTopic;

class ActiveMQMessageTransformation
{
  static ActiveMQDestination transformDestination(Destination destination)
    throws JMSException
  {
    ActiveMQDestination result = null;
    if (destination != null) {
      if ((destination instanceof ActiveMQDestination)) {
        result = (ActiveMQDestination)destination;
      }
      else if ((destination instanceof TemporaryQueue)) {
        result = new ActiveMQTemporaryQueue(((Queue)destination).getQueueName());
      }
      else if ((destination instanceof TemporaryTopic)) {
        result = new ActiveMQTemporaryTopic(((Topic)destination).getTopicName());
      }
      else if ((destination instanceof Queue)) {
        result = new ActiveMQQueue(((Queue)destination).getQueueName());
      }
      else if ((destination instanceof Topic)) {
        result = new ActiveMQTopic(((Topic)destination).getTopicName());
      }
    }

    return result;
  }

  public static final ActiveMQMessage transformMessage(Message message)
    throws JMSException
  {
    if ((message instanceof ActiveMQMessage)) {
      return (ActiveMQMessage)message;
    }

    ActiveMQMessage activeMessage = null;
    if ((message instanceof ObjectMessage)) {
      ObjectMessage objMsg = (ObjectMessage)message;
      ActiveMQObjectMessage msg = new ActiveMQObjectMessage();
      msg.setObject(objMsg.getObject());
      activeMessage = msg;
    }
    else if ((message instanceof TextMessage)) {
      TextMessage textMsg = (TextMessage)message;
      ActiveMQTextMessage msg = new ActiveMQTextMessage();
      msg.setText(textMsg.getText());
      activeMessage = msg;
    }
    else if ((message instanceof MapMessage)) {
      MapMessage mapMsg = (MapMessage)message;
      ActiveMQMapMessage msg = new ActiveMQMapMessage();
      for (Enumeration iter = mapMsg.getMapNames(); iter.hasMoreElements(); ) {
        String name = iter.nextElement().toString();
        msg.setObject(name, mapMsg.getObject(name));
      }
      activeMessage = msg;
    } else {
      if ((message instanceof BytesMessage)) {
        BytesMessage bytesMsg = (BytesMessage)message;
        bytesMsg.reset();
        ActiveMQBytesMessage msg = new ActiveMQBytesMessage();
        try {
          while (true) {
            msg.writeByte(bytesMsg.readByte());
          }
        }
        catch (JMSException e)
        {
          activeMessage = msg;
        }
      }
      if ((message instanceof StreamMessage)) {
        StreamMessage streamMessage = (StreamMessage)message;
        streamMessage.reset();
        ActiveMQStreamMessage msg = new ActiveMQStreamMessage();
        Object obj = null;
        try {
          while ((obj = streamMessage.readObject()) != null)
            msg.writeObject(obj);
        }
        catch (JMSException e)
        {
        }
        activeMessage = msg;
      }
      else {
        activeMessage = new ActiveMQMessage();
      }
    }
    activeMessage.setJMSMessageID(message.getJMSMessageID());
    activeMessage.setJMSCorrelationID(message.getJMSCorrelationID());
    activeMessage.setJMSReplyTo(transformDestination(message.getJMSReplyTo()));
    activeMessage.setJMSDestination(transformDestination(message.getJMSDestination()));
    activeMessage.setJMSDeliveryMode(message.getJMSDeliveryMode());
    activeMessage.setJMSRedelivered(message.getJMSRedelivered());
    activeMessage.setJMSType(message.getJMSType());
    activeMessage.setJMSExpiration(message.getJMSExpiration());
    activeMessage.setJMSPriority(message.getJMSPriority());
    activeMessage.setJMSTimestamp(message.getJMSTimestamp());
    for (Enumeration propertyNames = message.getPropertyNames(); propertyNames.hasMoreElements(); ) {
      String name = propertyNames.nextElement().toString();
      Object obj = message.getObjectProperty(name);
      activeMessage.setObjectProperty(name, obj);
    }
    return activeMessage;
  }
}