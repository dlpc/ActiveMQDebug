package org.codehaus.activemq.broker;

import java.io.File;
import javax.jms.JMSException;
import javax.transaction.xa.XAException;
import org.codehaus.activemq.capacity.CapacityMonitor;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ActiveMQXid;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.service.Service;
import org.codehaus.activemq.store.PersistenceAdapter;

public abstract interface Broker extends Service, CapacityMonitor
{
  public abstract void addMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void removeMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract ActiveMQXid[] getPreparedTransactions(BrokerClient paramBrokerClient)
    throws XAException;

  public abstract void acknowledgeMessage(BrokerClient paramBrokerClient, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void acknowledgeTransactedMessage(BrokerClient paramBrokerClient, String paramString, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void redeliverMessage(BrokerClient paramBrokerClient, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void sendMessage(BrokerClient paramBrokerClient, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void sendTransactedMessage(BrokerClient paramBrokerClient, String paramString, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void cleanUpClient(BrokerClient paramBrokerClient)
    throws JMSException;

  public abstract void deleteSubscription(String paramString1, String paramString2)
    throws JMSException;

  public abstract void startTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void startTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract int prepareTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid, boolean paramBoolean)
    throws XAException;

  public abstract File getTempDir();

  public abstract String getBrokerName();

  public abstract PersistenceAdapter getPersistenceAdapter();

  public abstract void setPersistenceAdapter(PersistenceAdapter paramPersistenceAdapter);
}