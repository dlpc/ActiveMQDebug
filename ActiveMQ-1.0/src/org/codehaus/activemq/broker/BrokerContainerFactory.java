package org.codehaus.activemq.broker;

public abstract interface BrokerContainerFactory
{
  public abstract BrokerContainer createBrokerContainer(String paramString);
}