package org.codehaus.activemq.broker;

import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.service.Service;
import org.codehaus.activemq.transport.TransportChannel;

public abstract interface BrokerClient extends Service
{
  public abstract void initialize(BrokerConnector paramBrokerConnector, TransportChannel paramTransportChannel);

  public abstract void dispatch(ActiveMQMessage paramActiveMQMessage);

  public abstract boolean isBrokerConnection();

  public abstract int getCapacity();

  public abstract boolean isSlowConsumer();

  public abstract void updateBrokerCapacity(int paramInt);

  public abstract String getClientID();

  public abstract void cleanUp();

  public abstract TransportChannel getChannel();
}