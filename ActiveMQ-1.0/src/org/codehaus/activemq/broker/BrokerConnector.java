package org.codehaus.activemq.broker;

import javax.jms.JMSException;
import javax.transaction.xa.XAException;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ActiveMQXid;
import org.codehaus.activemq.message.BrokerInfo;
import org.codehaus.activemq.message.ConnectionInfo;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.DurableUnsubscribe;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.message.ProducerInfo;
import org.codehaus.activemq.message.SessionInfo;
import org.codehaus.activemq.service.Service;
import org.codehaus.activemq.transport.TransportServerChannel;

public abstract interface BrokerConnector extends Service
{
  public abstract BrokerInfo getBrokerInfo();

  public abstract TransportServerChannel getServerChannel();

  public abstract int getBrokerCapacity();

  public abstract void registerClient(BrokerClient paramBrokerClient, ConnectionInfo paramConnectionInfo)
    throws JMSException;

  public abstract void deregisterClient(BrokerClient paramBrokerClient, ConnectionInfo paramConnectionInfo)
    throws JMSException;

  public abstract void registerMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void deregisterMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void registerMessageProducer(BrokerClient paramBrokerClient, ProducerInfo paramProducerInfo)
    throws JMSException;

  public abstract void deregisterMessageProducer(BrokerClient paramBrokerClient, ProducerInfo paramProducerInfo)
    throws JMSException;

  public abstract void registerSession(BrokerClient paramBrokerClient, SessionInfo paramSessionInfo)
    throws JMSException;

  public abstract void deregisterSession(BrokerClient paramBrokerClient, SessionInfo paramSessionInfo)
    throws JMSException;

  public abstract void startTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void startTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract ActiveMQXid[] getPreparedTransactions(BrokerClient paramBrokerClient)
    throws XAException;

  public abstract int prepareTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid, boolean paramBoolean)
    throws XAException;

  public abstract void sendTransactedMessage(BrokerClient paramBrokerClient, String paramString, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void acknowledgeTransactedMessage(BrokerClient paramBrokerClient, String paramString, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void sendMessage(BrokerClient paramBrokerClient, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void acknowledgeMessage(BrokerClient paramBrokerClient, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void durableUnsubscribe(BrokerClient paramBrokerClient, DurableUnsubscribe paramDurableUnsubscribe)
    throws JMSException;

  public abstract String getResourceManagerId(BrokerClient paramBrokerClient);

  public abstract BrokerContainer getBrokerContainer();
}