package org.codehaus.activemq.broker;

import java.util.List;
import javax.jms.InvalidClientIDException;
import javax.jms.JMSException;
import javax.transaction.xa.XAException;
import org.codehaus.activemq.message.ActiveMQMessage;
import org.codehaus.activemq.message.ActiveMQXid;
import org.codehaus.activemq.message.ConnectionInfo;
import org.codehaus.activemq.message.ConsumerInfo;
import org.codehaus.activemq.message.DurableUnsubscribe;
import org.codehaus.activemq.message.MessageAck;
import org.codehaus.activemq.message.ProducerInfo;
import org.codehaus.activemq.message.SessionInfo;
import org.codehaus.activemq.service.Service;

public abstract interface BrokerContainer extends Service
{
  public abstract void registerConnection(BrokerClient paramBrokerClient, ConnectionInfo paramConnectionInfo)
    throws InvalidClientIDException;

  public abstract void deregisterConnection(BrokerClient paramBrokerClient, ConnectionInfo paramConnectionInfo)
    throws JMSException;

  public abstract void registerMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void deregisterMessageConsumer(BrokerClient paramBrokerClient, ConsumerInfo paramConsumerInfo)
    throws JMSException;

  public abstract void registerMessageProducer(BrokerClient paramBrokerClient, ProducerInfo paramProducerInfo)
    throws JMSException;

  public abstract void deregisterMessageProducer(BrokerClient paramBrokerClient, ProducerInfo paramProducerInfo)
    throws JMSException;

  public abstract void registerSession(BrokerClient paramBrokerClient, SessionInfo paramSessionInfo)
    throws JMSException;

  public abstract void deregisterSession(BrokerClient paramBrokerClient, SessionInfo paramSessionInfo)
    throws JMSException;

  public abstract void startTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, String paramString)
    throws JMSException;

  public abstract void sendTransactedMessage(BrokerClient paramBrokerClient, String paramString, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void acknowledgeTransactedMessage(BrokerClient paramBrokerClient, String paramString, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void sendMessage(BrokerClient paramBrokerClient, ActiveMQMessage paramActiveMQMessage)
    throws JMSException;

  public abstract void acknowledgeMessage(BrokerClient paramBrokerClient, MessageAck paramMessageAck)
    throws JMSException;

  public abstract void durableUnsubscribe(BrokerClient paramBrokerClient, DurableUnsubscribe paramDurableUnsubscribe)
    throws JMSException;

  public abstract void startTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract ActiveMQXid[] getPreparedTransactions(BrokerClient paramBrokerClient)
    throws XAException;

  public abstract int prepareTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void rollbackTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid)
    throws XAException;

  public abstract void commitTransaction(BrokerClient paramBrokerClient, ActiveMQXid paramActiveMQXid, boolean paramBoolean)
    throws XAException;

  public abstract void addConnector(BrokerConnector paramBrokerConnector);

  public abstract void removeConnector(BrokerConnector paramBrokerConnector);

  public abstract Broker getBroker();

  public abstract List getConnectors();
}