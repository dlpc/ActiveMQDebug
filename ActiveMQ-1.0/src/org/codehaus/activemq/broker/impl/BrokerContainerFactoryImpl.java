package org.codehaus.activemq.broker.impl;

import org.codehaus.activemq.broker.BrokerContainer;
import org.codehaus.activemq.broker.BrokerContainerFactory;
import org.codehaus.activemq.store.PersistenceAdapter;

public class BrokerContainerFactoryImpl
  implements BrokerContainerFactory
{
  private PersistenceAdapter persistenceAdapter;

  public BrokerContainer createBrokerContainer(String brokerName)
  {
    if (this.persistenceAdapter != null) {
      return new BrokerContainerImpl(brokerName, this.persistenceAdapter);
    }

    return new BrokerContainerImpl(brokerName);
  }

  public PersistenceAdapter getPersistenceAdapter()
  {
    return this.persistenceAdapter;
  }

  public void setPersistenceAdapter(PersistenceAdapter persistenceAdapter) {
    this.persistenceAdapter = persistenceAdapter;
  }
}