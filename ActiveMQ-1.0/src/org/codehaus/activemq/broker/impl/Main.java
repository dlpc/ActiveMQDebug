package org.codehaus.activemq.broker.impl;

import java.io.PrintStream;
import javax.jms.JMSException;
import org.codehaus.activemq.broker.BrokerConnector;
import org.codehaus.activemq.message.DefaultWireFormat;

public class Main
{
  public static void main(String[] args)
  {
    try
    {
      String url = "tcp://localhost:61616";
      if (args.length > 0) {
        url = args[0];
      }

      BrokerContainerImpl container = new BrokerContainerImpl(url);
      BrokerConnector brokerConnector = new BrokerConnectorImpl(container, url, new DefaultWireFormat());
      container.start();
      brokerConnector.start();

      Object lock = new Object();
      synchronized (lock) {
        lock.wait();
      }
    }
    catch (JMSException e) {
      System.out.println("Caught: " + e);
      e.printStackTrace();
      Exception le = e.getLinkedException();
      System.out.println("Reason: " + le);
      if (le != null)
        le.printStackTrace();
    }
    catch (Exception e)
    {
      System.out.println("Caught: " + e);
      e.printStackTrace();
    }
  }
}